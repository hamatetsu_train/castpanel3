﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CastPanel3
{
    internal class frmConfig : Form
    {
        #region コントロール定義
        private Label lblPortNo;
        private NumericUpDown nudPortNo;
        private Button btnPort0;
        private Button btnPort1;
        private Button btnPort2;
        private Button btnPort3;
        private Button btnPort4;
        private Button btnPort5;
        private Button btnPort6;
        private Button btnPort7;
        private Button btnPort8;
        private Button btnPort9;
        private Button btnPortC;
        private Button btnStart;
        private Button btnCancel;
        private Panel pnlPort;
        private Button btnPortR;

        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfig));
            this.lblPortNo = new System.Windows.Forms.Label();
            this.nudPortNo = new System.Windows.Forms.NumericUpDown();
            this.btnPort0 = new System.Windows.Forms.Button();
            this.btnPort1 = new System.Windows.Forms.Button();
            this.btnPort2 = new System.Windows.Forms.Button();
            this.btnPort3 = new System.Windows.Forms.Button();
            this.btnPort4 = new System.Windows.Forms.Button();
            this.btnPort5 = new System.Windows.Forms.Button();
            this.btnPort6 = new System.Windows.Forms.Button();
            this.btnPort7 = new System.Windows.Forms.Button();
            this.btnPort8 = new System.Windows.Forms.Button();
            this.btnPort9 = new System.Windows.Forms.Button();
            this.btnPortC = new System.Windows.Forms.Button();
            this.btnPortR = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlPort = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.nudPortNo)).BeginInit();
            this.pnlPort.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPortNo
            // 
            this.lblPortNo.AutoSize = true;
            this.lblPortNo.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPortNo.ForeColor = System.Drawing.Color.White;
            this.lblPortNo.Location = new System.Drawing.Point(8, 17);
            this.lblPortNo.Name = "lblPortNo";
            this.lblPortNo.Size = new System.Drawing.Size(122, 24);
            this.lblPortNo.TabIndex = 0;
            this.lblPortNo.Text = "受信ポート番号";
            // 
            // nudPortNo
            // 
            this.nudPortNo.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.nudPortNo.Location = new System.Drawing.Point(146, 3);
            this.nudPortNo.Maximum = new decimal(new int[] {
            65525,
            0,
            0,
            0});
            this.nudPortNo.Name = "nudPortNo";
            this.nudPortNo.Size = new System.Drawing.Size(146, 55);
            this.nudPortNo.TabIndex = 1;
            this.nudPortNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudPortNo.Value = new decimal(new int[] {
            49201,
            0,
            0,
            0});
            // 
            // btnPort0
            // 
            this.btnPort0.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort0.Location = new System.Drawing.Point(41, 84);
            this.btnPort0.Name = "btnPort0";
            this.btnPort0.Size = new System.Drawing.Size(45, 45);
            this.btnPort0.TabIndex = 10;
            this.btnPort0.Text = "０";
            this.btnPort0.UseVisualStyleBackColor = true;
            this.btnPort0.Click += new System.EventHandler(this.btnPort0_Click);
            // 
            // btnPort1
            // 
            this.btnPort1.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort1.Location = new System.Drawing.Point(92, 84);
            this.btnPort1.Name = "btnPort1";
            this.btnPort1.Size = new System.Drawing.Size(45, 45);
            this.btnPort1.TabIndex = 11;
            this.btnPort1.Text = "１";
            this.btnPort1.UseVisualStyleBackColor = true;
            this.btnPort1.Click += new System.EventHandler(this.btnPort1_Click);
            // 
            // btnPort2
            // 
            this.btnPort2.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort2.Location = new System.Drawing.Point(143, 84);
            this.btnPort2.Name = "btnPort2";
            this.btnPort2.Size = new System.Drawing.Size(45, 45);
            this.btnPort2.TabIndex = 12;
            this.btnPort2.Text = "２";
            this.btnPort2.UseVisualStyleBackColor = true;
            this.btnPort2.Click += new System.EventHandler(this.btnPort2_Click);
            // 
            // btnPort3
            // 
            this.btnPort3.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort3.Location = new System.Drawing.Point(194, 84);
            this.btnPort3.Name = "btnPort3";
            this.btnPort3.Size = new System.Drawing.Size(45, 45);
            this.btnPort3.TabIndex = 13;
            this.btnPort3.Text = "３";
            this.btnPort3.UseVisualStyleBackColor = true;
            this.btnPort3.Click += new System.EventHandler(this.btnPort3_Click);
            // 
            // btnPort4
            // 
            this.btnPort4.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort4.Location = new System.Drawing.Point(245, 84);
            this.btnPort4.Name = "btnPort4";
            this.btnPort4.Size = new System.Drawing.Size(45, 45);
            this.btnPort4.TabIndex = 14;
            this.btnPort4.Text = "４";
            this.btnPort4.UseVisualStyleBackColor = true;
            this.btnPort4.Click += new System.EventHandler(this.btnPort4_Click);
            // 
            // btnPort5
            // 
            this.btnPort5.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort5.Location = new System.Drawing.Point(41, 135);
            this.btnPort5.Name = "btnPort5";
            this.btnPort5.Size = new System.Drawing.Size(45, 45);
            this.btnPort5.TabIndex = 15;
            this.btnPort5.Text = "５";
            this.btnPort5.UseVisualStyleBackColor = true;
            this.btnPort5.Click += new System.EventHandler(this.btnPort5_Click);
            // 
            // btnPort6
            // 
            this.btnPort6.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort6.Location = new System.Drawing.Point(92, 135);
            this.btnPort6.Name = "btnPort6";
            this.btnPort6.Size = new System.Drawing.Size(45, 45);
            this.btnPort6.TabIndex = 16;
            this.btnPort6.Text = "６";
            this.btnPort6.UseVisualStyleBackColor = true;
            this.btnPort6.Click += new System.EventHandler(this.btnPort6_Click);
            // 
            // btnPort7
            // 
            this.btnPort7.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort7.Location = new System.Drawing.Point(143, 135);
            this.btnPort7.Name = "btnPort7";
            this.btnPort7.Size = new System.Drawing.Size(45, 45);
            this.btnPort7.TabIndex = 17;
            this.btnPort7.Text = "７";
            this.btnPort7.UseVisualStyleBackColor = true;
            this.btnPort7.Click += new System.EventHandler(this.btnPort7_Click);
            // 
            // btnPort8
            // 
            this.btnPort8.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort8.Location = new System.Drawing.Point(194, 135);
            this.btnPort8.Name = "btnPort8";
            this.btnPort8.Size = new System.Drawing.Size(45, 45);
            this.btnPort8.TabIndex = 18;
            this.btnPort8.Text = "８";
            this.btnPort8.UseVisualStyleBackColor = true;
            this.btnPort8.Click += new System.EventHandler(this.btnPort8_Click);
            // 
            // btnPort9
            // 
            this.btnPort9.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPort9.Location = new System.Drawing.Point(245, 135);
            this.btnPort9.Name = "btnPort9";
            this.btnPort9.Size = new System.Drawing.Size(45, 45);
            this.btnPort9.TabIndex = 19;
            this.btnPort9.Text = "９";
            this.btnPort9.UseVisualStyleBackColor = true;
            this.btnPort9.Click += new System.EventHandler(this.btnPort9_Click);
            // 
            // btnPortC
            // 
            this.btnPortC.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPortC.Location = new System.Drawing.Point(320, 84);
            this.btnPortC.Name = "btnPortC";
            this.btnPortC.Size = new System.Drawing.Size(45, 45);
            this.btnPortC.TabIndex = 20;
            this.btnPortC.Text = "C";
            this.btnPortC.UseVisualStyleBackColor = true;
            this.btnPortC.Click += new System.EventHandler(this.btnPortC_Click);
            // 
            // btnPortR
            // 
            this.btnPortR.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPortR.Location = new System.Drawing.Point(320, 135);
            this.btnPortR.Name = "btnPortR";
            this.btnPortR.Size = new System.Drawing.Size(45, 45);
            this.btnPortR.TabIndex = 21;
            this.btnPortR.Text = "確";
            this.btnPortR.UseVisualStyleBackColor = true;
            this.btnPortR.Click += new System.EventHandler(this.btnPortR_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnStart.Location = new System.Drawing.Point(45, 275);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(147, 60);
            this.btnStart.TabIndex = 22;
            this.btnStart.Text = "開始";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("メイリオ", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCancel.Location = new System.Drawing.Point(249, 275);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(147, 60);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "ｷｬﾝｾﾙ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlPort
            // 
            this.pnlPort.Controls.Add(this.nudPortNo);
            this.pnlPort.Controls.Add(this.lblPortNo);
            this.pnlPort.Controls.Add(this.btnPort0);
            this.pnlPort.Controls.Add(this.btnPortR);
            this.pnlPort.Controls.Add(this.btnPort1);
            this.pnlPort.Controls.Add(this.btnPortC);
            this.pnlPort.Controls.Add(this.btnPort2);
            this.pnlPort.Controls.Add(this.btnPort9);
            this.pnlPort.Controls.Add(this.btnPort3);
            this.pnlPort.Controls.Add(this.btnPort8);
            this.pnlPort.Controls.Add(this.btnPort4);
            this.pnlPort.Controls.Add(this.btnPort7);
            this.pnlPort.Controls.Add(this.btnPort5);
            this.pnlPort.Controls.Add(this.btnPort6);
            this.pnlPort.Location = new System.Drawing.Point(5, 12);
            this.pnlPort.Name = "pnlPort";
            this.pnlPort.Size = new System.Drawing.Size(427, 201);
            this.pnlPort.TabIndex = 24;
            // 
            // frmConfig
            // 
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(434, 361);
            this.Controls.Add(this.pnlPort);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConfig";
            this.Text = "設定";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudPortNo)).EndInit();
            this.pnlPort.ResumeLayout(false);
            this.pnlPort.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        #region 変数定義
        /// <summary>
        /// リッスンポート番号
        /// </summary>
        public int PortNo;

        /// <summary>
        /// リッスン状態
        /// </summary>
        public bool ListenStart;
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public frmConfig()
        {
            InitializeComponent();
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            try
            {
                nudPortNo.Value = this.PortNo;
                if(this.ListenStart == true)
                {
                    btnStart.Text = "停止";
                    pnlPort.Enabled = false;
                }
                else
                {
                    btnStart.Text = "開始";
                    pnlPort.Enabled = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddValue(int pValue)
        {
            try
            {
                int v = (int)(nudPortNo.Value) * 10 + pValue;
                if (v > nudPortNo.Maximum)
                {
                    v = (int)(nudPortNo.Maximum);
                }
                nudPortNo.Value = v;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnPort0_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort1_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort2_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort3_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort4_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(4);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort5_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(5);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort6_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(6);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort7_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(7);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort8_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(8);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPort9_Click(object sender, EventArgs e)
        {
            try
            {
                AddValue(9);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPortC_Click(object sender, EventArgs e)
        {
            try
            {
                nudPortNo.Value = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPortR_Click(object sender, EventArgs e)
        {
            try
            {
                if (nudPortNo.Value < 49152)
                {
                    MessageBox.Show("49153～65535の番号帯を使うことをおすすめします", this.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ListenStart == true)
                {
                    // 停止
                    this.ListenStart = false;
                    this.Close();
                }
                else
                {
                    // 開始
                    if (nudPortNo.Value < 1024)
                    {
                        MessageBox.Show("指定できないポート番号です。", this.Text);
                    }
                    else if (nudPortNo.Value < 49152)
                    {
                        if (MessageBox.Show("他のアプリと競合するかもしれないポート番号です。\n開始しますか？", this.Text, MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            this.PortNo = (int)(nudPortNo.Value);
                            this.ListenStart = true;
                            this.Close();
                        }
                    }
                    else
                    {
                        this.PortNo = (int)(nudPortNo.Value);
                        this.ListenStart = true;
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
