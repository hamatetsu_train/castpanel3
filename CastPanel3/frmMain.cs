﻿using AtsPlugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Linq;
using System.Media;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HL.Bve;
using static System.Net.Mime.MediaTypeNames;
using static AtsPlugin.CastEngine;
using static AtsPlugin.EngineBase;

namespace CastPanel3
{
    internal class frmMain : Form
    {
        #region コントロール定義
        private PictureBox pbxCast;
        private SoundPlayer soundPlayerDeparture = null;
        private SoundPlayer soundPlayerNearStation = null;
        //private System.ComponentModel.IContainer components;

        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pbxCast = new System.Windows.Forms.PictureBox();
            this.pnlStaff = new System.Windows.Forms.Panel();
            this.pbxStaff = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCast)).BeginInit();
            this.pnlStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxStaff)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxCast
            // 
            this.pbxCast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxCast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxCast.Location = new System.Drawing.Point(0, 0);
            this.pbxCast.Name = "pbxCast";
            this.pbxCast.Size = new System.Drawing.Size(600, 800);
            this.pbxCast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCast.TabIndex = 0;
            this.pbxCast.TabStop = false;
            this.pbxCast.DoubleClick += new System.EventHandler(this.pbxCast_DoubleClick);
            this.pbxCast.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbxCast_MouseClick);
            // 
            // pnlStaff
            // 
            this.pnlStaff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStaff.AutoScroll = true;
            this.pnlStaff.BackColor = System.Drawing.Color.Black;
            this.pnlStaff.Controls.Add(this.pbxStaff);
            this.pnlStaff.Location = new System.Drawing.Point(0, 200);
            this.pnlStaff.Name = "pnlStaff";
            this.pnlStaff.Size = new System.Drawing.Size(600, 100);
            this.pnlStaff.TabIndex = 1;
            this.pnlStaff.Visible = false;
            this.pnlStaff.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlStaff_Scroll);
            this.pnlStaff.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlStaff_MouseMove);
            this.pnlStaff.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlStaff_MouseUp);
            // 
            // pbxStaff
            // 
            this.pbxStaff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxStaff.Location = new System.Drawing.Point(20, 0);
            this.pbxStaff.Name = "pbxStaff";
            this.pbxStaff.Size = new System.Drawing.Size(560, 100);
            this.pbxStaff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxStaff.TabIndex = 0;
            this.pbxStaff.TabStop = false;
            this.pbxStaff.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbxStaff_MouseDown);
            this.pbxStaff.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxStaff_MouseMove);
            // 
            // frmMain
            // 
            this.ClientSize = new System.Drawing.Size(600, 800);
            this.Controls.Add(this.pnlStaff);
            this.Controls.Add(this.pbxCast);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CastPanel3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pbxCast)).EndInit();
            this.pnlStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxStaff)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        #region 変数定義
        /// <summary>
        /// エンジン
        /// </summary>
        private AtsPlugin.CastEngine cast;
        /// <summary>
        /// 設定
        /// </summary>
        private AtsPlugin.Config CFG;

        /// <summary>
        /// 車両状態
        /// </summary>
        private AtsEngine.MyVehicleState vehicleState;
        /// <summary>
        /// 
        /// </summary>
        private bool isEnter = false;

        /// <summary>
        /// Cast動作中
        /// </summary>
        private bool isDisping = false;

        //private System.Media.SoundPlayer SoundPlayer = null;

        #region -通信関係
        private System.Net.Sockets.UdpClient udpClient = null;
        private int mPortNo = 49201;
        private System.Windows.Forms.Panel pnlStaff;
        private PictureBox pbxStaff;
        private bool mIsListening = false;
        #endregion

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public frmMain()
        {
            InitializeComponent();


        }

        #region イベント
        /// <summary>
        /// フォームロードイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                //EngineBase.Log.Init();

                CFG = new Config();
                CFG.LoadStaions();

                cast = new AtsPlugin.CastEngine(true);
                this.cast.LogInit();

                //音声用意
                //SoundPlayer = new System.Media.SoundPlayer(CastPanel2.Properties.Resources.CastCaution);
                if (System.IO.File.Exists(EngineBase.ModuleDirectoryPath + "\\departure.wav") == true)
                {
                    soundPlayerDeparture = new SoundPlayer();
                    soundPlayerDeparture.SoundLocation = EngineBase.ModuleDirectoryPath + "\\departure.wav";
                    soundPlayerDeparture.Load();
                }
                else
                {
                    soundPlayerDeparture = null;
                    this.cast.LogWrite("不明 " + EngineBase.ModuleDirectoryPath + "\\departure.wav");
                }

                if (System.IO.File.Exists(EngineBase.ModuleDirectoryPath + "\\near.wav") == true)
                {
                    soundPlayerNearStation = new SoundPlayer();
                    soundPlayerNearStation.SoundLocation = EngineBase.ModuleDirectoryPath + "\\near.wav";
                    soundPlayerNearStation.Load();
                }
                else
                {
                    soundPlayerNearStation = null;
                    this.cast.LogWrite("不明 " + EngineBase.ModuleDirectoryPath + "\\near.wav");
                }

                //this.cast.SetSoundPlayer(soundPlayerDeparture, soundPlayerNearStation);

                // veshiStateを更新する
                this.vehicleState = new AtsEngine.MyVehicleState();

                // テストデータ
                //cast.mBveStaff = new HL.Staff();
                cast.mBveStaff = new HL.Bve.Station();
                //cast.mBveStaff.StaffPack.StaffYomikomi("castimg\\station.txt");

                //cast.Init(cast.mBveStaff.StaffPack.Cars, null);
                //pbxCast.Image = cast.DrawCast(pbxCast.Size, 0, 0, 0, DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second);
                pbxCast.Image = MakeSampleImage(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// ファイルクローズ前イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //UdpClientを閉じる
                if (udpClient != null)
                {
                    udpClient.Close();
                }

                this.cast.LogWrite("[FromClosing]");
                this.cast.LogDispose();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region ピクチャボックス
        private void pbxCast_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                int mX = (int)((double)e.X / (double)pbxCast.Width * 600.0); 
                int mY = (int)((double)e.Y / (double)pbxCast.Height * 800.0);
                double tmpX = (double)e.X * 600.0 / (double)pbxCast.Width;
                double tmpY = (double)e.Y * 800.0 / (double)pbxCast.Height;
                this.cast.LogWrite("[Click] (" + tmpX.ToString("0") + ", " + tmpY.ToString("0") + ")");

                if (isDisping == true)
                {
                    if(InArea(new Rectangle(366, 751, 80, 35)) == true)
                    {
                        // 戻る
                        this.cast.PushNextButton(false);
                    }
                    else if (InArea(new Rectangle(459, 751, 115, 35)) == true)
                    {
                        // 次へ
                        this.cast.PushNextButton(true);
                    }

                    else if (this.cast.DispDetailCautionType == -1 && this.cast.DispDetailCautionReceived > 0 && InArea(new Rectangle(260, 700, 80, 20)) == true)
                    {
                        // (時刻表) 確認
                        pnlStaff.Visible = false;
                        CloseDetailCaution();
                    }

                    else if (this.cast.DispDetailCautionType > -1 && this.cast.DispDetailCautionCount > 0 && InArea(new Rectangle(70, 130, 600, 18)) == true)
                    {
                        // 通告詳細[0]
                        ClickDetailCaution(0);
                    }
                    else if (this.cast.DispDetailCautionType > -1 && this.cast.DispDetailCautionCount > 1 && InArea(new Rectangle(70, 148, 600, 18)) == true)
                    {
                        // 通告詳細[1]
                        ClickDetailCaution(1);
                    }
                    else if (this.cast.DispDetailCautionType > -1 && this.cast.DispDetailCautionCount > 2 && InArea(new Rectangle(70, 166, 600, 18)) == true)
                    {
                        // 通告詳細[2]
                        ClickDetailCaution(2);
                    }
                    else if (this.cast.DispDetailCautionType > -1 && this.cast.DispDetailCautionCount > 3 && InArea(new Rectangle(70, 184, 600, 18)) == true)
                    {
                        // 通告詳細[3]
                        ClickDetailCaution(3);
                    }
                    else if (this.cast.DispDetailCautionType > -1 && this.cast.DispDetailCautionCount > 4 && InArea(new Rectangle(70, 202, 600, 18)) == true)
                    {
                        // 通告詳細[4]
                        ClickDetailCaution(4);
                    }
                    else if (this.cast.DispDetailCautionType > -1 && this.cast.DispDetailCautionCount > 0 && InArea(new Rectangle(230, 233, 140, 20)) == true)
                    {
                        // 通告詳細[受領]
                        CloseDetailCaution();
                    }

                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount > 0 && InArea(new Rectangle(14, 107, 106, 156)) == true)
                    {
                        // 通告一覧[0]
                        ClickCaution(0);
                    }
                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount > 1 && InArea(new Rectangle(134, 107, 106, 156)) == true)
                    {
                        // 通告一覧[1]
                        ClickCaution(1);
                    }
                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount > 2 && InArea(new Rectangle(254, 107, 106, 156)) == true)
                    {
                        // 通告一覧[2]
                        ClickCaution(2);
                    }
                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount > 3 && InArea(new Rectangle(374, 107, 106, 156)) == true)
                    {
                        // 通告一覧[3]
                        ClickCaution(3);
                    }
                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount > 4 && InArea(new Rectangle(494, 107, 106, 156)) == true)
                    {
                        // 通告一覧[4]
                        ClickCaution(4);
                    }

                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount == 0 && InArea(new Rectangle(14, 150, 24, 24)) == true)
                    {
                        // 時刻表
                        this.ClickCaution(-1);
                    }

                    else if (this.cast.DispDetailCautionCount == 0 && this.cast.ValidCautionCount > 0 && InArea(new Rectangle(14, 280, 24, 24)) == true)
                    {
                        // 時刻表
                        this.ClickCaution(-1);
                    }

                    else if (this.cast.ValidCautionCount == 0 && InArea(new Rectangle(54, 180, 88, 56)) == true)
                    {
                        // スタフ 5つ次の駅
                        this.cast.SetStaionIndex(this.cast.StationIndex + 5);
                    }
                    else if (InArea(new Rectangle(54, 270, 88, 56)) == true)
                    {
                        // スタフ 4つ次の駅
                        this.cast.SetStaionIndex(this.cast.StationIndex + 4);
                    }
                    else if (InArea(new Rectangle(54, 360, 88, 56)) == true)
                    {
                        // スタフ 3つ次の駅
                        this.cast.SetStaionIndex(this.cast.StationIndex + 3);
                    }
                    else if (InArea(new Rectangle(54, 450, 88, 56)) == true)
                    {
                        // スタフ 2つ次の駅
                        this.cast.SetStaionIndex(this.cast.StationIndex + 2);
                    }
                    else if (InArea(new Rectangle(54, 540, 88, 56)) == true)
                    {
                        // スタフ 1つ次の駅
                        this.cast.SetStaionIndex(this.cast.StationIndex + 1);
                    }
                    else if (InArea(new Rectangle(54, 630, 88, 56)) == true)
                    {
                        // スタフ この駅
                    }
                    else if (InArea(new Rectangle(54, 720, 88, 56)) == true)
                    {
                        // スタフ 1つ前の駅
                        this.cast.SetStaionIndex(this.cast.StationIndex - 1);
                    }

                    // Top=90- (this.mBveStaff.StaffPack.StationList.Count * 90) + 90*this.mStationIndex
                    // クリック可能 (143 or 273)～
                    // 54～+88, 630-90*i～+56
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // 座標が範囲内にあるか
            bool InArea(Rectangle rg)
            {
                double tmpX = (double)e.X * 600.0 /(double)pbxCast.Width;
                double tmpY = (double)e.Y * 800.0 / (double)pbxCast.Height;
                if (rg.Left <= tmpX && tmpX<=rg.Left+rg.Width && rg.Top<= tmpY && tmpY <= rg.Top + rg.Height)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private void pbxCast_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if(this.WindowState != FormWindowState.Maximized)
                {
                    // 枠なし最大化して設定画面を出す
                    this.FormBorderStyle = FormBorderStyle.None;
                    this.WindowState = FormWindowState.Maximized;
                    using(frmConfig f = new frmConfig())
                    {
                        f.PortNo = this.mPortNo;
                        f.ListenStart = this.mIsListening;
                        f.ShowDialog();
                        this.mPortNo = f.PortNo;
                        if(this.mIsListening != f.ListenStart)
                        {
                            this.mIsListening = f.ListenStart;
                            if (f.ListenStart == true)
                            {
                                pbxCast.Image = MakeSampleImage(1);
                                // リッスンスタート
                                RecieveStart();
                                this.cast.LogWrite("[開始] port=" + this.mPortNo.ToString(""));
                            }
                            else
                            {
                                // 停止
                                if (this.udpClient != null)
                                {
                                    this.udpClient.Close();
                                    this.cast.LogWrite("[停止]");
                                }
                            }
                        }
                    }
                }
                else
                {
                    // 標準モードに戻す
                    this.FormBorderStyle = FormBorderStyle.Sizable;
                    this.WindowState = FormWindowState.Normal;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region スタフパネル
        // https://ja.stackoverflow.com/questions/6632/windowsフォームアプリにおけるスワイプによるスクロールについて
        private int _PreviousY;
        private void pnlStaff_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (pnlStaff.Capture)
                {
                    // パネルのドラッグ処理
                    var y = pnlStaff.PointToScreen(e.Location).Y;
                    pnlStaff.VerticalScroll.Value = Math.Min(
                                                        Math.Max(
                                                            pnlStaff.VerticalScroll.Minimum,
                                                            pnlStaff.VerticalScroll.Value + _PreviousY - y),
                                                            pnlStaff.VerticalScroll.Maximum);
                    _PreviousY = y;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pnlStaff_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                pnlStaff.Capture = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pbxStaff_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                // ボタンがクリックされた場合
                var c = (Control)sender;

                c.Capture = true;

                _PreviousY = c.PointToScreen(e.Location).Y;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void pbxStaff_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                var c = (Control)sender;
                if (c.Capture)
                {
                    // ボタンが押された状態である程度動いたらpanel1にイベントを付け替える
                    var y = pnlStaff.PointToScreen(e.Location).Y;
                    if (Math.Abs(y - _PreviousY) > 3)
                    {
                        c.Capture = false;
                        pnlStaff.Capture = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #endregion

        #region データ受信
        //データ受信の待機を開始する
        private void RecieveStart()
        {
            if (udpClient != null)
            {
                //Return
                udpClient.Close();
            }

            //UdpClientを作成し、指定したポート番号にバインドする
            System.Net.IPEndPoint localEP = new System.Net.IPEndPoint(System.Net.IPAddress.Any, (int)(this.mPortNo));
            udpClient = new System.Net.Sockets.UdpClient(localEP);
            // 非同期的なデータ受信を開始する
            udpClient.BeginReceive(new AsyncCallback(ReceiveCallback), udpClient);
        }

        //データを受信した時
        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                System.Net.Sockets.UdpClient udp = (System.Net.Sockets.UdpClient)(ar.AsyncState);

                //非同期受信を終了する
                System.Net.IPEndPoint remoteEP = null;
                byte[] rcvBytes;
                try
                {
                    rcvBytes = udp.EndReceive(ar, ref remoteEP);
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    Console.WriteLine("受信エラー({0}/{1})", ex.Message, ex.ErrorCode);
                    return;
                }
                catch //(ObjectDisposedException ex) 
                {
                    //すでに閉じている時は終了
                    Console.WriteLine("Socketは閉じられています。");
                    return;
                }


                //データを文字列に変換する
                string rcvMsg = System.Text.Encoding.UTF8.GetString(rcvBytes);
                rcvMsg = System.Uri.UnescapeDataString(rcvMsg);

                //受信したデータと送信者の情報をRichTextBoxに表示する
                //Dim displayMsg As String = String.Format("[{0} ({1})] > {2}", remoteEP.Address, remoteEP.Port, rcvMsg)
                string displayMsg = String.Format("{0}", rcvMsg);
                pbxCast.BeginInvoke(new Action<string>(RenewCastPanel), displayMsg);
#if DEBUG
                //Console.WriteLine(displayMsg);
#endif

                //再びデータ受信を開始する
                udp.BeginReceive(new AsyncCallback(ReceiveCallback), udp);


            }
            catch (Exception ex)
            {
                Console.Write("RC : " + ex.Message);
            }
        }

        #endregion


        #region 受信データ解析
        private void RenewCastPanel(string text)
        {
            ComSchema cs = null;
            try
            {
                if (text == "") return;

                //JSONをクラスに変換 ※クラスは出力したJSONを貼り付けて作成し、Listにデシリアライズする
                JsonSerializerOptions jso = new JsonSerializerOptions();
                jso.AllowTrailingCommas = true;
                jso.ReadCommentHandling = JsonCommentHandling.Skip;
                jso.NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.WriteAsString;
                jso.WriteIndented = true;
                jso.Encoder = JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All);

                cs = JsonSerializer.Deserialize<ComSchema>(text, jso);

                if (cs == null)
                {

                }
                else
                {
                    if (cs.Beacon != null && cs.Beacon.Count() > 0)
                    {
                        SetBeaconData(cs);
                    }

                    if (cs.Stations != null)
                    {
                        // 駅リスト
                        bool staffAri = true;
                        this.cast.mBveStaff.LoadFromData(cs.Stations.Replace("\\n", "\n"), ref staffAri);
                        this.cast.mBveStaff.StationList = this.cast.mBveStaff.SortStaffStation();
                        this.cast.mBveStaff.CalcGapTime();
                        if(cs.StationLocs != null && cs.StationLocs.Count() > 0)
                        {
                            foreach (StationLocSchema stl in cs.StationLocs)
                            {
                                this.cast.mBveStaff.SetLocation(stl.Key, stl.Location);
                            }
                        }
                        if (this.cast.mBveStaff != null && this.cast.mBveStaff.StaffHeader != null)
                        {
                            this.cast.Init(this.cast.mBveStaff.StaffHeader.Cars, this.CFG.Stations);
                        }
                        else
                        {
                            this.cast.Init(2, this.CFG.Stations);
                        }

                        // デフォルト通告
                        CautionData cautionData = new CautionData();
                        cautionData.Received = 0;
                        cautionData.Type = -1; // 時刻表確認
                        cautionData.Title = "変更・注意事項";
                        cautionData.Number = 0;
                        cautionData.Caution = "確認";
                        cautionData.DispStart = 0;

                        // 時刻表画像
                        HL.Staff sf = new HL.Staff();
                        sf.StaffPack.LoadFromStations(this.cast.mBveStaff);
                        Bitmap sbm = sf.DrawStaff2();
                        if(sbm != null && sbm.Width != 0 && pbxStaff.Width != 0)
                        {
                            pbxStaff.Height = (int)((double)sbm.Height * (double)pbxStaff.Width / (double)sbm.Width);
                            if(pbxStaff.Height < pnlStaff.Height)
                            {
                                //cautionData.Received = 1;
                            }
                            cautionData.Received = 1;
                            pbxStaff.Image = sbm;
                        }

                        this.cast.SetCaution(cautionData);

                        cautionData = new CautionData();
                        cautionData.Received = 0;
                        cautionData.Type = 1; // 徐行
                        cautionData.Title = "計画徐行";
                        cautionData.Number = 0;
                        cautionData.Caution = "なし";
                        cautionData.DispStart = 0;
                        this.cast.SetCaution(cautionData);
                    }
                    else if(cs.Caution != null)
                    {
                        // 通告
                        foreach(CautionSchema cautionSchema in cs.Caution)
                        {
                            CautionData cautionData = new CautionData(cautionSchema);
                            cautionData.Received = 0;
                            this.cast.SetCaution(cautionData);
                        }
                    }
                    else if (cs.Doors != null)
                    {
                        // ドア開閉
                        this.cast.SetDoorOpen(cs.Doors.Open);
                    }
                    else
                    {
                        // フレーム
                        // 駅接近時に音を鳴らす
                        //if (this.cast.NearStation == 1)
                        //{
                        //    if (isEnter == true && SoundPlayer != null)
                        //    {
                        //        SoundPlayer.Play();
                        //    }
                        //}

                        //if (cs.StopStation == 1)
                        //{
                        //    this.StopMode = StopModeEnum.Near;
                        //    //Call pnlStop_Change()
                        //    //pnlStop.Visible = True
                        //}

                        //初回起動時は出場パネルを表示
                        if (isEnter == false)
                        {
                            isEnter = true;
                            //ucEnter1.Visible = True
                        }

                        // veshiStateを更新する
                        AtsDefine.AtsVehicleState avs;
                        avs.BcPressure = 0;
                        avs.BpPressure = 0;
                        avs.Current = 0;
                        avs.ErPressure = 0;
                        avs.Location = cs.Location;
                        avs.MrPressure = 0;
                        avs.SapPressure = 0;
                        avs.Speed = (float)cs.Speed;
                        avs.Time = cs.Time * 1000; // ms
                        vehicleState.SetState(avs);

                        this.cast.Elapse(vehicleState);
                        // CAST画像を更新する                        
                        pbxCast.Image = cast.DrawCast(pbxCast.Size, cs.Location, cs.PanelLocation, cs.Speed, cs.Time); //, cs.StaId);
                        // サウンド
                        //if (this.cast.SoundFirstDeparture == AtsDefine.AtsSoundControlInstruction.Play || this.cast.SoundFirstDeparture == AtsDefine.AtsSoundControlInstruction.Stop)
                        //{
                        //    Task t1 = Task.Run(() => { PlaySoundDeparture(); });
                        //}
                        if (this.soundPlayerDeparture != null && cs.Speed == 0) // && this.soundPlayerDeparture.IsLoadCompleted == true)
                        {
                            if (this.cast.SoundFirstDeparture == AtsDefine.AtsSoundControlInstruction.Play)
                            {
#if DEBUG
                                this.cast.LogWrite("FORM PLAY");
#endif
                                this.soundPlayerDeparture.PlaySync();
                            }
                            else if (this.cast.SoundFirstDeparture == AtsDefine.AtsSoundControlInstruction.Stop)
                            {
#if DEBUG
                                this.cast.LogWrite("FORM STOP");
#endif
                                this.soundPlayerDeparture.Stop();
                            }
                            else
                            {
#if DEBUG
                                this.cast.LogWrite("FORM ELSE");
#endif
                            }
                        }

                        if (this.soundPlayerNearStation != null) // && this.soundPlayerNearStation.IsLoadCompleted == true)
                        {
                            if (this.cast.SoundNearStation == AtsDefine.AtsSoundControlInstruction.Play)
                            {
                                this.soundPlayerNearStation.PlaySync();
                            }
                            else if (this.cast.SoundNearStation == AtsDefine.AtsSoundControlInstruction.Stop)
                            {
                                this.soundPlayerNearStation.Stop();
                            }
                        }

                        isDisping = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "RenewCastPanel");
                Console.Write(ex.Message);
                //return null;
            }
        }

        private void PlaySoundDeparture()
        {
            if (this.soundPlayerDeparture != null) // && this.soundPlayerDeparture.IsLoadCompleted == true)
            {
                if (this.cast.SoundFirstDeparture == AtsDefine.AtsSoundControlInstruction.Play)
                {
#if DEBUG
                                this.cast.LogWrite("FORM PLAY");
#endif
                    this.soundPlayerDeparture.Play();
                }
                else if (this.cast.SoundFirstDeparture == AtsDefine.AtsSoundControlInstruction.Stop)
                {
#if DEBUG
                                this.cast.LogWrite("FORM STOP");
#endif
                    this.soundPlayerDeparture.Stop();
                    this.soundPlayerDeparture.Tag = null;
                }
                else
                {
#if DEBUG
                                this.cast.LogWrite("FORM ELSE");
#endif
                    this.soundPlayerDeparture.Tag = null;
                }
            }
        }
        #endregion

        #region SetBeaconData
        /// <summary>
        /// 地上子通過
        /// </summary>
        /// <param name="cs">受信データ</param>
        public void SetBeaconData(ComSchema cs)
        {
            try
            {
                if (cs.Beacon != null && cs.Beacon.Count() > 0)
                {
                    foreach (BeaconSchema beacon in cs.Beacon)
                    {
                        AtsDefine.AtsBeaconData beaconData;
                        beaconData.Type = beacon.Type;
                        beaconData.Distance = (float)beacon.Distance;
                        beaconData.Signal = beacon.Signal;
                        beaconData.Optional = beacon.Optional;

                        this.cast.SetBeaconData(vehicleState, beaconData);
                    }
                }
            }
            catch //(Exception ex)
            {
                //Log.Write("[Cast.SetBeaconData] : " + ex.Message);
            }
        }

        #endregion


        #region サンプル画像
        private Bitmap MakeSampleImage(int pMode)
        {
            Bitmap bmp = new Bitmap(600,800);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if(pMode == 0)
                {
                    g.FillRectangle(Brushes.White, new RectangleF(0, 0, 600, 800));
                    HL.GraOpe.g_mojiInzi("表示エリアをダブルクリックして\n" +
                                         "設定画面を開いてください。", 
                                         g, new Font("MS ゴシック", 12), new Point(200, 350), new Size(200, 100), Brushes.Black, HL.GraOpe.ENUM_TextAlign.CenterMiddle);
                }
                else if(pMode == 1)
                {
                    g.FillRectangle(Brushes.White, new RectangleF(0, 0, 600, 800));
                    HL.GraOpe.g_mojiInzi("信号を待っています。\n" +
                                         "表示エリアをダブルクリックすると元のサイズに戻ります\n" +
                                         "終了する際は元のサイズに戻して右上のバツボタンを押してください。", 
                                         g, new Font("MS ゴシック", 12), new Point(200, 350), new Size(200, 100), Brushes.Black, HL.GraOpe.ENUM_TextAlign.CenterMiddle);
                }
            }

            return bmp;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Resize(object sender, EventArgs e)
        {
            try
            {
                pnlStaff.Top = (int)(180.0 * (double)this.Height / 800.0);
                pnlStaff.Height = (int)(480.0 * (double)this.Height / 800.0);
                pbxStaff.Left = (int)(pnlStaff.Width * 0.1);
                pbxStaff.Width = (int)(pnlStaff.Width * 0.8);
            }
            catch
            {

            }
        }

        private void pnlStaff_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                if (e.ScrollOrientation == ScrollOrientation.VerticalScroll && e.Type == ScrollEventType.Last)
                {
                    CloseDetailCaution(); // 確認ボタンを表示させる
                }
                Console.WriteLine(e.Type.ToString());
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void CloseDetailCaution()
        {
            try
            {
                if(this.cast != null)
                {
                    this.cast.CloseDetailCaution();
                    pbxCast.Image = this.cast.DrawCast(pbxCast.Size);
                }
            }
            catch
            {

            }
        }

        private void ClickDetailCaution(int index)
        {
            try
            {
                if (this.cast != null)
                {
                    this.cast.ClickDetailCaution(index);
                    pbxCast.Image = this.cast.DrawCast(pbxCast.Size);
                }
            }
            catch
            {

            }
        }

        private void ClickCaution(int index)
        {
            try
            {
                if (this.cast != null)
                {
                    this.cast.ClickCaution(index);
                    pbxCast.Image = this.cast.DrawCast(pbxCast.Size);

                    if (this.cast.DispDetailCautionType == -1)
                    {
                        // 時刻表モード
                        pnlStaff.Visible = true;
                        pnlStaff.VerticalScroll.Value = 0;
                    }
                }
            }
            catch
            {

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(this.cast != null)
            {

            }
        }
    }
}
