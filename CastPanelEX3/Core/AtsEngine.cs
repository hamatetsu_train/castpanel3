﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
//using System.Threading.Tasks;
using System.Xml.Linq;
using System.Windows.Forms;

using Zbx1425.DXDynamicTexture;
using HL;
using System.Text.Json.Serialization;

namespace AtsPlugin
{
    public class AtsEngine : EngineBase
    {
        #region DLL使用宣言
        // キーボード状態の取得
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32", SetLastError = true)]
        internal static extern bool FreeLibrary(IntPtr hModule);

        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = false)]
        internal static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

        // DXDT

        #endregion

        #region DXDT変数
        private static TextureHandle hCastTex;
        private static GDIHelper gCast;
        private static Bitmap imgCast;

        #endregion

        public static AtsEngine ATS = new AtsEngine(0);

        #region 定義

        #region - 変数定義
        /// <summary>
        /// 列車状態
        /// </summary>
        private MyVehicleState pMyVeshicleState;

        /// <summary>
        /// 入力ハンドル値
        /// </summary>
        MyAtsHandles InHandles;

        /// <summary>
        /// 出力ハンドル値
        /// </summary>
        AtsDefine.AtsHandles OutHandles;

        /// <summary>
        /// 設定
        /// </summary>
        internal Config CFG;

        /// <summary>
        /// 停車場データ
        /// </summary>
        //HL.Bve.Station mStation;

        /// <summary>
        /// 停車場距離程リストデータ
        /// </summary>
        List<HL.Bve.Map.Station> lStationLoc;

        /// <summary>
        /// キーボード状態
        /// </summary>
        private short[] KeyStates;

        private CastEngine Cast;

        private bool IsDisposing;

        // 外部送信
        private UdpCom CastCom = null;

        private int mStationIndex = -1;
        private int mOldStationIndex = -1;

        // 未送信の通告データ
        private List<CautionSchema> UncommitedCautionData;
        #endregion

        #region - クラス

        #region -- ハンドル位置保存クラス
        private class MyAtsHandles
        {
            #region 変数・プロパティ定義
            public int Power;
            public int Brake;
            public int Reverser;
            public int ConstantSpeed;

            private int pEmgBrake;

            public bool IsEmgBrake
            {
                get { if (this.Brake == this.pEmgBrake) return true; else return false; }
            }
            #endregion

            public AtsDefine.AtsHandles Handles
            {
                get { return new AtsDefine.AtsHandles() { Power = this.Power, Brake = this.Brake, Reverser = this.Reverser, ConstantSpeed = this.ConstantSpeed }; }
                set { this.Power = value.Power; this.Brake = value.Brake; this.Reverser = value.Reverser; this.ConstantSpeed = value.ConstantSpeed; }
            }

            #region コンストラクタ
            /// <summary>
            /// コンストラクタ
            /// </summary>
            public MyAtsHandles()
            {
                this.Power = 0;
                this.Brake = 0;
                this.Reverser = 0;
                this.ConstantSpeed = 0;

            }
            public MyAtsHandles(int _EmgBrake, int _InitialHandle)
            {
                this.Power = 0;
                if (_InitialHandle == 0)
                {
                    // SVC
                    this.Brake = 0;
                }
                else
                {
                    this.Brake = _EmgBrake;
                }
                this.Reverser = 0;
                this.ConstantSpeed = 0;
                this.pEmgBrake = _EmgBrake;
            }
            #endregion

        }
        #endregion

        #region -- 車両状態保存クラス
        public class MyVehicleState
        {
            #region 変数定義
            private bool pIsFirst = true;

            private AtsDefine.AtsVehicleState pNewState;
            public AtsDefine.AtsVehicleState NewState
            {
                get { return this.pNewState; }
            }

            private AtsDefine.AtsVehicleState pOldState;

            private bool pPilotLamp;
            public bool PilotLamp
            {
                get { return this.pPilotLamp; }
            }

            private double pLocation;
            public double Location
            {
                get { return Math.Abs(this.pLocation); }
            }

            /// <summary>
            /// 加速度[km/h/s]
            /// </summary>
            private float pAccelaration;
            /// <summary>
            /// 加速度[km/h/s]
            /// </summary>
            public float Accelaration
            {
                get { return this.pAccelaration; }
            }

            private float pOldAccelaration;

            public double pExNowTime { get; set; }
            public double pExOldTime { get; set; }
            #endregion

            #region コンストラクタ
            public MyVehicleState()
            {
                this.pIsFirst = true;
                this.pNewState = new AtsDefine.AtsVehicleState();
                this.pOldState = new AtsDefine.AtsVehicleState();
                this.pPilotLamp = true;
                this.pAccelaration = 0;
                this.pOldAccelaration = 0;
                this.pExNowTime = 0;
                this.pExOldTime = 0;
            }
            #endregion

            #region セット
            public void SetState(AtsDefine.AtsVehicleState vehicleState)
            {
                if (this.pIsFirst == true)
                {
                    this.pOldState = vehicleState;
                    this.pIsFirst = false;
                }
                else
                {
                    this.pOldState = this.pNewState;
                    this.pOldAccelaration = this.pAccelaration;
                    this.pAccelaration = this.pNewState.Speed - this.pOldState.Speed;
                }

                this.pNewState = vehicleState;
                this.pLocation += this.GetDeltaLocation();
            }

            /// <summary>
            /// BVE側の戸閉状態を更新
            /// </summary>
            /// <param name="_IsClose"></param>
            public void SetDoorClose(bool _IsClose)
            {
                this.pPilotLamp = _IsClose;
            }

            public void SetLocation(double _Location)
            {
                this.pLocation = _Location;
            }

            #endregion

            #region Get
            /// <summary>
            /// 前フレームとの時間差[ms]
            /// </summary>
            /// <returns></returns>
            public int GetDeltaTime()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (this.pNewState.Time - this.pOldState.Time + 60 * 60 * 24 * 1000) % (60 * 60 * 24 * 1000);
                }
            }

            /// <summary>
            /// 前フレームとの距離差[m]
            /// </summary>
            /// <returns></returns>
            public float GetDeltaLocation()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (float)(this.pNewState.Location - this.pOldState.Location);
                }
            }

            /// <summary>
            /// 前フレームとの加速度変化量[km/h/s2]
            /// </summary>
            /// <returns></returns>
            public float GetDeltaAccelaration()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (float)(this.pAccelaration - this.pOldAccelaration);
                }
            }

            /// <summary>
            /// 前回と同じ時間帯か
            /// </summary>
            /// <param name="_Interval">時間帯間隔[ms]</param>
            /// <returns></returns>
            public bool GetAnotherMoment(int _Interval)
            {
                if (_Interval > 0)
                {
                    if ((int)(this.pNewState.Time / _Interval) == (int)(this.pOldState.Time / _Interval))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// 指定した間隔の前半か後半か (前半ならtrue)
            /// </summary>
            /// <param name="_Interval">時間帯間隔[ms]</param>
            /// <returns></returns>
            public bool GetOddInterval(int _Interval)
            {
                if (_Interval > 1)
                {
                    if ((this.pNewState.Time % _Interval) < (int)(_Interval / 2))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }

            /// <summary>
            /// 時を取得
            /// </summary>
            /// <returns></returns>
            public int GetHour()
            {
                return (int)(this.pNewState.Time / 3600000);
            }

            /// <summary>
            /// 分を取得
            /// </summary>
            /// <returns></returns>
            public int GetMinute()
            {
                return (int)((int)(this.pNewState.Time / 1000) / 60) % 60;
            }

            /// <summary>
            /// 秒を取得
            /// </summary>
            /// <returns></returns>
            public int GetSecond()
            {
                return (int)(this.pNewState.Time / 1000) % 60;
            }

            public int GetBc()
            {
                return (int)(this.pNewState.BcPressure);
            }

            public int GetMr()
            {
                return (int)(this.pNewState.MrPressure);
            }

            #endregion
        }
        #endregion

        #endregion

        #endregion


        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="logNo">0:自身の静的用 1:AtsExコンストラクタから</param>
        public AtsEngine(int logNo)
        {
            try
            {
                // ログ初期化
                Log.Init();
                Log.Write("[Construct](" + logNo.ToString() + ")");

                // 初期化
                Init();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainConstruct]", true);
            }
        }
        #endregion

        #region 初期化
        public void Init()
        {
            try
            {
#if DEBUG
                Log.Write("[Init] - Start");
#endif
                this.pMyVeshicleState = new MyVehicleState();
                this.InHandles = new MyAtsHandles();
                this.OutHandles = new AtsDefine.AtsHandles() { Power = 0, Brake = 0, ConstantSpeed = AtsDefine.AtsCscInstruction.Continue, Reverser = 0 };

                this.CFG = new Config();

                this.KeyStates = new short[256];
                this.IsDisposing = false;

                this.Cast = new CastEngine(false);
                this.UncommitedCautionData = new List<CautionSchema>();

#if DEBUG
                Log.Write("[Init] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainInit]", true);
            }
        }
        #endregion


        // _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/
        // ココカラ
        // _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/

        #region Load
        public void Load(string pRoutePath)
        {
            string logText = "";
            try
            {
#if DEBUG
                Log.Write("[Load] - Start : " + pRoutePath);
#else
                Log.Write("[Load] " + pRoutePath);
#endif
                // 設定ロード
                if (this.CFG.MainLoad() == true)
                {
                    Log.Write("　・ImageName=" + this.CFG.Cast.ImageName);
                    Log.Write("　・DxSize=(" + this.CFG.Cast.DxSize.Width.ToString() + "," + this.CFG.Cast.DxSize.Height.ToString() + ")");
                    if (this.CFG.Cast.Deformation == true)
                    {
                        Log.Write("　・DeformationPoint(cnt)=" + this.CFG.Cast.DeformationPoint.Length.ToString());
                    }
                    if (this.CFG.Cast.UseCom == true)
                    {
                        Log.Write("　・HostName=" + this.CFG.Cast.HostName);
                        Log.Write("　・ToPort=" + this.CFG.Cast.ToPort.ToString());
                    }
                }
                else
                {
                    Log.Write("[Load] - 失敗");
                }
                this.IsDisposing = false;

                if (this.CFG.Cast.ImageName != "")
                {
                    // DXDT処理
                    //TextureManager.Initialize();
                    // この画像が使われているオブジェクトを探す:path, 置換後のオブジェクトのサイズ(2^n, 2^m)
                    hCastTex = TextureManager.Register(this.CFG.Cast.ImageName, this.CFG.Cast.DxSize.Width, this.CFG.Cast.DxSize.Height);
                    // 
                    gCast = new GDIHelper(this.CFG.Cast.DxSize.Width, this.CFG.Cast.DxSize.Height);
                }

                //停車場リスト
                logText = "LoadStationList";
                Log.Write("　・MapLoad");
                this.lStationLoc = new List<Bve.Map.Station>();
                string stl = LoadStationList(pRoutePath);

                //Log.Write("　・StaffLoad");
                //this.Cast.mBveStaff = new Staff();
                //this.Cast.mBveStaff.StaffPack.LoadFromData(stl.Replace("\\n","\n"));
                Log.Write("　　・Staff.StaCount=" + this.Cast.mBveStaff.StationList.Count.ToString());
                Log.Write("　　・Staff.Sta.Location");
                for (int i = 0; i <this.Cast.mBveStaff.StationList.Count; i++)
                {
                    Log.Write("　　　・[" + i.ToString("000") + "] " + this.Cast.mBveStaff.StationList[i].Location.ToString());
                }

                logText = "Com";
                if (this.CFG.Cast.UseCom == true)
                {
                    this.CastCom = new UdpCom();
                    this.CastCom.SendInit(this.CFG.Cast.HostName, this.CFG.Cast.MyPort, this.CFG.Cast.ToPort);
                    //this.CastCom.Send(this.CFG.StationTxt);
                    //停車場リスト
                    this.CastCom.Send(stl);
#if DEBUG
                    Log.Write("[Load] - Send:" + stl);
#endif
                }
                else
                {
                    this.CastCom = null;
                }

                // 保存上書き
                logText = "Save";
                this.CFG.Save();

                // 初期化
                if (this.Cast != null)
                {
                    this.Cast.Init(0, this.CFG.Stations);
                }
#if DEBUG
                Log.Write("[Load] - End");
#endif

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainLoad] " + logText, true);
            }
        }
#endregion

        #region Dispose
        public void Dispose()
        {
            try
            {
#if DEBUG
                Log.Write("[MainDispose] - Start");
#endif
                this.IsDisposing = true;

                // DXDT

                //TextureManager.Dispose();

                if (hCastTex != null)
                {
                    hCastTex.Dispose(); // 入力デバイス版は必要
                }
                if (imgCast != null)
                {
                    imgCast.Dispose();
                }
                if (gCast != null)
                {
                    gCast.Dispose();
                }

                if (this.Cast != null)
                {
                    this.Cast.Dispose();
                }

                if (this.CastCom != null)
                {
                    this.CastCom.End();
                    this.CastCom = null;
                }

                // ログ片付け
                Log.Write("[MainDispose]");
                Log.Dispose();

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainDispose]", true);
            }
        }
        #endregion

        #region SetVehicleSpec
        public void SetVehicleSpec(AtsDefine.AtsVehicleSpec vehicleSpec)
        {
            try
            {
#if DEBUG
                Log.Write("[SetVehicleSpec] - Start");
#endif
                // 自分用に保存
                this.CFG.VehicleSpec.SetSpec(vehicleSpec);

                // 初期化
                this.OutHandles = new AtsDefine.AtsHandles() { Power = 0, Brake = this.CFG.VehicleSpec.EmgBrakeNotch, ConstantSpeed = AtsDefine.AtsCscInstruction.Continue, Reverser = 0 };

                if (this.Cast != null)
                {
                    this.Cast.SetCars(vehicleSpec.Cars);
                }
#if DEBUG
                Log.Write("[SetVehicleSpec] - End");
#endif

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetVehicleSpec]", true);
            }
        }
        #endregion

        #region Initialize
        public void Initialize(int initialHandlePosition)
        {
            try
            {
#if DEBUG
                Log.Write("[Initialize] - Start : " + initialHandlePosition.ToString());
#endif

#if DEBUG
                Log.Write("[Initialize] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainInitialize]", true);
            }
        }
        #endregion

        #region Elapse

        public AtsDefine.AtsHandles Elapse(AtsDefine.AtsVehicleState vehicleState, IntPtr panel, IntPtr sound)
        {
            string logtext = "";
            try
            {
#if DEBUG
                //Log.Write("[Elapse] - Start");
#endif
                // 列車状態
                logtext = "SetState";
                this.pMyVeshicleState.SetState(vehicleState);

                // キーボード入力チェック
                //this.CheckKeyState();

                // いまの駅番号(扉開の場合は+1 -> やめる mStationIndexをそのまま使う)
                //int pStationIndex = this.mStationIndex;
                //if (this.pMyVeshicleState.PilotLamp == false)
                //{
                //    pStationIndex++;
                //}

                // ハンドル値 (このプラグインでは制御しないのでそのまま渡す)
                this.OutHandles = this.InHandles.Handles;

                logtext = "hCastTex";
                if (this.pMyVeshicleState.GetAnotherMoment(1000) == false)
                {
                    if(hCastTex.IsCreated == true)
                    {
#if DEBUG
                        Log.Write("[Elapse] - スタフ生成開始");
#endif
                        // 変形
                        logtext = "Deformation";
                        if (this.CFG.Cast.Deformation == true)
                        {
                            // 変形あり
                            logtext = "変形あり";
#if TOUCH
                            Bitmap tmpbit = this.Cast.DrawCast(this.CFG.Cast.ImageSize, (int)this.pMyVeshicleState.NewState.Location, (int)this.pMyVeshicleState.Location,
                                                               vehicleState.Speed, (int)(vehicleState.Time / 1000)); // Bitmapを生成(枠 349*1242, 画像 512*2048)
#else
                            Bitmap tmpbit = this.Cast.DrawCast(this.CFG.Cast.ImageSize, (int)this.pMyVeshicleState.NewState.Location, (int)this.pMyVeshicleState.Location,
                                                               vehicleState.Speed, (int)(vehicleState.Time / 1000), this.mStationIndex); // Bitmapを生成(枠 349*1242, 画像 512*2048)
#endif
                            // 非同期生成にしたのでここでは処理できない
                            imgCast = null;
                            //Point[] startP = new Point[4];
                            //startP[0] = new Point(0, 0);
                            //startP[1] = new Point(0, this.CFG.Cast.ImageSize.Height);
                            //startP[2] = new Point(this.CFG.Cast.ImageSize.Width, this.CFG.Cast.ImageSize.Height);
                            //startP[3] = new Point(this.CFG.Cast.ImageSize.Width, 0);
                            //imgCast = HL.GraOpe.Homography(tmpbit, this.CFG.Cast.DxSize, startP, this.CFG.Cast.DeformationPoint, this.CFG.Cast.HomographyMatrix);
                        }
                        else
                        {
                            // 変形なし
                            logtext = "変形なし";
#if TOUCH
                            imgCast = this.Cast.DrawCast(this.CFG.Cast.ImageSize, (int)this.pMyVeshicleState.NewState.Location, (int)this.pMyVeshicleState.Location,
                                                         vehicleState.Speed, (int)(vehicleState.Time / 1000)); // Bitmapを生成(枠 349*1242, 画像 512*2048)
#else
                            imgCast = this.Cast.DrawCast(this.CFG.Cast.ImageSize, (int)this.pMyVeshicleState.NewState.Location, (int)this.pMyVeshicleState.Location,
                                                         vehicleState.Speed, (int)(vehicleState.Time / 1000), this.mStationIndex); // Bitmapを生成(枠 349*1242, 画像 512*2048)
#endif
                            // 非同期生成にしたのでここでは処理できない
                            imgCast = null;
                        }

                        //if (hTex.HasEnoughTimePassed(5))
                        //{
                        if (imgCast != null)
                        {
#if DEBUG
                            Log.Write("[Elapse] - 生成成功 : " + imgCast.Width.ToString() + ", " + imgCast.Height.ToString());
                            imgCast.Save(Path.Combine(EngineBase.ModuleDirectoryPath, "cast.png"));
#endif
                            try
                            {
                                gCast.Graphics.DrawImage(imgCast, 0, 0, imgCast.Width, imgCast.Height);
                            }
                            catch (Exception ex2)
                            {
                                throw new Exception("(gCast.Graphics.DrawImage) " + ex2.Message);
                            }
                            // ↓この方法だと透過が消える
                            //gStaff.BeginGDI();
                            ////gStaff.DrawImage(imgStaff, 0, 0);
                            //gStaff.DrawImagePartly(imgStaff, 0, 0, this.CFG.Cast.ImageSize.Width, this.CFG.Cast.ImageSize.Height);
                            //gStaff.EndGDI();

                            try
                            {
                                hCastTex.Update(gCast);
                            }
                            catch (Exception ex2)
                            {
                                throw new Exception("(hCastTex.Update) " + ex2.Message);
                            }

                        }
                        else
                        {
                            //Log.Write("[Elapse] - 生成スキップ:画像の生成に失敗した可能性があります");
                        }
                        //}
                    }
                    else
                    {
                        Log.Write("[Elapse] - 生成スキップ: 置換対象の " + this.CFG.Cast.ImageName + " がパネル内に見つからなかった可能性があります");
                    }
                }
                else if (this.pMyVeshicleState.GetAnotherMoment(500) == false)
                {
                    if (hCastTex.IsCreated == true)
                    {
#if DEBUG
                        Log.Write("[Elapse] - スタフ生成開始");
#endif
                        // 変形
                        logtext = "Deformation";
                        imgCast = null;
                        if (this.Cast.IsDrawing == false)
                        {
                            if (this.CFG.Cast.Deformation == true)
                            {
                                // 変形あり
                                logtext = "変形あり";
                                Bitmap tmpbit = this.Cast.CastImage; // Bitmapを生成(枠 349*1242, 画像 512*2048)
                                Point[] startP = new Point[4];
                                startP[0] = new Point(0, 0);
                                startP[1] = new Point(0, this.CFG.Cast.ImageSize.Height);
                                startP[2] = new Point(this.CFG.Cast.ImageSize.Width, this.CFG.Cast.ImageSize.Height);
                                startP[3] = new Point(this.CFG.Cast.ImageSize.Width, 0);
                                imgCast = HL.GraOpe.Homography(tmpbit, this.CFG.Cast.DxSize, startP, this.CFG.Cast.DeformationPoint, this.CFG.Cast.HomographyMatrix);
                            }
                            else
                            {
                                // 変形なし
                                logtext = "変形なし";
                                imgCast = this.Cast.CastImage; // Bitmapを生成(枠 349*1242, 画像 512*2048)
                            }

                            //if (hTex.HasEnoughTimePassed(5))
                            //{
                            if (imgCast != null)
                            {
#if DEBUG
                            Log.Write("[Elapse] - 生成成功 : " + imgCast.Width.ToString() + ", " + imgCast.Height.ToString());
                            imgCast.Save(Path.Combine(EngineBase.ModuleDirectoryPath, "cast.png"));
#endif
                                try
                                {
                                    gCast.Graphics.DrawImage(imgCast, 0, 0, imgCast.Width, imgCast.Height);
                                }
                                catch (Exception ex2)
                                {
                                    throw new Exception("(gCast.Graphics.DrawImage) " + ex2.Message);
                                }
                                // ↓この方法だと透過が消える
                                //gStaff.BeginGDI();
                                ////gStaff.DrawImage(imgStaff, 0, 0);
                                //gStaff.DrawImagePartly(imgStaff, 0, 0, this.CFG.Cast.ImageSize.Width, this.CFG.Cast.ImageSize.Height);
                                //gStaff.EndGDI();

                                try
                                {
                                    hCastTex.Update(gCast);
                                }
                                catch (Exception ex2)
                                {
                                    throw new Exception("(hCastTex.Update) " + ex2.Message);
                                }

                            }
                            else
                            {
                                Log.Write("[Elapse] - 生成スキップ:画像の生成に失敗した可能性があります");
                            }
                            //}
                        }
                        else
                        {
                            Log.Write("[Elapse] - 生成スキップ:画像の生成が間に合っていない可能性があります");
                        }

                    }
                    else
                    {
                        Log.Write("[Elapse] - 生成スキップ: 置換対象の " + this.CFG.Cast.ImageName + " がパネル内に見つからなかった可能性があります");
                    }
                }


                logtext = "Elapse";
                if (this.Cast != null)
                {
                    this.Cast.Elapse(this.pMyVeshicleState);
                    //
                    SetIndexValue(sound, this.CFG.Sound.FirstDeparture, this.Cast.SoundFirstDeparture);
                    SetIndexValue(sound, this.CFG.Sound.NearStation, this.Cast.SoundNearStation);
                }

                // CAST
                logtext = "Caution";
                if (this.pMyVeshicleState.GetAnotherMoment(1000) == false)
                {
                    // 通告データをセット
                    foreach (CautionSchema caution in this.UncommitedCautionData)
                    {
                        if (caution != null)
                        {
                            CastEngine.CautionData cautionData = new CastEngine.CautionData(caution);
                            cautionData.Received = 0;
                            this.Cast.SetCaution(cautionData);
                        }
                    }
                    if (this.UncommitedCautionData.Count > 0)
                    {
                        this.Cast.ReceiveAllCaution();
                    }

                    if (this.CFG.Cast.UseCom == true && (this.CastCom != null))
                    {
                        // 外部送信
                        this.CastCom.Send(this.Cast.MakeComText(this.pMyVeshicleState, this.mStationIndex, this.UncommitedCautionData));
                    }

                    this.UncommitedCautionData.Clear();
                }

#if DEBUG
                //Log.Write("[Elapse] - End : P=" + this.OutHandles.Power.ToString() + ", B=" + this.OutHandles.Brake.ToString() + ", R=" + this.OutHandles.Reverser.ToString() + ", C=" + this.OutHandles.ConstantSpeed.ToString());
#endif
                //Log.Write("[Elapse] SPD=" + (this.pMyVeshicleState.NewState.Speed / 3.6).ToString("0.000") + " BVE=" + this.pMyVeshicleState.GetDeltaLocation().ToString() + " / " + this.pMyVeshicleState.GetDeltaTime().ToString() + " = " + (this.pMyVeshicleState.GetDeltaLocation() / this.pMyVeshicleState.GetDeltaTime() * 1000).ToString("0.000") + "m/s " +
                //          "EX=" + this.pMyVeshicleState.GetDeltaLocation().ToString() + " / " + (this.pMyVeshicleState.pExNowTime-this.pMyVeshicleState.pExOldTime).ToString("0.000") + " = " + (this.pMyVeshicleState.GetDeltaLocation() / (this.pMyVeshicleState.pExNowTime - this.pMyVeshicleState.pExOldTime) * 1000).ToString("0.000") + "m/s ");
            }
            catch (Exception ex)
            {
                ExceptionMessageOnce(ex, "[MainElapse] (" + logtext + ")", true);
            }

            this.mOldStationIndex = this.mStationIndex;

            return this.OutHandles;
        }
#endregion

#region SetPower
        public void SetPower(int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Write("[SetPower] - Start : " + handlePosition.ToString());
#endif
                this.InHandles.Power = handlePosition;
#if DEBUG
                Log.Write("[SetPower] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetPower]", true);
            }
        }
#endregion

#region SetBrake
        public void SetBrake(int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Write("[SetBrake] - Start : " + handlePosition.ToString());
#endif
                this.InHandles.Brake = handlePosition;
#if DEBUG
                Log.Write("[SetBrake] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBrake]", true);
            }
        }
#endregion

#region SetReverser
        public void SetReverser(int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Write("[SetReverser] - Start : " + handlePosition.ToString());
#endif
                this.InHandles.Reverser = handlePosition;
#if DEBUG
                Log.Write("[SetReverser] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetReverser]", true);
            }
        }
#endregion

#region キーイベント

#region KeyDown
        public void KeyDown(int keyIndex)
        {
            try
            {
#if DEBUG
                Log.Write("[KeyDown] - Start : " + keyIndex.ToString());
#endif
                if (this.IsDisposing == false)
                {
                    // NFBチェック
                    // CheckKeyPush(Config.KeyInput_ENUM.AtsKey, keyIndex);

                }
#if DEBUG
                Log.Write("[KeyDown] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainKeyDown]", true);
            }
        }
        public void FormKeyDown(int keyCode)
        {
            try
            {
                //for (int i = 0; i < this.CFG.KeyAssign.Count; i++)
                //{
                //    Config.CFG_KEYASSIGN keyassign = this.CFG.KeyAssign[i];

                //    if (keyassign.KeyInput == Config.KeyInput_ENUM.Keyboard && keyassign.KeyMode != Config.KeyWork_ENUM.non && keyassign.KeyCode == keyCode)
                //    {
                //        CheckKeyPush(keyassign.KeyMode);
                //        break;
                //    }
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show("キーボード押下検知でエラーが発生しました。[FormKeyDown]\n" + ex.Message);
            }
        }
#endregion

#region KeyUp
        public void KeyUp(int keyIndex)
        {
            try
            {
#if DEBUG
                Log.Write("[KeyUp] - Start : " + keyIndex.ToString());
#endif
                if (this.IsDisposing == false)
                {
                    // キーチェック
                    //CheckKeyRelease(Config.KeyInput_ENUM.AtsKey, keyIndex);

                }
#if DEBUG
                Log.Write("[KeyUp] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainKeyUp]", true);
            }
        }
        public void FormKeyUp(int keyCode)
        {
            try
            {
                //for (int i = 0; i < this.CFG.KeyAssign.Count; i++)
                //{
                //    Config.CFG_KEYASSIGN keyassign = this.CFG.KeyAssign[i];

                //    if (keyassign.KeyInput == Config.KeyInput_ENUM.Keyboard && keyassign.KeyMode != Config.KeyWork_ENUM.non && keyassign.KeyCode == keyCode)
                //    {
                //        CheckKeyRelease(keyassign.KeyMode);
                //        break;
                //    }
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show("キーボード離上検知でエラーが発生しました。[FormKeyDown]\n" + ex.Message);
            }
        }
#endregion

#region - キーボード
        private void CheckKeyState()
        {
            try
            {
                foreach (int keyCode in this.CFG.KeyCodes)
                {
                    // キーが押されていたら (最上位ビットが1になる->signed shortなので負の数になる)
                    short KS = GetKeyState(keyCode);
                    if (this.KeyStates[keyCode] >= 0 && KS < 0)
                    {
                        CheckKeyPush(Config.KeyInput_ENUM.Keyboard, keyCode);
                    }
                    else if (this.KeyStates[keyCode] < 0 && KS >= 0)
                    {
                        CheckKeyRelease(Config.KeyInput_ENUM.Keyboard, keyCode);
                    }

                    this.KeyStates[keyCode] = KS;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyState]", true);
#endif
                Log.Write("[CheckKeyState] (Error) " + ex.Message);
            }
        }
#endregion

#region キーボード押
        private void CheckKeyPush(Config.KeyInput_ENUM _KeyInput, int _KeyCode)
        {
            try
            {

            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyPush]", true);
#endif
                Log.Write("[CheckKeyPush] (Error) " + ex.Message);
            }
        }
#endregion

#region キーボード離
        private void CheckKeyRelease(Config.KeyInput_ENUM _KeyInput, int _KeyCode)
        {
            try
            {

            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyRelease]", true);
#endif
                Log.Write("[CheckKeyRelease] (Error) " + ex.Message);
            }
        }
#endregion

#endregion

#region SetHornBlow
        public void HornBlow(int hornIndex)
        {
            try
            {
#if DEBUG
                Log.Write("[HornBlow] - Start : " + hornIndex.ToString());
#endif

#if DEBUG
                Log.Write("[HornBlow] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainHornBlow]", true);
            }
        }
#endregion

        #region DoorOpen
        public void DoorOpen()
        {
            try
            {
#if DEBUG
                Log.Write("[DoorOpen] - Start");
#endif
                this.pMyVeshicleState.SetDoorClose(false);
                if (this.Cast != null)
                {
#if !TOUCH
                    this.Cast.SetDoorOpen(true);
#endif
                }
#if DEBUG
                Log.Write("[DoorOpen] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainDoorOpen]", true);
            }
        }
#endregion

#region DoorClose
        public void DoorClose()
        {
            try
            {
#if DEBUG
                Log.Write("[DoorClose] - Start");
#endif
                this.pMyVeshicleState.SetDoorClose(true);
                if (this.Cast != null)
                {
#if !TOUCH
                    this.Cast.SetDoorOpen(false);
#endif
                }
#if DEBUG
                Log.Write("[DoorClose] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainDoorClose]", true);
            }
        }
#endregion

#region SetSignal
        public void SetSignal(int signalIndex)
        {
            try
            {
#if DEBUG
                Log.Write("[SetSignal] - Start : " + signalIndex.ToString());
#endif

#if DEBUG
                Log.Write("[SetSignal] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetSignal]", true);
            }
        }
#endregion

#region SetBeaconData
        /// <summary>
        /// 標準版
        /// </summary>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
#if DEBUG
                Log.Write("[SetBeaconData] - Start");
#endif
                if (8 <= beaconData.Type && beaconData.Type < 9)
                {
                    if (this.Cast != null)
                    {
                        this.Cast.SetBeaconData(this.pMyVeshicleState, beaconData);
                    }
                }
                else if(beaconData.Type == 80)
                {
                    this.pMyVeshicleState.SetLocation((double)beaconData.Optional);
                }
                else if(85000 <= beaconData.Type && beaconData.Type < 86000)
                {
                    if (this.Cast != null)
                    {
                        this.Cast.SetBeaconData(this.pMyVeshicleState, beaconData);
                    }
                }
#if DEBUG
                Log.Write("[SetBeaconData] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBeaconData]", true);
            }
        }
#endregion


#region SetHandles
        public void SetHandles(int powerHandle, int brakeHandle, int reverserHandle, int cscHandle)
        {
            try
            {
#if DEBUG
                Log.Write("[SetHandles] - Start");
#endif
                this.InHandles.Power = powerHandle;
                this.InHandles.Brake = brakeHandle;
                this.InHandles.Reverser = reverserHandle;
                this.InHandles.ConstantSpeed = cscHandle;
#if DEBUG
                Log.Write("[SetHandles] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[SetHandles]", true);
            }
        }
        #endregion

#if TOUCH
        public void MouseClick(System.Drawing.Point mousePos)
        {
            try
            {
                //Log.Write("[MouseClick] (" + mousePos.X.ToString() + ", " + mousePos.Y.ToString() + ")");
                if (InArea(new Rectangle(366, 751, 80, 35)) == true)
                {
                    // 戻る
                    this.Cast.PushNextButton(false);
                }
                else if (InArea(new Rectangle(459, 751, 115, 35)) == true)
                {
                    // 次へ
                    this.Cast.PushNextButton(true);
                }

                else if (this.Cast.DispDetailCautionType == -1 && this.Cast.DispDetailCautionReceived > 0 && InArea(new Rectangle(260, 700, 80, 20)) == true)
                {
                    // (時刻表) 確認
                    //pnlStaff.Visible = false;
                    CloseDetailCaution();
                }

                else if (this.Cast.DispDetailCautionType > -1 && this.Cast.DispDetailCautionCount > 0 && InArea(new Rectangle(70, 130, 600, 18)) == true)
                {
                    // 通告詳細[0]
                    ClickDetailCaution(0);
                }
                else if (this.Cast.DispDetailCautionType > -1 && this.Cast.DispDetailCautionCount > 1 && InArea(new Rectangle(70, 148, 600, 18)) == true)
                {
                    // 通告詳細[1]
                    ClickDetailCaution(1);
                }
                else if (this.Cast.DispDetailCautionType > -1 && this.Cast.DispDetailCautionCount > 2 && InArea(new Rectangle(70, 166, 600, 18)) == true)
                {
                    // 通告詳細[2]
                    ClickDetailCaution(2);
                }
                else if (this.Cast.DispDetailCautionType > -1 && this.Cast.DispDetailCautionCount > 3 && InArea(new Rectangle(70, 184, 600, 18)) == true)
                {
                    // 通告詳細[3]
                    ClickDetailCaution(3);
                }
                else if (this.Cast.DispDetailCautionType > -1 && this.Cast.DispDetailCautionCount > 4 && InArea(new Rectangle(70, 202, 600, 18)) == true)
                {
                    // 通告詳細[4]
                    ClickDetailCaution(4);
                }
                else if (this.Cast.DispDetailCautionType > -1 && this.Cast.DispDetailCautionCount > 0 && InArea(new Rectangle(230, 233, 140, 20)) == true)
                {
                    // 通告詳細[受領]
                    CloseDetailCaution();
                }

                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount > 0 && InArea(new Rectangle(14, 107, 106, 156)) == true)
                {
                    // 通告一覧[0]
                    ClickCaution(0);
                }
                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount > 1 && InArea(new Rectangle(134, 107, 106, 156)) == true)
                {
                    // 通告一覧[1]
                    ClickCaution(1);
                }
                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount > 2 && InArea(new Rectangle(254, 107, 106, 156)) == true)
                {
                    // 通告一覧[2]
                    ClickCaution(2);
                }
                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount > 3 && InArea(new Rectangle(374, 107, 106, 156)) == true)
                {
                    // 通告一覧[3]
                    ClickCaution(3);
                }
                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount > 4 && InArea(new Rectangle(494, 107, 106, 156)) == true)
                {
                    // 通告一覧[4]
                    ClickCaution(4);
                }

                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount == 0 && InArea(new Rectangle(14, 150, 24, 24)) == true)
                {
                    // 時刻表
                    ClickCaution(-1);
                }

                else if (this.Cast.DispDetailCautionCount == 0 && this.Cast.ValidCautionCount > 0 && InArea(new Rectangle(14, 280, 24, 24)) == true)
                {
                    // 時刻表
                    ClickCaution(-1);
                }

                else if (this.Cast.ValidCautionCount == 0 && InArea(new Rectangle(54, 180, 88, 56)) == true)
                {
                    // スタフ 5つ次の駅
                    this.Cast.SetStaionIndex(this.Cast.StationIndex + 5);
                }
                else if (InArea(new Rectangle(54, 270, 88, 56)) == true)
                {
                    // スタフ 4つ次の駅
                    this.Cast.SetStaionIndex(this.Cast.StationIndex + 4);
                }
                else if (InArea(new Rectangle(54, 360, 88, 56)) == true)
                {
                    // スタフ 3つ次の駅
                    this.Cast.SetStaionIndex(this.Cast.StationIndex + 3);
                }
                else if (InArea(new Rectangle(54, 450, 88, 56)) == true)
                {
                    // スタフ 2つ次の駅
                    this.Cast.SetStaionIndex(this.Cast.StationIndex + 2);
                }
                else if (InArea(new Rectangle(54, 540, 88, 56)) == true)
                {
                    // スタフ 1つ次の駅
                    this.Cast.SetStaionIndex(this.Cast.StationIndex + 1);
                }
                else if (InArea(new Rectangle(54, 630, 88, 56)) == true)
                {
                    // スタフ この駅
                }
                else if (InArea(new Rectangle(54, 720, 88, 56)) == true)
                {
                    // スタフ 1つ前の駅
                    this.Cast.SetStaionIndex(this.Cast.StationIndex - 1);
                }

            }
            catch (Exception ex)
            {
                Log.Write("[MouseClick] " + ex.Message);
            }

            // 座標が範囲内にあるか
            bool InArea(Rectangle rg)
            {
                if (rg.Left <= mousePos.X && mousePos.X <= rg.Left + rg.Width && rg.Top <= mousePos.Y && mousePos.Y <= rg.Top + rg.Height)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // 通告のクリック
            void ClickCaution(int index)
            {
                try
                {
                    this.Cast.ClickCaution(index);
                    //pbxCast.Image = this.Cast.DrawCast(pbxCast.Size);

                    if (this.Cast.DispDetailCautionType == -1)
                    {
                        // 時刻表モード
                        //pnlStaff.Visible = true;
                        //pnlStaff.VerticalScroll.Value = 0;
                    }
                }
                catch
                {

                }
            }

            // 通告内容のクリック
            void ClickDetailCaution(int index)
            {
                try
                {
                    this.Cast.ClickDetailCaution(index);
                    //pbxCast.Image = this.cast.DrawCast(pbxCast.Size);
                }
                catch
                {

                }
            }

            // 通告内容を閉じる
            void CloseDetailCaution()
            {
                try
                {
                    this.Cast.CloseDetailCaution();
                    //pbxCast.Image = this.cast.DrawCast(pbxCast.Size);
                }
                catch
                {

                }
            }

        }
#endif

        #region 路線ファイル読み込み→停車場リストファイルオープン
        private string LoadStationList(string RoutePath = "")
        {
            string filepath = "";
            string loctext = "";
            try
            {
                // AtsEX 路線ファイルから駅リストのパスを取得
                if (RoutePath != "" && System.IO.File.Exists(RoutePath) == true)
                {
#if DEBUG
                    Log.Write("[LoadStationList] - RoutePath Path=" + RoutePath);
#endif
                    //HL.Bve.Map map2 = new Bve.Map(1); // 駅リスト検索モード
                    HL.Bve.Map map2 = new Bve.Map(); // StationListも欲しいので駅リスト検索モードだとだめ
                    if (map2.Load(RoutePath, false) == true)
                    {
                        if (map2 != null)
                        {
                            filepath = map2.StationPath;
                            if (map2.StationList != null)
                            {
                                foreach (HL.Bve.Map.Station stat in map2.StationList)
                                {
                                    HL.Bve.Map.Station s = new Bve.Map.Station();
                                    stat.Copy(s);
                                    this.lStationLoc.Add(s);
                                }
                            }
                            // 85150番地上子を取得して通告データを取得
                            if (map2.BeaconList != null)
                            {
                                foreach (HL.Bve.Map.Beacon beacon in map2.BeaconList)
                                {
                                    if (beacon.Type == 85150)
                                    {
                                        string cp = System.IO.Path.GetDirectoryName(beacon.Etc) + "\\caution" + beacon.Optional.ToString() + ".json";
                                        Log.Write("[Load 85150] " + cp);
                                        if (System.IO.File.Exists(cp) == true)
                                        {
                                            try
                                            {
                                                // JSONファイルを開く
                                                using (var stream = new FileStream(cp, FileMode.Open))
                                                {
                                                    // JSONファイルを読み出す
                                                    using (var sr = new StreamReader(stream))
                                                    {
                                                        // デシリアライズオブジェクト関数に読み込んだデータを渡して、
                                                        // 指定されたデータ用のクラス型で値を返す。
                                                        CastEngine.CautionData cautionData;
                                                        //JSONをクラスに変換 ※クラスは出力したJSONを貼り付けて作成し、Listにデシリアライズする
                                                        JsonSerializerOptions jso = new JsonSerializerOptions();
                                                        jso.AllowTrailingCommas = true;
                                                        jso.ReadCommentHandling = JsonCommentHandling.Skip;
                                                        jso.NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.WriteAsString;
                                                        jso.WriteIndented = true;
                                                        jso.Encoder = JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All);

                                                        string sre = sr.ReadToEnd();
                                                        cautionData = JsonSerializer.Deserialize<CastEngine.CautionData>(sre, jso);
                                                        cautionData.Received = 0;
                                                        if(cautionData.DispStart == -1)
                                                        {
                                                            cautionData.DispStart = beacon.Location;
                                                        }
                                                        this.UncommitedCautionData.Add(cautionData);
                                                        // ※この段階ではまだ初期化されていない
                                                        //this.Cast.SetCaution(cautionData);
                                                        // ※この段階ではまだCastComがオープンしていない
                                                        //if (this.CFG.Cast.UseCom == true && (this.CastCom != null))
                                                        //{
                                                        //    this.CastCom.Send(sre);
                                                        //}
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.Write("[Load 85150] (error) " + ex.Message);
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            filepath = "";
                            Log.Write("[LoadStationList] - Bve.Map is null Path=" + RoutePath);
                        }
                    }
                    else
                    {
                        filepath = "";
                        Log.Write("[LoadStationList] - Bve.Map CannotLoad. Path=" + RoutePath);
                    }
                    map2 = null;
                }
                else
                {
                    Log.Write("[LoadStationList] - RoutePath NotFound Path=" + RoutePath);
                }

                if(filepath != "")
                {
                    Log.Write("[LoadStationList] - StaionList Path=" + filepath);
                }
                if (filepath != "" && System.IO.File.Exists(filepath) == true)
                {
                    // 停車場リスト読み込み
                    bool staffAri = false;
                    //this.mStation = new HL.Bve.Station();
                    this.Cast.mBveStaff = new Bve.Station();
                    //this.mStation.Load(filepath, ref staffAri);
                    this.Cast.mBveStaff.Load(filepath, ref staffAri);
                    Log.Write("[LoadStationList] - StaionList enc=" + this.Cast.mBveStaff.Enc.CodePage.ToString());
                    //this.mStation.StationList = this.mStation.SortStaffStation(); // スタフ表示順ソート
                    this.Cast.mBveStaff.StationList = this.Cast.mBveStaff.SortStaffStation(); // スタフ表示順ソート
                    this.Cast.mBveStaff.CalcGapTime(); // 運転時分計算

                    StringBuilder sb = new StringBuilder(1024);
                    // 距離程代入
                    if (this.lStationLoc != null)
                    {
                        foreach(HL.Bve.Map.Station st in this.lStationLoc)
                        {
                            //this.mStation.SetLocation(st.StationKey, st.Location);
                            this.Cast.mBveStaff.SetLocation(st.StationKey, st.Location);
                            if (loctext != "")
                            {
                                //loctext += ",";
                            }
                            if (sb.Length > 0)
                            {
                                sb.Append(",");
                            }
                            //loctext += "{\"Location\":" + st.Location.ToString() + ",\"Key\":\"" + st.StationKey.Replace("'", "").Replace("\"", "") + "\"}";
                            sb.Append("{\"Location\":" + st.Location.ToString() + ",\"Key\":\"" + st.StationKey.Replace("'", "").Replace("\"", "") + "\"}");
                        }
                        loctext = sb.ToString();
                        sb.Clear();
                    }

                    // 停車場リスト外部送信用文字列作成
                    string retval = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(filepath, this.Cast.mBveStaff.Enc))
                    {
                        //
                        while (sr.Peek() > -1)
                        {
                            //retval += sr.ReadLine() + "\\n";
                            sb.AppendLine(sr.ReadLine());
                        }
                    }

                    this.Cast.mBveStaff.StaffHeader.Hensyu = true;
                    retval = "{\"Stations\":\"" + this.Cast.mBveStaff.OutputText().Replace("\r\n", "\\n").Replace("\n", "\\n") + "\"";
                    sb.Clear();
                    sb.Append(retval);
                    if (loctext != "")
                    {
                        retval += ",\"StationLocs\":[" + loctext + "]";
                    }
                    retval += "}";

                    return retval;
                }
                else
                {
                    Log.Write("[LoadStationList] - StationList File NotFound " + filepath);
                    return "{\"Stations\":null}";
                }
            }
            catch (Exception ex)
            {
                Log.Write("[LoadStationList] (Error) - " + ex.Message);
                return "{\"Stations\":null}";
            }
        }
        #endregion

        #region 駅インデックス
        /// <summary>
        /// BVEの駅インデックスをもらう (EX)
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pStationName"></param>
        public void SetStationIndex(int pIndex, string pStationName)
        {
#if TOUCH
            if (this.mStationIndex + this.Cast.DeltaStaIndex != pIndex && this.mStationIndex + this.Cast.DeltaStaIndex < 0)
            {
                this.mStationIndex = pIndex - this.Cast.DeltaStaIndex;
                if (this.mStationIndex != this.mOldStationIndex)
                {
                    // 駅が変わったとみなして停車接近をオフ
                    this.Cast.ResetNearStation();
                    this.Cast.ReceiveAllCaution();
                }
                Log.Write("[SetStationIndex] " + pIndex.ToString() + " Name=" + pStationName + " Delta=" + this.Cast.DeltaStaIndex.ToString());
            }
#else
            if(this.mStationIndex + this.Cast.DeltaStaIndex != pIndex)
            {
                this.mStationIndex = pIndex - this.Cast.DeltaStaIndex;
                if(this.mStationIndex != this.mOldStationIndex)
                {
                    // 駅が変わったとみなして停車接近をオフ
                    this.Cast.ResetNearStation();
                    this.Cast.ReceiveAllCaution();
                }
                /*
                // Deltaの判定に使う距離程の取得がうまくいっていないので暫定対応 3/30直った？
                // 一致する駅名を探してDeltaIndexを求める
                for(int i = 0;i < this.Cast.mBveStaff.StaffPack.StationList.Count;i++)
                {
                    if (this.Cast.mBveStaff.StaffPack.StationList[i].StationName == pStationName)
                    {
                        this.Cast.DeltaStaIndex = i - pIndex;
                        break;
                    }
                }
                */
                //Log.Write("[SetStationIndex] " + pIndex.ToString() + " Name=" + pStationName + " Delta=" + this.Cast.DeltaStaIndex.ToString());
            }
#endif
        }
        #endregion

        public void SetMS(TimeSpan ts)
        {
            this.pMyVeshicleState.pExOldTime = this.pMyVeshicleState.pExNowTime;
            this.pMyVeshicleState.pExNowTime = ts.TotalMilliseconds;
        }
    }
}
