﻿using BveTypes.ClassWrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using BveEx.PluginHost.Plugins;
using BveEx.Extensions.Native;
using System.Runtime.Remoting.Messaging;


namespace AtsPlugin
{

    [Plugin(PluginType.VehiclePlugin)]
    public class AtsMain : AssemblyPluginBase
    {
        private readonly INative Native;

        IntPtr panel;
        IntPtr sound;
        //IAtsPanelValue<int>[] ExPanel = new IAtsPanelValue<int>[256];
        //IAtsSound[] ExSound = new IAtsSound[256];

        public AtsMain(PluginBuilder services) : base(services)
        {
            Native = Extensions.GetExtension<INative>();
            BveHacker.MainFormSource.Focus();

            BveHacker.MainFormSource.KeyDown += new KeyEventHandler(FrmMain_KeyDown);
            BveHacker.MainFormSource.KeyUp += new KeyEventHandler(FrmMain_KeyUp);
#if TOUCH
            BveHacker.MainFormSource.MouseClick += new MouseEventHandler(FrmMain_MouseClick);
#endif
            Native.BeaconPassed += new EventHandler<BeaconPassedEventArgs>(BeaconPassed);
            Native.DoorClosed += new EventHandler(DoorClosed);
            Native.DoorOpened += new EventHandler(DoorOpened);
            Native.Started += new EventHandler<StartedEventArgs>(Initialized);
            // Native.NativeKeys.AtsKeys[AtsEx.PluginHost.Input.Native.NativeAtsKeyName.A1].Pressed

            panel = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(int)) * 256);
            sound = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(int)) * 256);
            for (int i = 0; i < 256; i++)
            {
                //ExPanel[i] = Native.AtsPanelValues.RegisterInt32(i, 0, AtsEx.PluginHost.Binding.BindingMode.TwoWay);
                //ExSound[i] = Native.AtsSounds.Register(i);
                EngineBase.SetIndexValue(panel, i, 0);
                EngineBase.SetIndexValue(sound, i, 0);
            }

            if (AtsEngine.ATS == null)
            {
                AtsEngine.ATS = new AtsEngine(1);
            }
            AtsEngine.ATS.Load(BveHacker.ScenarioInfo.RouteFiles.SelectedFile.Path);
            // ここではまだNative.VehicleSpec=null
            //AtsEngine.ATS.SetVehicleSpec(Ex2Def_VehicleSpec(Native.VehicleSpec));

            //if (0 <= AtsEngine.ATS.CFG.Sound.NearStation && AtsEngine.ATS.CFG.Sound.NearStation <= 255)
            //{
            //    ExSound[AtsEngine.ATS.CFG.Sound.NearStation] = Native.AtsSounds.Register(AtsEngine.ATS.CFG.Sound.NearStation);
            //}
            //if (0 <= AtsEngine.ATS.CFG.Sound.FirstDeparture && AtsEngine.ATS.CFG.Sound.FirstDeparture <= 255)
            //{
            //    ExSound[AtsEngine.ATS.CFG.Sound.FirstDeparture] = Native.AtsSounds.Register(AtsEngine.ATS.CFG.Sound.FirstDeparture);
            //}

        }

        public override void Dispose()
        {
            BveHacker.MainFormSource.KeyDown -= new KeyEventHandler(FrmMain_KeyDown);
            BveHacker.MainFormSource.KeyUp -= new KeyEventHandler(FrmMain_KeyUp);
#if TOUCH
            BveHacker.MainFormSource.MouseClick -= new MouseEventHandler(FrmMain_MouseClick);
#endif
            Native.BeaconPassed -= new EventHandler<BveEx.Extensions.Native.BeaconPassedEventArgs>(BeaconPassed);
            Native.DoorClosed -= new EventHandler(DoorClosed);
            Native.DoorOpened -= new EventHandler(DoorOpened);
            Native.Started -= new EventHandler<StartedEventArgs>(Initialized);

            if (AtsEngine.ATS != null)
            {
                AtsEngine.ATS.Dispose();
            }
            AtsEngine.ATS = null;
        }

        public override void Tick(TimeSpan elapsed)
        {
            // 次の駅のインデックス(たぶん戸閉すると加算される)
            int stationIndex = BveHacker.Scenario.Map.Stations.CurrentIndex + 1;
            string stationName = ((Station)BveHacker.Scenario.Map.Stations[stationIndex]).Name;
            AtsEngine.ATS.SetStationIndex(stationIndex, stationName);

            //var panelArray = new AtsDefine.AtsIoArray(panel);
            //var soundArray = new AtsDefine.AtsIoArray(sound);

            //AtsEngine.ATS.SetMS(BveHacker.Scenario.TimeManager.Time);

            AtsDefine.AtsVehicleState vehicleState = Ex2Def_VehicleState(Native.VehicleState);
            //Ex2Def_PanelSound();

            AtsDefine.AtsHandles outHandles = AtsEngine.ATS.Elapse(vehicleState, panel, sound);

            //Def2Ex_PanelSound();
            if (0 <= AtsEngine.ATS.CFG.Sound.NearStation && AtsEngine.ATS.CFG.Sound.NearStation <= 255)
            {
                // Sound
                //int s = EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.NearStation);
                //switch (s)
                //{
                //    case AtsDefine.AtsSoundControlInstruction.Play:
                //        ExSound[AtsEngine.ATS.CFG.Sound.NearStation].Play();
                //        break;
                //    case AtsDefine.AtsSoundControlInstruction.Continue:
                //        break;
                //    case AtsDefine.AtsSoundControlInstruction.PlayLooping:
                //        ExSound[AtsEngine.ATS.CFG.Sound.NearStation].PlayLoop(0);
                //        break;
                //    case AtsDefine.AtsSoundControlInstruction.Stop:
                //        ExSound[AtsEngine.ATS.CFG.Sound.NearStation].Stop();
                //        break;
                //    default:
                //        ExSound[AtsEngine.ATS.CFG.Sound.NearStation].PlayLoop((double)EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.NearStation));
                //        break;
                //}
                Native.AtsSoundArray[AtsEngine.ATS.CFG.Sound.NearStation] = EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.NearStation);
            }

            if (0 <= AtsEngine.ATS.CFG.Sound.FirstDeparture && AtsEngine.ATS.CFG.Sound.FirstDeparture <= 255)
            {
                // Sound
                //int s = EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.FirstDeparture);
                //switch (s)
                //{
                //    case AtsDefine.AtsSoundControlInstruction.Play:
                //        ExSound[AtsEngine.ATS.CFG.Sound.FirstDeparture].Play();
                //        break;
                //    case AtsDefine.AtsSoundControlInstruction.Continue:
                //        break;
                //    case AtsDefine.AtsSoundControlInstruction.PlayLooping:
                //        ExSound[AtsEngine.ATS.CFG.Sound.FirstDeparture].PlayLoop(0);
                //        break;
                //    case AtsDefine.AtsSoundControlInstruction.Stop:
                //        ExSound[AtsEngine.ATS.CFG.Sound.FirstDeparture].Stop();
                //        break;
                //    default:
                //        ExSound[AtsEngine.ATS.CFG.Sound.FirstDeparture].PlayLoop((double)EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.FirstDeparture));
                //        break;
                //}
                Native.AtsSoundArray[AtsEngine.ATS.CFG.Sound.NearStation] = EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.FirstDeparture);
            }

            //AtsEngine.Log.Write("Weight=" + BveHacker.Scenario.Vehicle.Dynamics.FirstCar.Weight.ToString());
            //AtsEngine.Log.Write("Station=" + BveHacker.Scenario.Route.Stations.CurrentIndex.ToString());

            BveTypes.ClassWrappers.AtsPlugin atsPlugin = BveHacker.Scenario.Vehicle.Instruments.AtsPlugin;
            atsPlugin.AtsHandles = Def2Ex_OutHandles(outHandles);
            //return new VehiclePluginTickResult();
            //BveTypes.ClassWrappers.AtsPlugin atsPlugin = BveHacker.Scenario.Vehicle.Instruments.AtsPlugin;
            //atsPlugin.AtsHandles.PowerNotch = 0;
            //atsPlugin.AtsHandles.BrakeNotch = Math.Max(atsPlugin.AtsHandles.NotchInfo.EmergencyBrakeNotch - 1, atsPlugin.Handles.BrakeNotch);
            //atsPlugin.AtsHandles.ConstantSpeedMode = ConstantSpeedMode.Disable;
        }

        private void FrmMain_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            AtsEngine.ATS.FormKeyDown((int)e.KeyCode);
        }
        private void FrmMain_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            AtsEngine.ATS.FormKeyUp((int)e.KeyCode);
        }
#if TOUCH
        private void FrmMain_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {

            // パネル上の座標
            Point point = HL.Bve.Panel.FormPos2PanelPos(new Point(e.X, e.Y), 
                                                        Direct3DProvider.Instance.PresentParameters.BackBufferWidth, 
                                                        BveHacker.Scenario.Vehicle.Panel.Resolution,
                                                        BveHacker.Scenario.Vehicle.Panel.Origin,
                                                        BveHacker.Scenario.Vehicle.CameraLocation.Plane);
            //EngineBase.Log.Write("[FrmMouseClick] (" + e.X.ToString() + ", " + e.Y.ToString() + ") -> (" + point.X.ToString() + ", " + point.Y.ToString() + ")");

            // CAST上の相対座標
            // - 原点を移動
            point.X -= AtsEngine.ATS.CFG.Cast.Location.X;
            point.Y -= AtsEngine.ATS.CFG.Cast.Location.Y;
            // - 拡大縮小
            point.X = (point.X * 600 / AtsEngine.ATS.CFG.Cast.ImageSize.Width);
            point.Y = (point.Y * 800 / AtsEngine.ATS.CFG.Cast.ImageSize.Height);
            AtsEngine.ATS.MouseClick(point);
        }
#endif
        private void Initialized(object sender, StartedEventArgs e)
        {
            if (AtsEngine.ATS == null)
            {
                AtsEngine.ATS.Initialize((int)e.DefaultBrakePosition);
            }
            AtsEngine.ATS.SetVehicleSpec(Ex2Def_VehicleSpec(Native.VehicleSpec));
        }

        private void DoorOpened(object sender, EventArgs e)
        {
            AtsEngine.ATS.DoorOpen();
        }

        private void DoorClosed(object sender, EventArgs e)
        {
            AtsEngine.ATS.DoorClose();
        }

        private void BeaconPassed(object sender, BveEx.Extensions.Native.BeaconPassedEventArgs e)
        {
            AtsEngine.ATS.SetBeaconData(Ex2Def_Beacon(e));
        }


        #region 変換
        private AtsDefine.AtsVehicleSpec Ex2Def_VehicleSpec(BveEx.Extensions.Native.VehicleSpec _VehicleSpec)
        {
            AtsDefine.AtsVehicleSpec vehicleSpec;

            vehicleSpec.AtsNotch = _VehicleSpec.AtsNotch;
            vehicleSpec.B67Notch = _VehicleSpec.B67Notch;
            vehicleSpec.BrakeNotches = _VehicleSpec.BrakeNotches;
            vehicleSpec.Cars = _VehicleSpec.Cars;
            vehicleSpec.PowerNotches = _VehicleSpec.PowerNotches;

            return vehicleSpec;
        }

        private AtsDefine.AtsVehicleState Ex2Def_VehicleState(BveEx.Extensions.Native.VehicleState _VehicleState)
        {
            AtsDefine.AtsVehicleState vehicleState;

            vehicleState.BcPressure = _VehicleState.BcPressure;
            vehicleState.BpPressure = _VehicleState.BpPressure;
            vehicleState.Current = _VehicleState.Current;
            vehicleState.ErPressure = _VehicleState.ErPressure;
            vehicleState.Location = _VehicleState.Location;
            vehicleState.MrPressure = _VehicleState.MrPressure;
            vehicleState.SapPressure = _VehicleState.SapPressure;
            vehicleState.Speed = _VehicleState.Speed;
            vehicleState.Time = (int)(_VehicleState.Time.TotalMilliseconds);

            return vehicleState;
        }

        private BveTypes.ClassWrappers.HandleSet Def2Ex_OutHandles(AtsDefine.AtsHandles outHandles)
        {
            BveTypes.ClassWrappers.AtsPlugin atsPlugin = BveHacker.Scenario.Vehicle.Instruments.AtsPlugin;

            // ※制御するものだけセット
            //atsPlugin.AtsHandles.PowerNotch = atsPlugin.Handles.PowerNotch;
            //atsPlugin.AtsHandles.PowerNotch = atsPlugin.Handles.BrakeNotch;
            //atsPlugin.AtsHandles.ReverserPosition = atsPlugin.Handles.ReverserPosition;
            //atsPlugin.AtsHandles.ConstantSpeedMode = atsPlugin.Handles.ConstantSpeedMode;
            //EngineBase.Log.Write(atsPlugin.AtsHandles.PowerNotch.ToString() + "," + atsPlugin.AtsHandles.BrakeNotch.ToString());
            return atsPlugin.AtsHandles;
        }

        //private void Ex2Def_PanelSound()
        //{
        //    for (int i = 0; i < 256; i++)
        //    {
        //        EngineBase.SetIndexValue(panel, i, ExPanel[i].Value);
        //        //EngineBase.SetPanelValue(sound, i, (int)ExSound[i].PlayState);
        //    }
        //}

        //private void Def2Ex_PanelSound()
        //{
        //    for (int i = 0; i < 256; i++)
        //    {
        //        // Panel
        //        ExPanel[i].Value = EngineBase.ReadIndexValue(panel, i);

        //        // Sound
        //        int s = EngineBase.ReadIndexValue(panel, i);
        //        switch (s)
        //        {
        //            case AtsDefine.AtsSoundControlInstruction.Play:
        //                ExSound[i].Play();
        //                break;
        //            case AtsDefine.AtsSoundControlInstruction.Continue:
        //                break;
        //            case AtsDefine.AtsSoundControlInstruction.PlayLooping:
        //                ExSound[i].PlayLoop(0);
        //                break;
        //            case AtsDefine.AtsSoundControlInstruction.Stop:
        //                ExSound[i].Stop();
        //                break;
        //            default:
        //                ExSound[i].PlayLoop((double)EngineBase.ReadIndexValue(sound, i));
        //                break;
        //        }
        //    }
        //}

        private AtsDefine.AtsBeaconData Ex2Def_Beacon(BeaconPassedEventArgs eBeacon)
        {
            AtsDefine.AtsBeaconData beaconData;
            beaconData.Type = eBeacon.Type;
            beaconData.Signal = eBeacon.SignalIndex;
            beaconData.Distance = eBeacon.Distance;
            beaconData.Optional = eBeacon.Optional;

            return beaconData;
        }
        #endregion
    }
}
