﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
//using System.Text.Json;
//using System.Text.Json.Serialization;
//using System.Threading.Tasks;
using System.Xml.Linq;
using System.Windows.Forms;

using HL;
using System.Diagnostics.Eventing.Reader;
using static HL.Bve.Station;
using System.Linq.Expressions;
using System.Media;
using System.Threading.Tasks;

namespace AtsPlugin
{
    /// <summary>
    /// CASTもどきの画像を作るクラス
    /// </summary>
    public class CastEngine : EngineBase
    {
        private Bitmap CastDisp;
        // - 背景
        private Bitmap CastBase = null;
        // - ヘッダー
        private Bitmap CastHeader = null;
        // - 時刻表
        private Bitmap CastStaff = null;
        private Bitmap CastNext = null;
        // - 路線
        private Bitmap ImgRail = null;
        private Bitmap ImgMyTrain = null;
        private Bitmap ImgSignal = null;
        private Bitmap ImgCross = null;
        private Bitmap ImgTunnelStart = null;
        private Bitmap ImgTunnelEnd = null;
        // - 素材
        // -- 数字
        private Bitmap[] ImgNumber58 = new Bitmap[10];
        private Bitmap[] ImgNumber35 = new Bitmap[10];
        private Bitmap[] ImgNumber20 = new Bitmap[10];
        private Bitmap[] ImgNumber18 = new Bitmap[10];
        private Bitmap[] ImgNumber15 = new Bitmap[10];
        private Bitmap[] ImgNumber13 = new Bitmap[10];
        private Bitmap[] ImgNumber10 = new Bitmap[12]; // 10:. 11:-
        private Bitmap[] ImgNumberSignal = new Bitmap[15]; // 10:出発 11:場内 12:中継 13:遠方 14:入換
        // -- お知らせ！
        private Bitmap ImgCaution = null;
        private Bitmap ImgCautionYellow = null;
        // -- 制限速度単位
        private Bitmap ImgLimitUnit = null;
        // -- 行き先時刻表
        private Bitmap ImgTimetable = null;
        // -- 停車マーク
        private Bitmap ImgStop1 = null;
        private Bitmap ImgStop2 = null;
        // -- 通過マーク
        private Bitmap ImgPass1 = null;
        private Bitmap ImgPass2 = null;
        // -- 全車自車
        private Bitmap ImgOneman1 = null;
        private Bitmap ImgOneman2 = null;
        // -- 戻るボタン・停車ボタン
        private Bitmap ImgBackButton = null;
        private Bitmap ImgStopButton = null;
        // -- 予告徐行区間
        private Bitmap ImgShimashima1 = null;
        // -- 予告徐行区間
        private Bitmap ImgShimashima2 = null;
        // -- 停車予告
        private Bitmap ImgStopStationCaution = null;
        // - 停車目標
        private Dictionary<int, Bitmap> StopTargetImage = null;

        /// <summary>
        /// t:フォーム f:EX
        /// </summary>
        private bool FrmMode = false;

        private bool pIsDrawing = false;
        /// <summary>
        /// CAST描画中
        /// </summary>
        public bool IsDrawing
        {
            get { return this.pIsDrawing; }
        }

        private Bitmap pCastImage = null;
        /// <summary>
        /// CAST画像 (サイズ調整後)
        /// </summary>
        public Bitmap CastImage
        {
            get { if (!this.pIsDrawing) return this.pCastImage; else return null; } 
        }

        private string mFontName = "メイリオ";

        private DataTable RailwayTBL = null;
        private bool pNFB = false;
        private int pCars = 0;
        private int pTmpLocation = 0;

        private int mNowLocation = 0;
        private int mPanelLocation = 0;
        private double mSpeed = 0;
        private int mTime = 0;

        // 
        private int pNearStation = 0; // 0:走行中 1:接近 2:停車中 3:(運転停車駅接近中) 4:(運転停車中)
        /// <summary>
        /// 0:走行中 1:接近 2:停車中 3:(運転停車駅接近中)
        /// </summary>
        public int NearStation
        {
            get { return pNearStation; }
        }
        private int pOldNearStation = 0; // 0:走行中 1:接近 2:停車中
        // 表示中駅インデックス
        private int mStationIndex = 0;
        /// <summary>
        /// 表示中駅インデックス
        /// </summary>
        public int StationIndex
        {
            get { return this.mStationIndex; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeltaStaIndex
        {
            get { return this.mDeltaStaIndex; }
            set { this.mDeltaStaIndex = value; }
        }
        private int mDeltaStaIndex = 0; // 停車場リストの0はスタフの何番目か
        // 停車駅番号
        private int pStopIndex1 = 0;
        private int pStopIndex2 = 0;
        // 停車目標番号
        private int pStopTarget1 = 0;
        private int pStopTarget2 = 0;
        // サウンド
        private int pSoundFirstDeparture = AtsDefine.AtsSoundControlInstruction.Stop;
        public int SoundFirstDeparture
        {
            get { return this.pSoundFirstDeparture; }
        }

        private int pSoundNearStation = AtsDefine.AtsSoundControlInstruction.Stop;
        public int SoundNearStation
        {
            get { return this.pSoundNearStation; }
        }

        private SoundPlayer soundPlayerDeparture = null;
        private SoundPlayer soundPlayerNearStation = null;

        // 駅リスト(駅一覧の駅番号, 駅名)
        private Dictionary<int, string> Stations;
        // 停車場リストデータ
        //public HL.Staff mBveStaff;
        public HL.Bve.Station mBveStaff;

        // 通告リスト
        private List<CautionData> Cautions; // 全部
        private List<CautionData> ValidCautions; // 有効(表示するもの)
        private CautionData DispDetailCaution; // 詳細表示中の通告
        private List<int> LimpSpeedList = null; // 駅間徐行速度リスト
        public int CautionCount
        {
            get { if (this.Cautions == null) return 0; else return this.Cautions.Count; }
        }
        public int ValidCautionCount
        {
            get { if (this.ValidCautions == null) return 0; else return this.ValidCautions.Count; }
        }
        public int DispDetailCautionCount
        {
            get { if (this.DispDetailCaution == null || this.DispDetailCaution.Checked == null) return 0; else return this.DispDetailCaution.Checked.Length; }
        }
        /// <summary>
        /// 表示中の通告の詳細の種類 (-2:null -1:時刻表確認 0:通常 1:徐行 2:規制)
        /// </summary>
        public int DispDetailCautionType
        {
            get { if (this.DispDetailCaution == null) return -2; else return this.DispDetailCaution.Type; }
        }
        public int DispDetailCautionReceived
        {
            get { if (this.DispDetailCaution == null) return -1; else return this.DispDetailCaution.Received; }
        }

        // 送信用
        private bool pStopFlag = false;
        private List<BeaconSchema> lBeacon;


        #region データクラス
        public class CautionData : CautionSchema // こいつを継承するのはよろしくないけど...
        {
            /// <summary>
            /// 通告番号
            /// </summary>
            //public int Number { get; set; }
            /// <summary>
            /// 通告種類(0:通常 1:徐行 2:規制)
            /// </summary>
            //public int Type { get; set; }
            /// <summary>
            /// 通告内容
            /// </summary>
            //public string Caution { get; set; }
            /// <summary>
            /// 開始距離程
            /// </summary>
            //public double Location1 { get; set; }
            /// <summary>
            /// 終了距離程
            /// </summary>
            //public double Location2 { get; set; }
            /// <summary>
            /// 規制速度
            /// </summary>
            //public int LimpSpeed { get; set; }
            /// <summary>
            /// 表示済み
            /// </summary>
            public bool Disped { get; set; }
            /// <summary>
            /// 受領済み (0:未受領 1:復唱待ち 2:受領済 3:発車前未確認 4:受領済)
            /// </summary>
            public int Received { get; set; }
            /// <summary>
            /// 確認済み
            /// </summary>
            public OneCaution[] Checked { get; set; }

            public CautionData()
            {
                this.Number = 0;
                this.Type = 0;
                this.Title = "";
                this.Caution = "";
                this.Location1 = 0;
                this.Location2 = 0;
                this.LimpSpeed = 0;
                this.DispStart = 0;
                this.Disped = false;
                this.Received = 0;
                this.Checked = null;
            }
            public CautionData(CautionSchema caution)
            {
                this.Number = caution.Number;
                this.Type = caution.Type;
                this.Title = caution.Title;
                this.Caution = caution.Caution;
                this.Location1 = caution.Location1;
                this.Location2 = caution.Location2;
                this.LimpSpeed = caution.LimpSpeed;
                this.DispStart = caution.DispStart;
                this.Disped = false;
                this.Received = 0;
                this.Checked = null;
            }
            public void Devide()
            {
                string[] strs = this.Caution.Split('\n');
                this.Checked = new OneCaution[strs.Length + 2]; // 0はNo 1はtitle 2以降
                this.Checked[0] = new OneCaution("");
                this.Checked[1] = new OneCaution("");
                for (int i = 0;i< strs.Length; i++)
                {
                    this.Checked[i+2] = new OneCaution(strs[i]);
                }
            }
        }
        public class OneCaution
        {
            /// <summary>
            /// 内容
            /// </summary>
            public string Content;
            /// <summary>
            /// タップ済み
            /// </summary>
            public bool Checked;

            public OneCaution(string caution)
            {
                Content = caution;
                Checked = false;
            }
        }
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CastEngine(bool frmMode)
        {
            this.FrmMode = frmMode;

            // 軌道ファイル用
            RailwayTBL = new DataTable();
            RailwayTBL.Columns.Add("kind", Type.GetType("System.String"));
            RailwayTBL.Columns.Add("start", Type.GetType("System.Int32"));
            RailwayTBL.Columns.Add("end", Type.GetType("System.Int32"));
            RailwayTBL.Columns.Add("etc", Type.GetType("System.String"));

            this.CastDisp = new Bitmap(600, 800);

            // SF Pro Display Boldがインスコされているか検査
            try
            {
                mFontName = "SF Pro Display Bold"; //OpenTypeなのでインスコされてても多分無理
                Font f = new Font(mFontName, 10);
                if (f.Name != mFontName)
                {
                    // 存在しないと違うのになるらしい
                    mFontName = "メイリオ";
                }
            }
            catch
            {
                // これで落ちても困るのでcatch
                mFontName = "メイリオ";
            }

        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="cars">両数</param>
        /// <param name="sta">駅リスト</param>
        public void Init(int cars, Dictionary<int, string> sta)
        {
            int i;
            string logText = "";
            try
            {
                this.pNFB = true;
                this.RailwayTBL.Clear();
                this.pCars = cars;
                if (sta != null)
                {
                    this.Stations = sta;
                }
                this.mStationIndex = 0;
                this.Cautions = new List<CautionData>();
                this.ValidCautions = new List<CautionData>();
                this.DispDetailCaution = null;

                this.LimpSpeedList = new List<int>();
                for (int j = 0; j < this.mBveStaff.StationList.Count - 1; j++)
                {
                    this.LimpSpeedList.Add(-1);
                }

                // 背景
                logText = "base2";
                if (System.IO.File.Exists(ModuleDirectoryPath + "\\castimg\\base2.png") == true)
                {
                    System.Drawing.Image cb = System.Drawing.Image.FromFile(ModuleDirectoryPath + "\\castimg\\base2.png");
                    this.CastBase = new Bitmap(cb, new Size(600, 800));
                }
                else
                {
                    this.CastBase = new Bitmap(600, 800);
                    Log.Write("[Init] " + logText + " file isn't found");
                }
                //素材
                logText = "sozai2";
                if (System.IO.File.Exists(ModuleDirectoryPath + "\\castimg\\sozai2.png") == true)
                {
                    System.Drawing.Image cb = System.Drawing.Image.FromFile(ModuleDirectoryPath + "\\castimg\\sozai2.png");
                    Bitmap Sozai = new Bitmap(cb, new Size(cb.Width, cb.Height));

                    logText = "number";
                    for (i=0; i<5; i++)
                    {
                        ImgNumber58[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(0, 45 * i, 35, 45));
                        ImgNumber58[i + 5] = HL.GraOpe.ClipImage(Sozai, new Rectangle(35, 45 * i, 35, 45));

                        ImgNumber35[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(70, 28 * i, 22, 28));
                        ImgNumber35[i + 5] = HL.GraOpe.ClipImage(Sozai, new Rectangle(92, 28 * i, 22, 28));

                        ImgNumber10[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(80, 9 * i + 170, 8, 9));
                        ImgNumber10[i + 5] = HL.GraOpe.ClipImage(Sozai, new Rectangle(96, 9 * i + 170, 8, 9));

                        ImgNumberSignal[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(181, 12 * i + 162, 8, 9));
                        ImgNumberSignal[i + 5] = HL.GraOpe.ClipImage(Sozai, new Rectangle(197, 12 * i + 163, 8, 9));
                        ImgNumberSignal[i + 10] = HL.GraOpe.ClipImage(Sozai, new Rectangle(209, 12 * i + 163, 19, 10));
                    }
                    ImgNumber10[10] = HL.GraOpe.ClipImage(Sozai, new Rectangle(79, 215, 10, 9));
                    ImgNumber10[11] = HL.GraOpe.ClipImage(Sozai, new Rectangle(96, 215, 8, 9));

                    logText = "number2";
                    for (i = 0; i < 10; i++)
                    {
                        ImgNumber20[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(121, 17 * i + 17, 13, 17));
                        ImgNumber18[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(150, 15 * i + 16, 13, 15));
                        ImgNumber15[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(175, 13 * i + 16, 10, 13));
                        ImgNumber13[i] = HL.GraOpe.ClipImage(Sozai, new Rectangle(196, 12 * i + 14, 8, 12));
                    }

                    ImgStop1 = HL.GraOpe.ClipImage(Sozai, new Rectangle(210, 0, 36, 36));
                    ImgStop2 = HL.GraOpe.ClipImage(Sozai, new Rectangle(210, 36, 26, 26));
                    ImgPass1 = HL.GraOpe.ClipImage(Sozai, new Rectangle(246, 0, 36, 36));
                    ImgPass2 = HL.GraOpe.ClipImage(Sozai, new Rectangle(246, 36, 26, 26));

                    ImgTimetable = HL.GraOpe.ClipImage(Sozai, new Rectangle(210, 62, 24, 24));
                    ImgCaution = HL.GraOpe.ClipImage(Sozai, new Rectangle(234, 62, 10, 10));
                    ImgLimitUnit = HL.GraOpe.ClipImage(Sozai, new Rectangle(234, 77, 25, 9));

                    ImgRail = HL.GraOpe.ClipImage(Sozai, new Rectangle(210, 86, 8, 28));
                    ImgMyTrain = HL.GraOpe.ClipImage(Sozai, new Rectangle(139, 168, 26, 26));
                    ImgSignal = HL.GraOpe.ClipImage(Sozai, new Rectangle(139, 195, 14, 22));
                    ImgCross = HL.GraOpe.ClipImage(Sozai, new Rectangle(139, 217, 34, 14));

                    ImgOneman1 = HL.GraOpe.ClipImage(Sozai, new Rectangle(282, 0, 74, 35));
                    ImgOneman2 = HL.GraOpe.ClipImage(Sozai, new Rectangle(356, 0, 74, 35));

                    ImgBackButton = HL.GraOpe.ClipImage(Sozai, new Rectangle(282, 35, 80, 35));
                    ImgStopButton = HL.GraOpe.ClipImage(Sozai, new Rectangle(362, 35, 116, 35));

                    ImgShimashima1 = HL.GraOpe.ClipImage(Sozai, new Rectangle(430, 0, 18, 12));
                    ImgShimashima2 = HL.GraOpe.ClipImage(Sozai, new Rectangle(430, 12, 16, 16));

                    ImgStopStationCaution = HL.GraOpe.ClipImage(Sozai, new Rectangle(233, 92, 367, 148));
                    ImgCautionYellow = HL.GraOpe.ClipImage(Sozai, new Rectangle(600, 0, 122, 172));
                }
                else
                {
                    for (i = 0; i < 5; i++)
                    {
                        ImgNumber58[i] = new Bitmap(35, 45);
                        ImgNumber58[i + 5] = new Bitmap(35, 45);

                        ImgNumber35[i] = new Bitmap(22, 28);
                        ImgNumber35[i + 5] = new Bitmap(22, 28);

                        ImgNumber10[i] = new Bitmap(8, 9);
                        ImgNumber10[i + 5] = new Bitmap(8, 9);

                        ImgNumberSignal[i] = new Bitmap(8, 9);
                        ImgNumberSignal[i + 5] = new Bitmap(8, 9);
                        ImgNumberSignal[i + 10] = new Bitmap(8, 9);
                    }
                    ImgNumber10[10] = new Bitmap(215, 9);
                    ImgNumber10[11] = new Bitmap(215, 9);

                    for (i = 0; i < 10; i++)
                    {
                        ImgNumber20[i] = new Bitmap(13, 17);
                        ImgNumber18[i] = new Bitmap(13, 15);
                        ImgNumber15[i] = new Bitmap(10, 13);
                        ImgNumber13[i] = new Bitmap(8, 12);
                    }

                    ImgStop1 = new Bitmap(0, 36);
                    ImgStop2 = new Bitmap(36, 26);
                    ImgPass1 = new Bitmap(0, 36);
                    ImgPass2 = new Bitmap(36, 26);

                    ImgTimetable = new Bitmap(24, 24);
                    ImgCaution = new Bitmap(62, 10);
                    ImgLimitUnit = new Bitmap(77, 9);

                    ImgMyTrain = new Bitmap(8, 28);
                    ImgMyTrain = new Bitmap(86, 26);
                    ImgSignal = new Bitmap(14, 22);
                    ImgCross = new Bitmap(34, 14);

                    ImgOneman1 = new Bitmap(0, 35);
                    ImgOneman2 = new Bitmap(0, 35);

                    ImgBackButton = new Bitmap(35, 35);
                    ImgStopButton = new Bitmap(35, 35);

                    ImgShimashima1 = new Bitmap(18, 12);
                    ImgShimashima2 = new Bitmap(16, 16);

                    ImgCautionYellow = new Bitmap(116, 166);

                    Log.Write("[Init] " + logText + " file isn't found");
                }
                // 停車目標(使用時に読み込む)
                logText = "STI";
                this.StopTargetImage = new Dictionary<int, Bitmap>();

                // スタフリストから始発駅を検索
                // StationListの中でLocationが一番小さいもの(Location=-1は実際のマップに存在しないのでスキップ)
                this.mDeltaStaIndex = -1;
                if(this.mBveStaff != null && this.mBveStaff.StationList != null)
                {
                    for(i=0; i< this.mBveStaff.StationList.Count;i++)
                    {
                        //Staff.StaffData.clsHensuu ssd = this.mBveStaff.StaffPack.StationList[i];
                        HL.Bve.Station.OneStation ssd = this.mBveStaff.StationList[i];
                        if (ssd.Location >= 0 && (this.mDeltaStaIndex == -1 || ssd.Location < this.mBveStaff.StationList[this.mDeltaStaIndex].Location))
                        {
                            this.mDeltaStaIndex = i;
                        }
                    }
                }
                if (this.mDeltaStaIndex == -1)
                {
                    this.mDeltaStaIndex = 0;
                }

                //毎回生成しなくていいもの
                logText = "Header";
                DrawHeader();
                logText = "Staff";
                DrawStaff();
                logText = "Next";
                DrawNext();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.Init]" + logText, false);
            }
        }

        public void Dispose()
        {
            if (this.CastBase != null)
            {
                this.CastBase.Dispose();
            }
            if (this.CastHeader != null)
            {
                this.CastHeader.Dispose();
            }
            if (this.ImgStop1 != null)
            {
                this.ImgStop1.Dispose();
            }
            if (this.ImgStop2 != null)
            {
                this.ImgStop2.Dispose();
            }
            if (this.ImgPass1 != null)
            {
                this.ImgPass1.Dispose();
            }
            if (this.ImgStop2 != null)
            {
                this.ImgStop2.Dispose();
            }
            if (this.ImgTimetable != null)
            {
                this.ImgTimetable.Dispose();
            }
            if (this.ImgCaution != null)
            {
                this.ImgCaution.Dispose();
            }
            if (this.ImgLimitUnit != null)
            {
                this.ImgLimitUnit.Dispose();
            }
            if (this.ImgMyTrain != null)
            {
                this.ImgMyTrain.Dispose();
            }
            if (this.ImgSignal != null)
            {
                this.ImgSignal.Dispose();
            }
            if (this.ImgCross != null)
            {
                this.ImgCross.Dispose();
            }
            if (this.ImgTunnelStart != null)
            {
                this.ImgTunnelStart.Dispose();
            }
            if (this.ImgTunnelEnd != null)
            {
                this.ImgTunnelEnd.Dispose();
            }
            if (this.ImgOneman1 != null)
            {
                this.ImgOneman1.Dispose();
            }
            if (this.ImgOneman2 != null)
            {
                this.ImgOneman2.Dispose();
            }
            if (this.ImgBackButton != null)
            {
                this.ImgBackButton.Dispose();
            }
            if (this.ImgStopButton != null)
            {
                this.ImgStopButton.Dispose();
            }
            for (int i=0; i<12; i++)
            {
                if(i < 10)
                {
                    if (this.ImgNumber58[i] != null)
                    {
                        this.ImgNumber58[i].Dispose();
                    }
                    if (this.ImgNumber35[i] != null)
                    {
                        this.ImgNumber35[i].Dispose();
                    }
                    if (this.ImgNumber20[i] != null)
                    {
                        this.ImgNumber20[i].Dispose();
                    }
                    if (this.ImgNumber18[i] != null)
                    {
                        this.ImgNumber18[i].Dispose();
                    }
                    if (this.ImgNumber15[i] != null)
                    {
                        this.ImgNumber15[i].Dispose();
                    }
                    if (this.ImgNumber13[i] != null)
                    {
                        this.ImgNumber13[i].Dispose();
                    }
                }
                if (this.ImgNumber10[i] != null)
                {
                    this.ImgNumber10[i].Dispose();
                }
            }
            if (this.StopTargetImage != null)
            {
                foreach(KeyValuePair<int, Bitmap> img in this.StopTargetImage)
                {
                    if (img.Value != null)
                    {
                        img.Value.Dispose();
                    }
                }
            }
            this.CastDisp.Dispose();
        }

        #region 描画
        public Bitmap DrawCast(Size dispSize)
        {
            return DrawCast(dispSize, mNowLocation, mPanelLocation, mSpeed, mTime, this.mStationIndex - this.mDeltaStaIndex);
        }
        public Bitmap DrawCast(Size dispSize, int nowLocation, int panelLocation, double pSpeed, int pTime)
        {
            return DrawCast(dispSize, nowLocation, panelLocation, pSpeed, pTime, this.mStationIndex - this.mDeltaStaIndex);
        }

        public Bitmap DrawCast(Size dispSize, int nowLocation, int panelLocation, double pSpeed, int pTime, int pStationIndex)
        {
            Bitmap rtn = new Bitmap(dispSize.Width, dispSize.Height);
            try
            {
                if (this.FrmMode == false)
                {
                    if (this.pIsDrawing == false)
                    {
                        this.pIsDrawing = true;
                        var task = Task.Run(() =>
                        {
                            this.pCastImage = DrawCast_Worker(dispSize, nowLocation, panelLocation, pSpeed, pTime, pStationIndex);
                            this.pIsDrawing = false;
                        });
                    }
                    rtn = null;
                }
                else
                {
                    rtn = DrawCast_Worker(dispSize, nowLocation, panelLocation, pSpeed, pTime, pStationIndex);
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawCast]", false);
            }
            return rtn;
        }

        private Bitmap DrawCast_Worker(Size dispSize, int nowLocation, int panelLocation, double pSpeed, int pTime, int pStationIndex)
        {
            Bitmap rtn = new Bitmap(dispSize.Width, dispSize.Height);
            mNowLocation = nowLocation;
            mPanelLocation = panelLocation;
            mSpeed = pSpeed;
            mTime = pTime;
            try
            {
                // 表示通告
                //Log.Write("★DrawCast - Start");
                //Log.Write("★DrawCast - ValidCautions");
                if (this.ValidCautions != null)
                {
                    for (int i = this.ValidCautions.Count - 1; i >= 0; i--)
                    {
                        CautionData OneCaution = this.ValidCautions[i];
                        // 徐行・規制で100m以上過ぎた && 詳細表示中ではない
                        if (OneCaution.Type >= 1 && OneCaution.Location2 + 100 < nowLocation && (this.DispDetailCaution == null || this.ValidCautions.IndexOf(this.DispDetailCaution) == -1))
                        {
                            this.ValidCautions.RemoveAt(i);
                            CheckLimpSpeedList();
                        }
                        // 初回の注意事項
                        else if (OneCaution.Type == 0 && OneCaution.Location1 == 0 && OneCaution.Location2 == 0 && OneCaution.Received == 2)
                        {
                            continue;
                        }
                        // 確認済みの時刻表
                        else if (OneCaution.Type == -1 && OneCaution.Received >= 2)
                        {
                            this.ValidCautions.RemoveAt(i);
                            CheckLimpSpeedList();
                        }
                    }
                }

                // 駅インデックス更新
                //Log.Write("★DrawCast - DrawNext");
                if (this.mStationIndex != pStationIndex + this.mDeltaStaIndex)
                {
                    this.mStationIndex = pStationIndex + this.mDeltaStaIndex;
                    DrawNext();
                }
                else
                {
                    this.mStationIndex = pStationIndex + this.mDeltaStaIndex;
                }

                //Log.Write("★DrawCast - DrawRailwayPanel");
                DrawRailwayPanel(mNowLocation, panelLocation, pTime);

                //Log.Write("★DrawCast - Image");
                using (Graphics g = Graphics.FromImage(rtn))
                {
                    // 指定サイズに拡大縮小
                    g.DrawImage(this.CastDisp,
                                0, 0, dispSize.Width, dispSize.Height);
                    //g.DrawImage(this.CastDisp,
                    //            new Rectangle(0, 0, dispSize.Width, dispSize.Height),
                    //            new Rectangle(0, 0, this.CastDisp.Width, this.CastDisp.Height),
                    //            GraphicsUnit.Pixel);
                    //rtn = new Bitmap(this.CastDisp, dispSize); // 遅い
                }

                //Log.Write("★DrawCast - End");
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawCast]", false);
            }
            return rtn;
        }

        #region ヘッダー
        /// <summary>
        /// ヘッダー部分を作る
        /// </summary>
        public void DrawHeader()
        {
            string logText = "";
            try
            {
                // 基本的に不変(列番・車種・両数・通告が変わらなければ)なので単発生成して使い回す
                if (this.pCars < 0)
                {
                    this.pCars = 0;
                }
                else if (99 < this.pCars)
                {
                    this.pCars = 99;
                }

                //
                logText = "graphics";
                this.CastHeader = new Bitmap(600, 93);

                using (Graphics g = Graphics.FromImage(this.CastHeader))
                {
                    // ベース
                    logText = "base";
                    g.DrawImage(HL.GraOpe.ClipImage(this.CastBase, new Rectangle(0, 0, 600, 93)), new Point(0, 0));
                    //g.FillRectangle(new SolidBrush(Color.FromArgb(28, 28, 30)), new RectangleF(0, 93, 600, 180));

                    logText = "cars";
                    int deltaW = 0;
                    if (10 <= this.pCars)
                    {
                        // 両数
                        HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)), new Rectangle(236, 15, 93, 70), 6);
                        g.DrawImage(this.ImgNumber58[(int)(this.pCars / 10)], new Point(247, 27));
                        g.DrawImage(this.ImgNumber58[(int)(this.pCars % 10)], new Point(282, 27));
                        deltaW = -35;
                    }
                    else
                    {
                        // 両数
                        HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)), new Rectangle(271, 15, 58, 70), 6);
                        g.DrawImage(this.ImgNumber58[this.pCars], new Point(282, 27));
                    }

                    // 車種
                    logText = "carkind";
                    SizeF sizef = g.MeasureString(this.mBveStaff.StaffHeader.Keisiki, new Font(this.mFontName, 14));
                    if (this.mBveStaff != null && this.mBveStaff.StaffHeader.Keisiki != "")
                    {
                        g.PageUnit = GraphicsUnit.Pixel;
                        if(sizef.Width > 55)
                        {
                            HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)),
                                                       new Rectangle(182 + deltaW + 55 - (int)sizef.Width, 48 - (int)(sizef.Height / 2), (int)sizef.Width + 20, (int)sizef.Height + 6), 3);
                            g.DrawString(this.mBveStaff.StaffHeader.Keisiki, new Font(this.mFontName, 14), Brushes.White,
                                         new Point(192 + deltaW + 55 - (int)sizef.Width, 51 - (int)(sizef.Height / 2)));
                        }
                        else
                        {
                            HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)),
                                                       new Rectangle(182 + deltaW, 48 - (int)(sizef.Height / 2), 75, (int)sizef.Height + 6), 3);
                            g.DrawString(this.mBveStaff.StaffHeader.Keisiki, new Font(this.mFontName, 14), Brushes.White,
                                         new Point(219 - (int)(sizef.Width / 2) + deltaW, 51 - (int)(sizef.Height / 2)));
                        }
                    }

                    // 列番
                    logText = "train number";
                    if (this.mBveStaff != null && this.mBveStaff.StaffHeader.TrainNumber != "")
                    {
                        int tnx = 104 + deltaW;
                        if(sizef.Width > 55)
                        {
                            tnx -= (int)(sizef.Width - 55);
                        }
                        HL.GraOpe.DrawString(this.mBveStaff.StaffHeader.TrainNumber, g, new Font(this.mFontName, 14),
                                             new Rectangle(tnx, 37, 56, 15), Brushes.White, GraOpe.ENUM_TextAlign.RightTop);
                    }

                    // 最高速度
                    logText = "maxspeed";
                    if (this.mBveStaff != null)
                    {
                        if (this.mBveStaff.StaffHeader.MaxSpeed3 > 0)
                        {
                            // 制限1
                            DrawMaxSpeed(g, 19, this.mBveStaff.StaffHeader.MaxArea1, this.mBveStaff.StaffHeader.MaxSpeed1);
                            // 制限2
                            DrawMaxSpeed(g, 43, this.mBveStaff.StaffHeader.MaxArea2, this.mBveStaff.StaffHeader.MaxSpeed2);
                            // 制限3
                            DrawMaxSpeed(g, 67, this.mBveStaff.StaffHeader.MaxArea3, this.mBveStaff.StaffHeader.MaxSpeed3);
                        }
                        else if (this.mBveStaff.StaffHeader.MaxSpeed2 > 0)
                        {
                            // 制限1
                            DrawMaxSpeed(g, 31, this.mBveStaff.StaffHeader.MaxArea1, this.mBveStaff.StaffHeader.MaxSpeed1);
                            // 制限2
                            DrawMaxSpeed(g, 55, this.mBveStaff.StaffHeader.MaxArea2, this.mBveStaff.StaffHeader.MaxSpeed2);
                        }
                        else
                        {
                            // 制限1
                            DrawMaxSpeed(g, 43, this.mBveStaff.StaffHeader.MaxArea1, this.mBveStaff.StaffHeader.MaxSpeed1);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawHeader]" + logText, false);
            }
        }
        private void DrawMaxSpeed(Graphics g, int pTop, string pMaxArea, int pMaxSpeed)
        {
            try
            {
                // 制限区間
                HL.GraOpe.DrawString(pMaxArea, g, new Font("メイリオ", 8),
                                     new Rectangle(340, pTop + 1, 56, 15), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                // 制限速度
                if (100 <= pMaxSpeed && pMaxSpeed <= 999)
                {
                    g.DrawImage(this.ImgNumber18[(int)(pMaxSpeed / 100)], new Point(380, pTop));
                    g.DrawImage(this.ImgNumber18[(int)(pMaxSpeed / 10) % 10], new Point(393, pTop));
                    g.DrawImage(this.ImgNumber18[pMaxSpeed % 10], new Point(406, pTop));
                }
                else if (10 <= pMaxSpeed && pMaxSpeed <= 99)
                {
                    g.DrawImage(this.ImgNumber18[(int)(pMaxSpeed / 10)], new Point(386, pTop));
                    g.DrawImage(this.ImgNumber18[pMaxSpeed % 10], new Point(399, pTop));
                }
                else if (0 <= pMaxSpeed && pMaxSpeed <= 9)
                {
                    g.DrawImage(this.ImgNumber18[pMaxSpeed], new Point(393, pTop));
                }
                g.DrawImage(this.ImgLimitUnit, new Point(424, pTop + 6));
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawMaxSpeed]", false);
            }
        }
        #endregion

        #region スタフ
        private void DrawStaff()
        {
            string errormsg = "";
            try
            {
                if (this.mBveStaff == null)
                {
                    return;
                }

                //Dim model As Model.DataTable1 = RenewCastPanel(str);
                this.CastStaff = new Bitmap(150, this.mBveStaff.StationList.Count * 90 + 800);

                using (Graphics g = Graphics.FromImage(this.CastStaff))
                {
                    g.FillRectangle(Brushes.Black, new Rectangle(0, 0, this.CastStaff.Width, this.CastStaff.Height));
                    g.FillRectangle(new SolidBrush(Color.FromArgb(90, 90, 90)), new Rectangle(47, 0, 2, this.CastStaff.Height));

                    //int cnt = 0;

                    for (int i=0;i < this.mBveStaff.StationList.Count;i++)
                    {
                        //Staff.StaffData.clsHensuu oneStation = this.mBveStaff.StaffPack.StationList[i];
                        HL.Bve.Station.OneStation oneStation = this.mBveStaff.StationList[i];
                        // 
                        int BaseTop = 630 + 90 * (this.mBveStaff.StationList.Count - 1 - i);
                        if (oneStation.StaffHensuu.ChakuFlag == "P")
                        {
                            // 通過
                            HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(90, 90, 90)), new Rectangle(54, BaseTop, 88, 56), 3);
                            HL.GraOpe.FillArcRectangle(g, Brushes.Black, new Rectangle(56, BaseTop + 2, 84, 52), 3);
                            HL.GraOpe.DrawString(oneStation.StationName, g, new Font("メイリオ", 12), 
                                                 new Rectangle(62, BaseTop + 7, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);

                            g.DrawImage(this.ImgNumber10[10], new Point(85, BaseTop + 37));
                        }
                        else
                        {
                            // 停車
                            HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(38, 66, 114)), new Rectangle(22, BaseTop, 120, 56), 3);
                            HL.GraOpe.FillArcRectangle(g, Brushes.Black, new Rectangle(24, BaseTop + 2, 116, 52), 3);
                            HL.GraOpe.DrawString(oneStation.StationName, g, new Font("メイリオ", 12), 
                                                 new Rectangle(30, BaseTop + 7, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                            switch(oneStation.StaffHensuu.ChakuFlag)
                            {
                                case "P": // 通過
                                    break;
                                case "Z": // 停車(無時)
                                    break;
                                case "X": // 停車(同時)
                                    g.DrawImage(this.ImgNumber10[11], new Point(44, BaseTop + 37));
                                    g.DrawImage(this.ImgNumber13[(int)(oneStation.ArrivalTimeDate.Minute / 10)], new Point(49, BaseTop + 34));
                                    g.DrawImage(this.ImgNumber13[oneStation.ArrivalTimeDate.Minute % 10], new Point(57, BaseTop + 34));
                                    g.DrawImage(this.ImgNumber10[(int)(oneStation.ArrivalTimeDate.Second / 10)], new Point(65, BaseTop + 37));
                                    g.DrawImage(this.ImgNumber10[oneStation.ArrivalTimeDate.Second % 10], new Point(73, BaseTop + 37));
                                    break;
                                default: // 停車(異時)
                                    if (oneStation.ArrivalTimeDate.Hour >= 10)
                                    {
                                        g.DrawImage(this.ImgNumber13[(int)(oneStation.ArrivalTimeDate.Hour / 10)], new Point(29, BaseTop + 34));
                                    }
                                    g.DrawImage(this.ImgNumber13[oneStation.ArrivalTimeDate.Hour % 10], new Point(37, BaseTop + 34));
                                    g.DrawImage(this.ImgNumber10[11], new Point(44, BaseTop + 37));
                                    g.DrawImage(this.ImgNumber13[(int)(oneStation.ArrivalTimeDate.Minute / 10)], new Point(49, BaseTop + 34));
                                    g.DrawImage(this.ImgNumber13[oneStation.ArrivalTimeDate.Minute % 10], new Point(57, BaseTop + 34));
                                    g.DrawImage(this.ImgNumber10[(int)(oneStation.ArrivalTimeDate.Second / 10)], new Point(65, BaseTop + 37));
                                    g.DrawImage(this.ImgNumber10[oneStation.ArrivalTimeDate.Second % 10], new Point(73, BaseTop + 37));
                                    break;
                            }
                        }
                        switch (oneStation.StaffHensuu.HatsuFlag)
                        {
                            case "P": // 通過
                                g.DrawImage(this.ImgNumber10[10], new Point(119, BaseTop + 37));
                                break;
                            case "T": // 終点
                                break;
                            case "Z": // 停車(無時)
                                g.DrawImage(this.ImgNumber10[10], new Point(119, BaseTop + 37));
                                break;
                            case "X": // 停車(同時)
                                g.DrawImage(this.ImgNumber10[11], new Point(98, BaseTop + 37));
                                g.DrawImage(this.ImgNumber13[(int)(oneStation.DefaultTimeDate.Minute / 10)], new Point(103, BaseTop + 34));
                                g.DrawImage(this.ImgNumber13[oneStation.DefaultTimeDate.Minute % 10], new Point(111, BaseTop + 34));
                                g.DrawImage(this.ImgNumber10[(int)(oneStation.DefaultTimeDate.Second / 10)], new Point(119, BaseTop + 37));
                                g.DrawImage(this.ImgNumber10[oneStation.DefaultTimeDate.Second % 10], new Point(127, BaseTop + 37));
                                break;
                            default: // 停車(異時)
                                if (oneStation.DefaultTimeDate.Hour >= 10)
                                {
                                    g.DrawImage(this.ImgNumber13[(int)(oneStation.DefaultTimeDate.Hour / 10)], new Point(83, BaseTop + 34));
                                }
                                g.DrawImage(this.ImgNumber13[oneStation.DefaultTimeDate.Hour % 10], new Point(91, BaseTop + 34));
                                g.DrawImage(this.ImgNumber10[11], new Point(98, BaseTop + 37));
                                g.DrawImage(this.ImgNumber13[(int)(oneStation.DefaultTimeDate.Minute / 10)], new Point(103, BaseTop + 34));
                                g.DrawImage(this.ImgNumber13[oneStation.DefaultTimeDate.Minute % 10], new Point(111, BaseTop + 34));
                                g.DrawImage(this.ImgNumber10[(int)(oneStation.DefaultTimeDate.Second / 10)], new Point(119, BaseTop + 37));
                                g.DrawImage(this.ImgNumber10[oneStation.DefaultTimeDate.Second % 10], new Point(127, BaseTop + 37));
                                break;
                        }

                        // 所要時間
                        if (oneStation.StaffHensuu.UntenJihunMin > 0 || (oneStation.StaffHensuu.UntenJihunMin == 0 && oneStation.StaffHensuu.UntenJihunSec > 0))
                        {
                            if(oneStation.StaffHensuu.UntenJihunGap > 0)
                            {
                                int dh = 45 * (oneStation.StaffHensuu.UntenJihunGap - 1);

                                if (oneStation.StaffHensuu.UntenJihunMin >= 10)
                                {
                                    g.DrawImage(this.ImgNumber18[(int)(oneStation.StaffHensuu.UntenJihunMin / 10)], new Point(1, BaseTop + 65 + dh));
                                }
                                g.DrawImage(this.ImgNumber18[oneStation.StaffHensuu.UntenJihunMin % 10], new Point(14, BaseTop + 65 + dh));
                                g.DrawImage(this.ImgNumber13[(int)(oneStation.StaffHensuu.UntenJihunSec / 10)], new Point(29, BaseTop + 68 + dh));
                                g.DrawImage(this.ImgNumber13[oneStation.StaffHensuu.UntenJihunSec % 10], new Point(37, BaseTop + 68 + dh));
                            }
                        }
                    }


                    // Graphicsオブジェクトのリソースを解放する
                    g.Dispose();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawStaff] : " + errormsg, false);
            }
        }
        #endregion

        #region 次駅表示
        private void DrawNext()
        {
            string errormsg = "";
            try
            {
                if (this.mBveStaff == null)
                {
                    Log.Write("[DrawNext] (Info) Staff is null");
                    return;
                }
                else if (this.mStationIndex < 0)
                {
                    Log.Write("[DrawNext] (Info) Index=" + this.mBveStaff.ToString());
                    return;
                }
                else if (this.mStationIndex >= this.mBveStaff.StationList.Count)
                {
                    Log.Write("[DrawNext] (Info) Index=" + this.mBveStaff.ToString() + " > " + this.mBveStaff.StationList.Count.ToString());
                    return;
                }

                //Dim model As Model.DataTable1 = RenewCastPanel(str);
                this.CastNext = new Bitmap(374, 800);

                using (Graphics g = Graphics.FromImage(this.CastNext))
                {
                    //g.FillRectangle(Brushes.Black, new Rectangle(0, 0, this.CastStaff.Width, this.CastStaff.Height));
                    //g.FillRectangle(new SolidBrush(Color.FromArgb(90, 90, 90)), new Rectangle(47, 0, 2, this.CastStaff.Height));

                    // 徐行・規制
                    errormsg = "徐行・規制 開始点";
                    int StartIndex;
                    if (this.mStationIndex <= 0)
                    {
                        StartIndex = 0;
                    }
                    else if (this.mStationIndex >= this.mBveStaff.StationList.Count)
                    {
                        StartIndex = this.mBveStaff.StationList.Count - 1;
                    }
                    else
                    {
                        StartIndex = this.mStationIndex - 1;
                    }

                    errormsg = "徐行・規制 該当区間";
                    if (this.ValidCautions != null)
                    {
                        if (this.LimpSpeedList != null)
                        {
                            for (int j = StartIndex; j < this.mBveStaff.StationList.Count - 1; j++)
                            {
                                if (j < this.LimpSpeedList.Count && this.LimpSpeedList[j] != -1)
                                {
                                    if (-1 == (j - this.mStationIndex))
                                    {
                                        // 塗りつぶし
                                        int CBaseTop = 476 + 95;
                                        int CBaseX01 = 139 - 114 * CBaseTop / 800; // (290->176)
                                        int CBaseX02 = 139 - 114; // (290->176)
                                        int CBaseX11 = 249 + 114 * CBaseTop / 800; // (514->400)
                                        int CBaseX12 = 249 + 114; // (514->400)
                                        g.FillPolygon(new SolidBrush(Color.FromArgb(128, 128, 0)), new Point[] { new Point(CBaseX01, CBaseTop), new Point(CBaseX11, CBaseTop), new Point(CBaseX12, 800), new Point(CBaseX02, 800) });
                                        CBaseTop = 510 + 95*0 + 145;
                                        // シマシマ
                                        HL.GraOpe.DrawImageContinuous(g, this.ImgShimashima1, new Rectangle(37, CBaseTop + 11, 316, 12));
                                        // 徐行速度背景
                                        g.FillEllipse(Brushes.Black, new Rectangle(181, CBaseTop + 3, 28, 28));
                                        g.FillEllipse(Brushes.Yellow, new Rectangle(183, CBaseTop + 5, 24, 24));
                                        // 徐行速度
                                        HL.GraOpe.DrawString(this.LimpSpeedList[j].ToString("0"),
                                            g, new Font("ＭＳ ゴシック", 12), new Rectangle(183, CBaseTop + 5, 24, 24), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                                    }
                                    else if (0 <= (j - this.mStationIndex))
                                    {
                                        // 塗りつぶし
                                        int CBaseTop = 476 - 95 * (j - this.mStationIndex);
                                        int CBaseX01 = 139 - 114 * CBaseTop / 800; // (290->176)
                                        int CBaseX02 = 139 - 114 * (CBaseTop + 33) / 800; // (290->176)
                                        int CBaseX11 = 249 + 114 * CBaseTop / 800; // (514->400)
                                        int CBaseX12 = 249 + 114 * (CBaseTop + 33) / 800; // (514->400)
                                        g.FillPolygon(new SolidBrush(Color.FromArgb(128, 128, 0)), new Point[] { new Point(CBaseX01, CBaseTop), new Point(CBaseX11, CBaseTop), new Point(CBaseX12, CBaseTop + 33), new Point(CBaseX02, CBaseTop + 33) });
                                        // シマシマ
                                        HL.GraOpe.DrawImageContinuous(g, this.ImgShimashima1, new Rectangle(37, CBaseTop + 11, 316, 12));
                                        // 徐行速度背景
                                        g.FillEllipse(Brushes.Black, new Rectangle(181, CBaseTop + 3, 28, 28));
                                        g.FillEllipse(Brushes.Yellow, new Rectangle(183, CBaseTop + 5, 24, 24));
                                        // 徐行速度
                                        HL.GraOpe.DrawString(this.LimpSpeedList[j].ToString("0"),
                                            g, new Font("ＭＳ ゴシック", 12), new Rectangle(183, CBaseTop + 5, 24, 24), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                                    }
                                }
                                else if (j < this.LimpSpeedList.Count && this.LimpSpeedList[j] == -1)
                                {
                                    // 徐行なし
                                    if (0 <= (j - this.mStationIndex))
                                    {
                                        // 塗りつぶし
                                        int CBaseTop = 476 - 95 * (j - this.mStationIndex);
                                        g.FillRectangle(Brushes.Black, new Rectangle(37, CBaseTop + 11, 316, 12));
                                    }
                                }
                                else if (this.LimpSpeedList.Count <= j)
                                {
                                    Log.Write("[Cast.DrawNext] (Info) LimpSpeedList[" + j.ToString() + "] is null");
                                }
                            }
                        }
                        else
                        {
                            Log.Write("[Cast.DrawNext] (Info) LimpSpeedList is null");
                        }

                    }

                    // 各駅の情報
                    for (int i = 0; i < this.mBveStaff.StationList.Count; i++)
                    {
                        //Staff.StaffData.clsHensuu oneStation = this.mBveStaff.StaffPack.StationList[i];
                        HL.Bve.Station.OneStation oneStation = this.mBveStaff.StationList[i];
                        // 
                        int BaseTop = 510 - 95 * (i - this.mStationIndex);

                        if (i == this.mStationIndex)
                        {
                            // 次の駅

                            // 枠
                            //X:164-151 w:361 Y:510 h:141
                            if (IsPass() == false)
                            {
                                // 停車
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(38, 66, 114)), new Rectangle(13, 510, 361, 141), 4);
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)), new Rectangle(16, 513, 355, 135), 4);
                            }
                            else
                            {
                                // 通過
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(90, 90, 90)), new Rectangle(13, 510, 361, 141), 4);
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)), new Rectangle(16, 513, 355, 135), 4);
                            }

                            if (this.pNearStation < 2)
                            {
                                // 駅名
                                HL.GraOpe.DrawString(oneStation.StationName, g, new Font("メイリオ", 22), 
                                                     new Rectangle(30, BaseTop + 22, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                // 停通
                                if (IsPass() == false)
                                {
                                    // 停車
                                    g.DrawImage(this.ImgStop1, new Point(172, BaseTop + 26));
                                }
                                else
                                {
                                    // 通過
                                    g.DrawImage(this.ImgPass1, new Point(172, BaseTop + 26));
                                }

                                // 番線
                                HL.GraOpe.DrawString(oneStation.StaffHensuu.Bansen1, g, new Font("メイリオ", 22), 
                                                     new Rectangle(225, BaseTop + 22, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                // 停車目標準備
                                // SetBeaconData内で処理

                                // 停車目標
                                if (oneStation.StaffHensuu.ChakuFlag != "P" && this.StopTargetImage.ContainsKey(this.pStopTarget1) == true)
                                {
                                    g.DrawImage(this.StopTargetImage[this.pStopTarget1], 293, BaseTop + 21, 62, 62);
                                }
                                // 着時刻
                                if (oneStation.StaffHensuu.ChakuFlag != "P" && oneStation.StaffHensuu.ChakuFlag != "Z")
                                {
                                    // 通過や着時刻未指定停車ではない場合は着時刻
                                    DrawTime(oneStation.ArrivalTimeDate);
                                    HL.GraOpe.DrawString("着", g, new Font("メイリオ", 9), new Rectangle(120, BaseTop + 89, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                else if (oneStation.StaffHensuu.ChakuFlag == "P" && oneStation.DefaultTimeDate != DateTime.MinValue)
                                {
                                    // 通過の場合は発時刻 (自分秒すべて0は非採時駅通過扱い)
                                    DrawTime(oneStation.DefaultTimeDate);
                                    HL.GraOpe.DrawString("発", g, new Font("メイリオ", 9), new Rectangle(120, BaseTop + 89, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                // ワンマン
                                if (oneStation.StaffHensuu.ChakuFlag != "P")
                                {
                                    if (oneStation.StaffHensuu.ZensyaKinshi == 2)
                                    {
                                        // 全車
                                        g.DrawImage(this.ImgOneman1, new Point(281, BaseTop + 89));
                                    }
                                    else if (oneStation.StaffHensuu.ZensyaKinshi == 1)
                                    {
                                        // 自車
                                        g.DrawImage(this.ImgOneman2, new Point(281, BaseTop + 89));
                                    }
                                }

                            }
                            else
                            {
                                // ドアが開いている

                                // 駅名
                                HL.GraOpe.DrawString(oneStation.StationName, g, new Font("メイリオ", 22), 
                                                     new Rectangle(30, BaseTop + 22, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                int strWidth = (int)g.MeasureString(oneStation.StationName + "　", new Font("メイリオ", 22)).Width + 30;
                                // 着時刻
                                if (oneStation.StaffHensuu.HatsuFlag == "T")
                                {
                                    // 終着駅
                                    HL.GraOpe.DrawString("乗務終了", g, new Font("メイリオ", 9), 
                                                         new Rectangle(strWidth, BaseTop + 40, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                else if (oneStation.StaffHensuu.HatsuFlag != "Z")
                                {
                                    // 着時刻未指定でなければ着時刻を表示
                                    if (oneStation.DefaultTimeDate.Hour >= 10)
                                    {
                                        g.DrawImage(this.ImgNumber20[(int)(oneStation.DefaultTimeDate.Hour / 10)], new Point(strWidth, BaseTop + 38));
                                    }
                                    g.DrawImage(this.ImgNumber20[oneStation.DefaultTimeDate.Hour % 10], new Point(strWidth+13, BaseTop + 38));
                                    g.DrawImage(this.ImgNumber10[11], new Point(strWidth+26, BaseTop + 44));
                                    g.DrawImage(this.ImgNumber20[(int)(oneStation.DefaultTimeDate.Minute / 10)], new Point(strWidth+34, BaseTop + 38));
                                    g.DrawImage(this.ImgNumber20[oneStation.DefaultTimeDate.Minute % 10], new Point(strWidth+47, BaseTop + 38));
                                    g.DrawImage(this.ImgNumber15[(int)(oneStation.DefaultTimeDate.Second / 10)], new Point(strWidth+63, BaseTop + 42));
                                    g.DrawImage(this.ImgNumber15[oneStation.DefaultTimeDate.Second % 10], new Point(strWidth+73, BaseTop + 42));
                                    HL.GraOpe.DrawString("発", g, new Font("メイリオ", 9), 
                                                         new Rectangle(strWidth+87, BaseTop + 40, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                // 番線
                                HL.GraOpe.DrawString(oneStation.StaffHensuu.Bansen1, g, new Font("メイリオ", 22), 
                                                     new Rectangle(strWidth+110, BaseTop + 22, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                            }
                            // 制限速度
                            // - 制限1
                            if (oneStation.StaffHensuu.SpeedLimit1 > 0 && (oneStation.StaffHensuu.ChakuFlag == "P" || oneStation.StaffHensuu.ChakuFlag != "P" && this.pNearStation < 2))
                            {
                                HL.GraOpe.FillArcRectangle(g, Brushes.White, new Rectangle(32, BaseTop + 140, 52, 30), 6);
                                g.FillRectangle(Brushes.White, new Rectangle(32, BaseTop + 140, 52, 24));
                                HL.GraOpe.DrawString(oneStation.StaffHensuu.SpeedLimit1.ToString(), g, new Font(this.mFontName, 14, FontStyle.Bold), 
                                                     new Rectangle(32, BaseTop + 147, 52, 20), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                            }
                            // - 制限2
                            if (oneStation.StaffHensuu.SpeedLimit2 > 0 && (oneStation.StaffHensuu.ChakuFlag == "P" || oneStation.StaffHensuu.ChakuFlag != "P" && this.pNearStation >= 2))
                            {
                                HL.GraOpe.FillArcRectangle(g, Brushes.White, new Rectangle(32, BaseTop - 30, 52, 30), 6);
                                g.FillRectangle(Brushes.White, new Rectangle(32, BaseTop - 24, 52, 24));
                                HL.GraOpe.DrawString(oneStation.StaffHensuu.SpeedLimit2.ToString(), g, new Font(this.mFontName, 14, FontStyle.Bold), 
                                                     new Rectangle(32, BaseTop - 23, 52, 20), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                            }
                        }
                        else if(i > this.mStationIndex)
                        {
                            // 先の駅
                            // 枠
                            //X:188-151 w:316 Y: h:60
                            if (IsPass() == false)
                            {
                                // 停車
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(38, 66, 114)), new Rectangle(37, BaseTop, 316, 60), 3);
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)), new Rectangle(39, BaseTop + 2, 312, 56), 3);
                            }
                            else
                            {
                                // 通過
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(90, 90, 90)), new Rectangle(37, BaseTop, 316, 60), 3);
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(43, 42, 47)), new Rectangle(39, BaseTop + 2, 312, 56), 3);
                            }

                            HL.GraOpe.DrawString(oneStation.StationName, g, new Font("メイリオ", 18), 
                                                 new Rectangle(54, BaseTop + 14, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                            // - 着発マーク
                            if (oneStation.StaffHensuu.ChakuFlag != "P")
                            {
                                g.DrawImage(this.ImgStop2, new Point(182, BaseTop + 17));
                            }
                            else
                            {
                                g.DrawImage(this.ImgPass2, new Point(182, BaseTop + 17));
                            }
                        }


                        // [Sub] 着時刻停通
                        bool IsPass()
                        {
                            switch (oneStation.StaffHensuu.ChakuFlag)
                            {
                                case "P": // 通過
                                    return true;
                                case "Z": // 停車(無時)
                                    break;
                                case "X": // 停車(同時)
                                    break;
                                default: // 停車(異時)
                                    break;
                            }
                            return false;
                        }

                        // [Sub] 時刻表示
                        void DrawTime(DateTime dt)
                        {
                            if (dt.Hour >= 10)
                            {
                                g.DrawImage(this.ImgNumber20[(int)(dt.Hour / 10)], new Point(37, BaseTop + 87));
                            }
                            g.DrawImage(this.ImgNumber20[dt.Hour % 10], new Point(50, BaseTop + 87));
                            g.DrawImage(this.ImgNumber10[11], new Point(63, BaseTop + 93));
                            g.DrawImage(this.ImgNumber20[(int)(dt.Minute / 10)], new Point(71, BaseTop + 87));
                            g.DrawImage(this.ImgNumber20[dt.Minute % 10], new Point(84, BaseTop + 87));
                            g.DrawImage(this.ImgNumber15[(int)(dt.Second / 10)], new Point(100, BaseTop + 91));
                            g.DrawImage(this.ImgNumber15[dt.Second % 10], new Point(110, BaseTop + 91));
                        }

                    }

                    // Graphicsオブジェクトのリソースを解放する
                    g.Dispose();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawNext]" + errormsg, false);
            }
        }
        #endregion

        private void DrawRailwayPanel(int nowLocation, int panelLocation, int pTime)
        {
            string errormsg = "";
            try
            {
                if (this.pNFB == false)
                {
                    Log.Write("[DrawRailwayPanel] (Info) NFB is off");
                    return;
                }
                else if (this.CastBase == null)
                {
                    Log.Write("[DrawRailwayPanel] (Info) Base is null");
                    return;
                }

                //Dim model As Model.DataTable1 = RenewCastPanel(str);

                using (Graphics g = Graphics.FromImage(this.CastDisp))
                {
                    errormsg = "cBase";
                    if(this.CastBase != null)
                    {
                        g.DrawImage(this.CastBase, new Point(0, 0));
                    }



                    errormsg = "cStaff";
                    if(this.CastStaff != null) 
                    {
                        g.DrawImage(this.CastStaff, new Point(0, 890 - this.CastStaff.Height + 90 * this.mStationIndex));
                    }
                    errormsg = "cNext";
                    if(this.CastNext != null) 
                    {
                        g.DrawImage(this.CastNext, new Point(151, 0));
                    }

                    //g.FillRectangle(Brushes.Gray, new Rectangle(150, 0, 100, 25));

                    errormsg = "cRailwayTBL";
                    StringFormat sf = new StringFormat();
                    //sf.Alignment = StringAlignment.Far;
                    //sf.LineAlignment = StringAlignment.Center;
                    //g.DrawString(((int)Math.Abs((double)panelLocation / 1000)).ToString() + "K" + (Math.Abs(panelLocation) % 1000).ToString("000") + "m", 
                    //             new Font("@ＭＳ ゴシック", 11), Brushes.White, new RectangleF(150, 0, 100, 25), sf);

                    //g.FillRectangle(new SolidBrush(Color.FromArgb(64, 64, 64)), new Rectangle(500, 0, 100, 800));
                    //g.DrawImage(this.ImgRail, 500, 0, 100, 800);

                    //int nowLocation = model.Rows(0).BVELocation; //現在のBVE距離程
                    int lastLocation = nowLocation + 1050; //CAST一番上の距離程

                    errormsg = "cRailwayTBL 徐行・規制 該当区間";
                    if (this.ValidCautions != null)
                    {
                        foreach (CautionData cd in this.ValidCautions)
                        {
                            if(cd.Received != 0 && cd.LimpSpeed != 0)
                            {
                                int PosSY = (int)((lastLocation - cd.Location1) / 1.5); //start位置のY座標
                                int PosEY = (int)((lastLocation - cd.Location2) / 1.5); //end位置のY座標
                                if(cd.Type == 2)
                                {
                                    HL.GraOpe.DrawImageContinuous(g, this.ImgShimashima2, new Rectangle(539, PosEY, 56, PosSY - PosEY));
                                }
                                else
                                {
                                    g.FillRectangle(Brushes.Yellow, new Rectangle(539, PosEY, 56, PosSY - PosEY));
                                }
                            }
                        }
                    }

                    errormsg = "cRailwayTBL 線路";
                    HL.GraOpe.DrawImageContinuous(g, this.ImgRail, new Rectangle(564, 94, 8, 706));

                    errormsg = "cRailwayTBL2";
                    DataRow[] ROWS = RailwayTBL.Select("([start]>=" + (nowLocation - 150).ToString() +
                                                       " AND [start]<" + (nowLocation + 1050).ToString() +
                                                       ") OR " +
                                                       "([end]>=" + (nowLocation - 150).ToString() +
                                                       " AND [end]<" + (nowLocation + 1050).ToString() +
                                                       ") OR " +
                                                       "([start]<" + (nowLocation - 150).ToString() +
                                                       " AND [end]>=" + (nowLocation + 1050).ToString() +
                                                       ")");
                    if (ROWS.Count() > 0)
                    {
                        foreach (DataRow ROW in ROWS)
                        {
                            int startLoc = (int)(ROW["start"]);
                            errormsg = "start";
                            int endLoc = (int)(ROW["end"]);
                            errormsg = "end";
                            int PosSY = (int)((lastLocation - startLoc) / 1.5); //start位置のY座標
                            int PosEY = (int)((lastLocation - endLoc) / 1.5); //end位置のY座標
                            errormsg = "kind";
                            switch (ROW["kind"].ToString())
                            {
                                case "STA": //駅
                                    g.FillRectangle(Brushes.Black, 555, PosEY, 24, PosSY - PosEY);
                                    HL.GraOpe.DrawArcRectangle(g, new SolidBrush(Color.FromArgb(34, 81, 183)), new Rectangle(555, PosEY, 24, PosSY - PosEY), 4);
                                    //駅名を表示
                                    // - StringFormatを作成
                                    //StringFormat sf = new StringFormat();
                                    sf = new StringFormat();
                                    // - 縦書きにする
                                    sf.FormatFlags = StringFormatFlags.DirectionVertical;
                                    //HL.GraOpe.DrawString(ROW["etc"].ToString(), 
                                    //    g, new Font("@ＭＳ ゴシック", 12, FontStyle.Bold), new Point(557, PosEY), new Size(22, PosSY - PosEY), 
                                    //    Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle, sf);
                                    SizeF s = HL.GraOpe.MeasureString("", g, new Font("@ＭＳ ゴシック", 12, FontStyle.Bold));
                                    //StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic);
                                    //sf.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;
                                    SizeF sizeF = g.MeasureString(ROW["etc"].ToString(), new Font("@ＭＳ ゴシック", 12, FontStyle.Bold), 24, sf);
                                    g.DrawString(ROW["etc"].ToString(), new Font("@ＭＳ ゴシック", 12, FontStyle.Bold), Brushes.White, 557, PosEY + (((PosSY - PosEY) - sizeF.Height) / 2), sf);
                                    break;

                                case "SIG": //信号
                                    if (this.ImgSignal != null)
                                    {
                                        g.DrawImage(this.ImgSignal, new Point(540, PosSY - 20));
                                    }
                                    //g.DrawString(ROW["etc"].ToString(), new Font("ＭＳ ゴシック", 12, FontStyle.Bold), Brushes.White, 539, PosSY);
                                    //HL.GraOpe.DrawString(ROW["etc"].ToString(), g, new Font("ＭＳ ゴシック", 6), new Rectangle(539, PosSY, 14, 10), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                                    switch (ROW["etc"].ToString())
                                    {
                                        case "":
                                            break;
                                        case "出発":
                                            g.DrawImage(this.ImgNumberSignal[10], new Point(539, PosSY));
                                            break;
                                        case "場内":
                                            g.DrawImage(this.ImgNumberSignal[11], new Point(539, PosSY));
                                            break;
                                        case "中継":
                                            g.DrawImage(this.ImgNumberSignal[12], new Point(539, PosSY));
                                            break;
                                        case "遠方":
                                            g.DrawImage(this.ImgNumberSignal[13], new Point(539, PosSY));
                                            break;
                                        case "入換":
                                            g.DrawImage(this.ImgNumberSignal[14], new Point(539, PosSY));
                                            break;
                                        default:
                                            g.DrawImage(this.ImgNumberSignal[HL.StrOpe.ToInt(ROW["etc"].ToString())], new Point(542, PosSY));
                                            break;
                                    }
                                    break;

                                case "TUN": //トンネル
                                            //g.DrawArc(New Pen(Color.White, 2), 20, PosEY - 10, 60, 20, 0, 180)
                                            //g.DrawArc(New Pen(Color.White, 2), 20, PosSY, 60, 20, 180, 180)
                                    if (this.ImgTunnelEnd != null)
                                    {
                                        g.DrawImage(this.ImgTunnelEnd, new Point(552, PosEY - 10));
                                    }
                                    if (this.ImgTunnelStart != null)
                                    {
                                        g.DrawImage(this.ImgTunnelStart, new Point(552, PosSY));
                                    }
                                    g.FillRectangle(new SolidBrush(Color.FromArgb(128, 255, 255, 255)), 552, PosEY, 30, PosSY - PosEY);
                                    break;

                                case "BRI": //橋梁
                                    g.DrawLine(new Pen(Color.White, 2), new Point(551, PosEY - 5), new Point(556, PosEY));
                                    g.DrawLine(new Pen(Color.White, 2), new Point(556, PosEY), new Point(556, PosSY));
                                    g.DrawLine(new Pen(Color.White, 2), new Point(556, PosSY), new Point(551, PosSY + 5));
                                    g.DrawLine(new Pen(Color.White, 2), new Point(583, PosEY - 5), new Point(578, PosEY));
                                    g.DrawLine(new Pen(Color.White, 2), new Point(578, PosEY), new Point(578, PosSY));
                                    g.DrawLine(new Pen(Color.White, 2), new Point(578, PosSY), new Point(583, PosSY + 5));
                                    break;

                                case "CRO": //踏切
                                    if (this.ImgCross != null)
                                    {
                                        g.DrawImage(this.ImgCross, new Point(551, PosSY));
                                    }
                                    break;
                            }
                        }
                    }

                    errormsg = "circle";
                    // 自車位置
                    if (this.ImgMyTrain != null)
                    {
                        g.DrawImage(this.ImgMyTrain, new Point(554, 686));
                    }

                    errormsg = "restTime";
                    // ドアが空いているとき
                    if (this.pNearStation == 2)
                    {
                        if (this.mStationIndex == this.mBveStaff.StationList.Count - 1 && this.pNearStation >= 2)
                        {

                        }
                        else if (this.mStationIndex < this.mBveStaff.StationList.Count)
                        {
                            //Staff.StaffData.clsHensuu oneStation = this.mBveStaff.StaffPack.StationList[this.mStationIndex];
                            HL.Bve.Station.OneStation oneStation = this.mBveStaff.StationList[this.mStationIndex];
                            int deltaT = ((oneStation.DefaultTimeDate.Hour + 24) * 3600 + oneStation.DefaultTimeDate.Minute * 60 + oneStation.DefaultTimeDate.Second - pTime) % (3600 * 24);
                            if ((oneStation.DefaultTimeDate.Hour * 3600 + oneStation.DefaultTimeDate.Minute * 60 + oneStation.DefaultTimeDate.Second - pTime) > 0)
                            {
                                if (deltaT > 60)
                                {
                                    HL.GraOpe.DrawString("発車時刻まであと" + ((int)(deltaT / 60)).ToString("00") + "分" + (deltaT % 60).ToString("00") + "秒",
                                        g, new Font("メイリオ", 9), new Rectangle(181, 600, 300, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                else
                                {
                                    HL.GraOpe.DrawString("発車時刻まであと" + deltaT.ToString("00") + "秒",
                                        g, new Font("メイリオ", 9), new Rectangle(181, 600, 300, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                            }
                            else
                            {
                                int overSec = pTime - (oneStation.DefaultTimeDate.Hour * 3600 + oneStation.DefaultTimeDate.Minute * 60 + oneStation.DefaultTimeDate.Second);
                                if (overSec < 15)
                                {
                                    HL.GraOpe.DrawString("定時",
                                        g, new Font("メイリオ", 9), new Rectangle(181, 600, 300, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                else if (overSec < 60)
                                {
                                    HL.GraOpe.DrawString((overSec - (overSec % 15)).ToString("00") + "秒延",
                                        g, new Font("メイリオ", 9), new Rectangle(181, 600, 300, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                else
                                {
                                    if (overSec % 60 < 15)
                                    {
                                        HL.GraOpe.DrawString(((int)(overSec / 60)).ToString() + "分延",
                                            g, new Font("メイリオ", 9), new Rectangle(181, 600, 300, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                    }
                                    else
                                    {
                                        HL.GraOpe.DrawString(((int)(overSec / 60)).ToString() + "分" + ((overSec - (overSec % 15)) % 60).ToString("00") + "秒延",
                                            g, new Font("メイリオ", 9), new Rectangle(181, 600, 300, 20), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                    }
                                }
                            }
                        }
                    }

                    errormsg = "nearStation";
                    // 停車駅接近
                    if (this.pNearStation == 1)
                    {
                        if(pTime % 2 == 0)
                        {
                            g.DrawImage(this.ImgStopStationCaution, new Point(162, 507));
                        }
                    }

                    // 時計
                    errormsg = "clock";
                    g.FillRectangle(new SolidBrush(Color.FromArgb(28, 28, 30)), new Rectangle(150, 738, 450, 62));
                    g.DrawImage(this.ImgNumber35[(int)((int)(pTime / 3600) % 24 / 10)], new Point(188, 756));
                    g.DrawImage(this.ImgNumber35[(int)((int)(pTime / 3600) % 24) % 10], new Point(210, 756));
                    g.DrawString(":", new Font("メイリオ", 26), Brushes.White, new PointF(223, 743));
                    g.DrawImage(this.ImgNumber35[(int)((int)((pTime % 3600) / 60) / 10)], new Point(241, 756));
                    g.DrawImage(this.ImgNumber35[(int)((int)((pTime % 3600) / 60)) % 10], new Point(263, 756));
                    g.DrawImage(this.ImgNumber20[(int)((pTime % 60) / 10)], new Point(296, 765));
                    g.DrawImage(this.ImgNumber20[(int)(pTime % 10)], new Point(308, 765));

                    // 戻る・停車
                    errormsg = "backstop";
                    // 始発駅では表示しない
                    if(this.mStationIndex != 0)
                    {
                        g.DrawImage(this.ImgBackButton, new Point(366, 751));
                    }
                    if(this.mStationIndex == 0 && this.pNearStation < 2)
                    {
                        // 始発駅 設置完了
                        HL.GraOpe.FillArcRectangle(g, Brushes.Red, new Rectangle(459, 751, 116, 35), 4);
                        HL.GraOpe.DrawString("設置完了", g, new Font("メイリオ", 8), new Rectangle(459, 751, 116, 35), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                    }
                    else if (this.mStationIndex == this.mBveStaff.StationList.Count - 1 && this.pNearStation >= 2)
                    {
                        // 終着駅 乗務完了
                        HL.GraOpe.FillArcRectangle(g, Brushes.Red, new Rectangle(459, 751, 116, 35), 4);
                        HL.GraOpe.DrawString("乗務終了", g, new Font("メイリオ", 8), new Rectangle(459, 751, 116, 35), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                    }
                    else
                    {
                        g.DrawImage(this.ImgStopButton, new Point(459, 751));
                    }

                    this.pOldNearStation = this.pNearStation;

                    // 通告・行先
                    int BaseTop = 93; // 93 or 273
                    if (this.ValidCautions != null && this.ValidCautions.Count > 0)
                    {
                        errormsg = "bound A";
                        BaseTop = 273;

                        // 通告
                        g.FillRectangle(new SolidBrush(Color.FromArgb(28, 28, 30)), new RectangleF(0, 93, 600, 180));
                        if(this.DispDetailCaution != null)
                        {
                            // 詳細表示
                            if(this.DispDetailCaution.Type < 0)
                            {
                                // 時刻表表示
                                // - 状態
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(215, 62, 73)), new Rectangle(70, 116, 33, 15), 3);
                                HL.GraOpe.DrawString("未受領", g, new Font("メイリオ", 7), new Rectangle(70, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                                // - タイトル
                                HL.GraOpe.DrawString(this.DispDetailCaution.Title,
                                                     g, new Font("メイリオ", 9), new Rectangle(100, 118, 400, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                g.FillRectangle(new SolidBrush(Color.FromArgb(28, 28, 30)), new RectangleF(0, 280, 600, 458));
                                if(this.DispDetailCaution.Received == 0)
                                {
                                    HL.GraOpe.DrawString("下までスクロールして確認してください。",
                                                         g, new Font("メイリオ", 9), new Rectangle(100, 700, 400, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                                }
                                else
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Gray, new Rectangle(260, 700, 80, 20), 4);
                                    HL.GraOpe.DrawString("確　認",
                                                         g, new Font("メイリオ", 9), new Rectangle(260, 700, 80, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterTop);

                                }
                            }
                            else if (this.DispDetailCaution.Type == 1 && this.DispDetailCaution.LimpSpeed == 0)
                            {
                                // 計画徐行なし
                                // - 状態
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(215, 62, 73)), new Rectangle(70, 116, 33, 15), 3);
                                HL.GraOpe.DrawString("未受領", g, new Font("メイリオ", 7), new Rectangle(70, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                                // - タイトル
                                HL.GraOpe.DrawString(this.DispDetailCaution.Title,
                                                     g, new Font("メイリオ", 9), new Rectangle(100, 118, 400, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                                // - 閉じる
                                g.FillRectangle(Brushes.Gray, new Rectangle(240, 233, 120, 20));
                                HL.GraOpe.DrawString("確　認",
                                                     g, new Font("メイリオ", 9), new Rectangle(240, 233, 120, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterTop);

                            }
                            else
                            {
                                // - 速度
                                if (this.DispDetailCaution.LimpSpeed > 0)
                                {
                                    g.FillEllipse(Brushes.Yellow, new Rectangle(22, 116, 32, 32));
                                    HL.GraOpe.DrawString(this.DispDetailCaution.LimpSpeed.ToString(),
                                        g, new Font("ＭＳ ゴシック", 12), new Rectangle(22, 116, 32, 32), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                                }
                                // - 状態
                                if (this.DispDetailCaution.Received == 0)
                                {
                                    HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(215, 62, 73)), new Rectangle(70, 116, 33, 15), 3);
                                    HL.GraOpe.DrawString("未受領", g, new Font("メイリオ", 7), new Rectangle(70, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else if (this.DispDetailCaution.Received == 1)
                                {
                                    HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(181, 231, 29)), new Rectangle(70, 116, 43, 15), 3);
                                    HL.GraOpe.DrawString("復唱待ち", g, new Font("メイリオ", 7), new Rectangle(70, 118, 43, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else if (this.DispDetailCaution.Received == 2)
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Gray, new Rectangle(70, 116, 33, 15), 3);
                                    HL.GraOpe.DrawString("受領済", g, new Font("メイリオ", 7), new Rectangle(70, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else if (this.DispDetailCaution.Received == 3)
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Red, new Rectangle(70, 116, 63, 15), 3);
                                    HL.GraOpe.DrawString("発車前再確認", g, new Font("メイリオ", 7), new Rectangle(70, 118, 63, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Gray, new Rectangle(70, 116, 33, 15), 3);
                                    HL.GraOpe.DrawString("受領済", g, new Font("メイリオ", 7), new Rectangle(70, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                // - 通告番号
                                int w1 = HL.GraOpe.DrawString("通告第" + this.DispDetailCaution.Number.ToString() + "号",
                                                     g, new Font("メイリオ", 9), new Rectangle(70, 135, 10, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop).Width + 10;
                                if (this.DispDetailCaution.Checked != null && this.DispDetailCaution.Checked.Length != 0)
                                {
                                    if (this.DispDetailCaution.Checked[0] == null)
                                    {
                                        HL.GraOpe.DrawString("✕",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w1, 135, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);

                                    }
                                    else if (this.DispDetailCaution.Checked[0].Checked == true)
                                    {
                                        HL.GraOpe.DrawString("●",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w1, 135, 91, 8), Brushes.Blue, GraOpe.ENUM_TextAlign.LeftTop);
                                        HL.GraOpe.DrawString("✔",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w1, 135, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);

                                    }
                                    else
                                    {
                                        HL.GraOpe.DrawString("◯",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w1, 135, 91, 8), Brushes.Gray, GraOpe.ENUM_TextAlign.LeftTop);
                                    }
                                }

                                // - タイトル
                                int w2 = HL.GraOpe.DrawString(this.DispDetailCaution.Title,
                                                     g, new Font("メイリオ", 9), new Rectangle(70, 153, 10, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop).Width + 10;
                                if (this.DispDetailCaution.Checked != null && this.DispDetailCaution.Checked.Length > 1)
                                {
                                    if (this.DispDetailCaution.Checked[1] == null)
                                    {
                                        HL.GraOpe.DrawString("✕",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w1, 153, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);

                                    }
                                    else if (this.DispDetailCaution.Checked[1].Checked == true)
                                    {
                                        HL.GraOpe.DrawString("●",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w2, 153, 91, 8), Brushes.Blue, GraOpe.ENUM_TextAlign.LeftTop);
                                        HL.GraOpe.DrawString("✔",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w2, 153, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);

                                    }
                                    else
                                    {
                                        HL.GraOpe.DrawString("◯",
                                                             g, new Font("メイリオ", 9), new Rectangle(70 + w2, 153, 91, 8), Brushes.Gray, GraOpe.ENUM_TextAlign.LeftTop);
                                    }

                                }

                                // - 内容
                                for (int i = 0; i < 3; i++)
                                {
                                    if (this.DispDetailCaution.Checked != null && i < this.DispDetailCaution.Checked.Length - 2)
                                    {
                                        if (this.DispDetailCaution.Checked[i + 2] == null)
                                        {
                                            HL.GraOpe.DrawString("✕",
                                                                 g, new Font("メイリオ", 9), new Rectangle(70, 171 + i * 18, 91, 8), Brushes.Gray, GraOpe.ENUM_TextAlign.LeftTop);
                                        }
                                        else
                                        {
                                            int w3 = HL.GraOpe.DrawString(this.DispDetailCaution.Checked[i + 2].Content,
                                                                 g, new Font("メイリオ", 9), new Rectangle(70, 171 + i * 18, 10, 8),
                                                                 Brushes.White, GraOpe.ENUM_TextAlign.LeftTop).Width + 10;
                                            if (this.DispDetailCaution.Checked[i + 2].Checked == true)
                                            {
                                                HL.GraOpe.DrawString("●",
                                                                     g, new Font("メイリオ", 9), new Rectangle(70 + w3, 171 + i * 18, 91, 8), Brushes.Blue, GraOpe.ENUM_TextAlign.LeftTop);
                                                HL.GraOpe.DrawString("✔",
                                                                     g, new Font("メイリオ", 9), new Rectangle(70 + w3, 171 + i * 18, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);

                                            }
                                            else
                                            {
                                                HL.GraOpe.DrawString("◯",
                                                                     g, new Font("メイリオ", 9), new Rectangle(70 + w3, 171 + i * 18, 91, 8), Brushes.Gray, GraOpe.ENUM_TextAlign.LeftTop);
                                            }
                                        }
                                    }
                                }
                                // - 閉じる
                                bool allChecked = true;
                                for (int i = 0; i < 5; i++)
                                {
                                    if (this.DispDetailCaution.Checked == null)
                                    {
                                        continue;
                                    }
                                    else if (i >= this.DispDetailCaution.Checked.Length)
                                    {
                                        break;
                                    }
                                    else if (this.DispDetailCaution.Checked[i].Checked == false)
                                    {
                                        allChecked = false;
                                        break;
                                    }
                                }
                                string tx;
                                if (this.DispDetailCaution.Received == 0)
                                {
                                    tx = "受領";
                                }
                                else if (this.DispDetailCaution.Received == 1)
                                {
                                    tx = "復唱完了";
                                }
                                else if (this.DispDetailCaution.Received == 3)
                                {
                                    tx = "発車直前に確認";
                                }
                                else
                                {
                                    tx = "閉じる";
                                }
                                if (allChecked == true)
                                {
                                    g.FillRectangle(Brushes.Gray, new Rectangle(230, 233, 140, 20));
                                    HL.GraOpe.DrawString(tx,
                                                         g, new Font("メイリオ", 9), new Rectangle(230, 233, 140, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterTop);
                                }
                                else
                                {
                                    g.FillRectangle(Brushes.DarkGray, new Rectangle(230, 233, 140, 20));
                                    HL.GraOpe.DrawString(tx,
                                                         g, new Font("メイリオ", 9), new Rectangle(230, 233, 140, 20), Brushes.LightGray, GraOpe.ENUM_TextAlign.CenterTop);
                                }
                            }
                        }
                        else
                        {
                            // 一覧表示
                            for (int i = 0; i < this.ValidCautions.Count; i++)
                            {
                                int BaseX = 14 + i * 120;
                                if (this.ValidCautions[i].Type == 2 && this.ValidCautions[i].Received != 0)
                                {
                                    // 背景黄色点滅
                                    if (pTime % 2 == 0)
                                    {
                                        g.DrawImage(this.ImgCautionYellow, new Point(BaseX - 8, 99));
                                    }
                                }
                                HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(44, 44, 48)), new Rectangle(BaseX, 107, 106, 156), 6);

                                if (this.ValidCautions[i].Received == 0 || (this.ValidCautions[i].Received == 1 && this.ValidCautions[i].Type < 0))
                                {
                                    HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(215, 62, 73)), new Rectangle(BaseX + 8, 116, 33, 15), 3);
                                    HL.GraOpe.DrawString("未受領", g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else if (this.ValidCautions[i].Received == 1)
                                {
                                    HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(181, 231, 29)), new Rectangle(BaseX + 8, 116, 43, 15), 3);
                                    HL.GraOpe.DrawString("復唱待ち", g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 118, 43, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else if (this.ValidCautions[i].Received == 2)
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Gray, new Rectangle(BaseX + 8, 116, 33, 15), 3);
                                    HL.GraOpe.DrawString("受領済", g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else if (this.ValidCautions[i].Received == 3)
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Red, new Rectangle(BaseX + 8, 116, 63, 15), 3);
                                    HL.GraOpe.DrawString("発車前再確認", g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 118, 63, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                else
                                {
                                    HL.GraOpe.FillArcRectangle(g, Brushes.Gray, new Rectangle(BaseX + 8, 116, 33, 15), 3);
                                    HL.GraOpe.DrawString("受領済", g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 118, 33, 15), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                }
                                if (this.ValidCautions[i].Type < 0 || this.ValidCautions[i].Type == 1 && this.ValidCautions[i].LimpSpeed == 0)
                                {
                                    HL.GraOpe.DrawString(this.ValidCautions[i].Title,
                                                         g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 135, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                    HL.GraOpe.DrawString(this.ValidCautions[i].Caution.Replace("\\n", "\n"),
                                                         g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 147, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                else
                                {
                                    HL.GraOpe.DrawString("通告第" + this.ValidCautions[i].Number.ToString() + "号",
                                                         g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 135, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                    HL.GraOpe.DrawString(this.ValidCautions[i].Title,
                                                         g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 147, 91, 8), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                    HL.GraOpe.DrawString(this.ValidCautions[i].Caution.Replace("\\n", "\n"),
                                                         g, new Font("メイリオ", 7), new Rectangle(BaseX + 8, 159, 91, 116), Brushes.White, GraOpe.ENUM_TextAlign.LeftTop);
                                }
                                if (this.ValidCautions[i].LimpSpeed > 0)
                                {
                                    g.FillEllipse(Brushes.Yellow, new RectangleF(BaseX + 8, 233, 24, 24));
                                    HL.GraOpe.DrawString(this.ValidCautions[i].LimpSpeed.ToString(),
                                                         g, new Font(this.mFontName, 8), new Rectangle(BaseX + 8, 235, 24, 24), new SolidBrush(Color.FromArgb(44, 44, 48)), GraOpe.ENUM_TextAlign.CenterMiddle);
                                }
                            }
                        }
                        // 徐行区間表示
                        if(this.DispDetailCaution == null || this.DispDetailCaution.Type >= 0)
                        {
                            int CheckF = 0; // 0:なし 1:計画徐行確認 2:発車前再確認
                            bool InArea = false; // T:徐行区間内
                            for (int i = 0; i < this.ValidCautions.Count; i++)
                            {
                                if (this.ValidCautions[i].Type < 0 || this.ValidCautions[i].Type == 1 && this.ValidCautions[i].Received < 2)
                                {
                                    CheckF = 1;
                                    break;
                                }
                                else if (this.ValidCautions[i].Type == 1 && this.ValidCautions[i].Received == 3)
                                {
                                    CheckF = 2;
                                }
                                if (this.ValidCautions[i].Type != 0 && this.ValidCautions[i].Received != 0 && this.ValidCautions[i].LimpSpeed > 0 &&
                                    this.ValidCautions[i].Location1 <= nowLocation && nowLocation <= this.ValidCautions[i].Location2)
                                {
                                    InArea = true;
                                }
                            }
                            if(CheckF == 1)
                            {
                                g.FillEllipse(Brushes.Red, new Rectangle(236, 700, 24, 24));
                                g.FillRectangle(Brushes.Red, new Rectangle(248, 700, 196, 24));
                                g.FillEllipse(Brushes.Red, new Rectangle(432, 700, 24, 24));
                                HL.GraOpe.DrawString("変更・注意事項と計画徐行を確認してください", g, new Font("メイリオ", 7), new Rectangle(236, 700, 220, 24),
                                                     Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            }
                            else if(CheckF == 2 && InArea == true)
                            {
                                g.FillEllipse(Brushes.Red, new Rectangle(210, 700, 24, 24));
                                g.FillRectangle(Brushes.Red, new Rectangle(222, 700, 156, 24));
                                g.FillEllipse(Brushes.Red, new Rectangle(366, 700, 24, 24));
                                HL.GraOpe.DrawString("発車前再確認をしてください", g, new Font("メイリオ", 7), new Rectangle(210, 700, 180, 24),
                                                     Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);

                                g.FillEllipse(Brushes.Yellow, new Rectangle(410, 700, 24, 24));
                                g.FillRectangle(Brushes.Yellow, new Rectangle(422, 700, 56, 24));
                                g.FillEllipse(Brushes.Yellow, new Rectangle(466, 700, 24, 24));
                                HL.GraOpe.DrawString("徐行区間内", g, new Font("メイリオ", 7), new Rectangle(410, 700, 80, 24),
                                                     new SolidBrush(Color.FromArgb(44, 44, 48)), GraOpe.ENUM_TextAlign.CenterMiddle);
                            }
                            else if (CheckF == 2 && InArea == false)
                            {
                                g.FillEllipse(Brushes.Red, new Rectangle(256, 700, 24, 24));
                                g.FillRectangle(Brushes.Red, new Rectangle(268, 700, 156, 24));
                                g.FillEllipse(Brushes.Red, new Rectangle(412, 700, 24, 24));
                                HL.GraOpe.DrawString("発車前再確認をしてください", g, new Font("メイリオ", 7), new Rectangle(256, 700, 180, 24),
                                                     Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            }
                            else if (CheckF == 0 && InArea == true)
                            {
                                g.FillEllipse(Brushes.Yellow, new Rectangle(306, 700, 24, 24));
                                g.FillRectangle(Brushes.Yellow, new Rectangle(318, 700, 56, 24));
                                g.FillEllipse(Brushes.Yellow, new Rectangle(362, 700, 24, 24));
                                HL.GraOpe.DrawString("徐行区間内", g, new Font("メイリオ", 7), new Rectangle(306, 700, 80, 24),
                                                     new SolidBrush(Color.FromArgb(44, 44, 48)), GraOpe.ENUM_TextAlign.CenterMiddle);
                            }
                        }
                    }
                    else
                    {
                        errormsg = "bound N";
                        BaseTop = 143;
                        g.FillRectangle(new SolidBrush(Color.FromArgb(28, 28, 30)), new RectangleF(0, 93, 600, 50));
                        HL.GraOpe.DrawString("現在受信している通告はありません",
                                             g, new Font("メイリオ", 9), new Rectangle(200, 113, 200, 20), Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                    }

                    g.FillRectangle(Brushes.Black, new Rectangle(0, BaseTop, 600, 37));
                    HL.GraOpe.FillArcRectangle(g, new SolidBrush(Color.FromArgb(61,62,63)), new Rectangle(15, BaseTop + 8, 21, 21), 3);
                    g.DrawImage(this.ImgTimetable, new Point(14, BaseTop + 7));

                    g.PageUnit = GraphicsUnit.Pixel;
                    SizeF sizef = g.MeasureString(this.mBveStaff.StaffHeader.Ikisaki, new Font("メイリオ", 13));
                    g.DrawString(this.mBveStaff.StaffHeader.Ikisaki, new Font("メイリオ", 13), Brushes.White, new PointF(50, BaseTop + 8));
                    g.DrawString("行き", new Font("メイリオ", 9), Brushes.White, new PointF(50 + sizef.Width, BaseTop + 14));

                    // 現在位置
                    errormsg = "location";
#if DEBUG
                    HL.GraOpe.DrawString(((int)(nowLocation / 1000)).ToString("000") + " k " + (nowLocation % 1000).ToString("000") + " m / " +
                                         ((int)(panelLocation / 1000)).ToString("000") + " k " + (panelLocation % 1000).ToString("000") + " m",
                                         g, new Font("メイリオ", 9), new Rectangle(490, BaseTop + 10,100, 20), Brushes.White, GraOpe.ENUM_TextAlign.RightTop);
#else
                    HL.GraOpe.DrawString(((int)(panelLocation / 1000)).ToString("000") + " k " + (panelLocation % 1000).ToString("000") + " m", 
                                         g, new Font("メイリオ", 9), new Rectangle(490, BaseTop + 10, 100, 20), Brushes.White, GraOpe.ENUM_TextAlign.RightTop);
#endif

                    // ヘッダー
                    errormsg = "cHeader";
                    g.DrawImage(this.CastHeader, new Point(0, 0));

                    // Graphicsオブジェクトのリソースを解放する
                    g.Dispose();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawRailwayPanel]" + errormsg, false);
            }
        }
#endregion

        #region Elapse
        public void Elapse(AtsEngine.MyVehicleState vehicleState)
        {
            if (this.pNearStation == 1)
            {
                if (this.pSoundNearStation == AtsDefine.AtsSoundControlInstruction.Play)
                {
                    this.pSoundNearStation = AtsDefine.AtsSoundControlInstruction.Continue;
                }
                else if (this.pSoundNearStation == AtsDefine.AtsSoundControlInstruction.Continue)
                {

                }
                else
                {
                    this.pSoundNearStation = AtsDefine.AtsSoundControlInstruction.Play;
                }
            }
            else
            {
                if (this.pSoundNearStation != AtsDefine.AtsSoundControlInstruction.Stop)
                {
                    this.pSoundNearStation = AtsDefine.AtsSoundControlInstruction.Stop;
                }
            }
            if (this.pNearStation == 3 && vehicleState.NewState.Speed == 0)
            {
                this.pNearStation = 4; // 停車扱い
            }
            else if (this.pNearStation == 4 && vehicleState.NewState.Speed > 0)
            {
                this.pNearStation = 0;
            }

            // mStationIndexが停車駅で、前フレームが2分超前で現フレームが2分以内ならTrue
            if (this.mBveStaff != null && this.mBveStaff.StationList != null && this.mBveStaff.StationList.Count > this.mStationIndex && 
                this.mBveStaff.StationList[mStationIndex].StaffHensuu.ChakuFlag != "P" &&
                vehicleState.NewState.Speed == 0)
            {
                int t = this.mBveStaff.StationList[mStationIndex].DefaultTimeDate.Hour * 3600000 +
                    this.mBveStaff.StationList[mStationIndex].DefaultTimeDate.Minute * 60000 +
                    this.mBveStaff.StationList[mStationIndex].DefaultTimeDate.Second * 1000;
                if (vehicleState.NewState.Time - vehicleState.GetDeltaTime() < t - 120000 && t - 120000 <= vehicleState.NewState.Time)
                {
#if DEBUG
                    Log.Write("CAST PLAY");
#endif
                    this.pSoundFirstDeparture = AtsDefine.AtsSoundControlInstruction.Play;
                    if (soundPlayerDeparture != null)
                    {
                        soundPlayerDeparture.Play();
                    }
                }
                else if(t - 60000 <= vehicleState.NewState.Time)
                {
                    this.pSoundFirstDeparture = AtsDefine.AtsSoundControlInstruction.Stop;
                }
                else
                {
                    this.pSoundFirstDeparture = AtsDefine.AtsSoundControlInstruction.Continue;
                }

            }
            else
            {
                this.pSoundFirstDeparture = AtsDefine.AtsSoundControlInstruction.Stop;
            }

            // 表示距離程を超えたか
            foreach(CautionData caution in this.Cautions)
            {
                if(caution.Disped == false && caution.DispStart <= vehicleState.Location)
                {
                    this.ValidCautions.Add(caution);
                    caution.Disped = true;
                    Log.Write("[ValidCautions.Add] E1 No=" + caution.Number.ToString() + " DS=" + caution.DispStart.ToString() + " loc=" + vehicleState.Location.ToString());
                }
            }
        }
        #endregion

        #region Door
        /// <summary>
        /// 
        /// </summary>
        /// <param name="open"></param>
        /// <param name="back">戻るボタンによる呼び出し</param>
        public void SetDoorOpen(bool open, bool back = false)
        {
            if (open == true)
            {
                this.pStopFlag = true;
                this.pNearStation = 2;
            }
            else
            {
                this.pStopFlag = false;
                this.pNearStation = 0;
            }
            ResetNearStation();

            if (this.ValidCautions != null && back == false)
            {
                // 次の停車駅
                int nextStop = -1;
                for(int i = this.StationIndex; i < this.mBveStaff.StationList.Count; i++)
                {
                    if(i == this.StationIndex && this.pStopFlag == true)
                    {
                        continue;
                    }
                    if (this.mBveStaff.StationList[i].ArrivalTime.ToLower() != "p")
                    {
                        nextStop = i;
                        break;
                    }
                }

                // 次の停車駅までに範囲に入るなら受領済みにする
                foreach (CautionData cd in this.ValidCautions)
                {
                    if(nextStop < 0 || nextStop >= this.mBveStaff.StationList.Count)
                    {
                        break;
                    }
                    if (this.mBveStaff.StationList[this.StationIndex].Location <= cd.Location1 && cd.Location1 <= this.mBveStaff.StationList[nextStop].Location && cd.Received <= 2)
                    {
                        cd.Received = 4;
                        Log.Write("[Caution.Received] 4 No=" + cd.Number.ToString());
                        CheckLimpSpeedList();
                    }
                }
            }

            DrawNext();
        }
        #endregion

        #region SetBeaconData
        /// <summary>
        /// 地上子通過
        /// </summary>
        /// <param name="vehicleState"></param>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsEngine.MyVehicleState vehicleState, AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
                // 選抜された地上子のみ来ている
                switch (beaconData.Type)
                {
                    case 8:
                        this.pStopFlag = true;
                        break;
                    case 9:
                        break;
                    case 34:
                        string[] alpha = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                        if (0 <= beaconData.Optional && beaconData.Optional <= 999976)
                        {
                            this.mBveStaff.StaffHeader.TrainNumber = ((int)(beaconData.Optional / 100)).ToString();
                            if (beaconData.Optional % 100 >= 50)
                            {
                                this.mBveStaff.StaffHeader.TrainNumber = "回" + this.mBveStaff.StaffHeader.TrainNumber;
                            }
                            if (beaconData.Optional % 50 != 0 && beaconData.Optional % 50 <= 26)
                            {
                                this.mBveStaff.StaffHeader.TrainNumber += alpha[beaconData.Optional % 50 - 1];
                            }
                            DrawHeader();
                        }
                        else
                        {
                            //this.mBveStaff.StaffPack.TrainNumber = "";
                        }
                        break;
                    case 85000:
                        // 初期化 (-1:完全初期化(停車場リストと通告) 0:部分初期化)
                        if(beaconData.Optional == -1)
                        {
                            Init(this.pCars, null);
                        }
                        else
                        {
                            this.RailwayTBL.Clear();
                        }
                        break;
                    case 85001:
                        // 距離設定
                        this.pTmpLocation = beaconData.Optional;
                        break;
                    case 85010:
                        // 停車場接近
                        this.pNearStation = 1;
                        break;
                    case 85011:
                        // 次停車場(停車目標)
                        this.pStopTarget2 = this.pStopTarget1;
                        this.pStopTarget1 = beaconData.Optional;
                        if(this.StopTargetImage == null)
                        {
                            this.StopTargetImage = new Dictionary<int, Bitmap>();
                        }
                        if (this.StopTargetImage.ContainsKey(this.pStopTarget1) == false)
                        {
                            if (System.IO.File.Exists(ModuleDirectoryPath + "\\castimg\\stop" + this.pStopTarget1.ToString("00") + ".png") == true)
                            {
                                System.Drawing.Image img = System.Drawing.Image.FromFile(ModuleDirectoryPath + "\\castimg\\stop" + this.pStopTarget1.ToString("00") + ".png");
                                Bitmap stp = new Bitmap(img, new Size(200, 200));
                                this.StopTargetImage.Add(this.pStopTarget1, stp);
                            }
                            else
                            {
                                this.StopTargetImage.Add(this.pStopTarget1, null);
                            }
                        }
                        break;
                    case 85012:
                        // 次停車場(駅番号)
                        this.pStopIndex2 = this.pStopIndex1;
                        this.pStopIndex1 = beaconData.Optional;
                        break;
                    case 85013:
                        // 停車場接近
                        if (vehicleState.NewState.Speed >= 0)
                        {
                            this.pNearStation = 3;
                        }
                        break;
                    case 85100:
                        // 駅
                        if (this.Stations != null && this.Stations.ContainsKey(beaconData.Optional) == true)
                        {
                            SetTBL("STA", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 150, this.Stations[beaconData.Optional]);
                        }
                        else
                        {
                            SetTBL("STA", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 150, "駅");
                        }
                        this.pTmpLocation = 0;
                        break;
                    case 85110:
                        // 信号
                        switch (beaconData.Optional)
                        {
                            case 10:
                                SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, "出発");
                                break;
                            case 11:
                                SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, "場内");
                                break;
                            case 12:
                                SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, "中継");
                                break;
                            case 13:
                                SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, "遠方");
                                break;
                            case 14:
                                SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, "入換");
                                break;
                            default:
                                if (1 <= beaconData.Optional && beaconData.Optional <= 9)
                                {
                                    SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, beaconData.Optional.ToString());
                                }
                                else
                                {
                                    SetTBL("SIG", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), 0, "");
                                }
                                break;
                        }
                        this.pTmpLocation = 0;
                        break;
                    case 85120:
                        // トンネル
                        SetTBL("TUN", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), beaconData.Optional, "");
                        this.pTmpLocation = 0;
                        break;
                    case 85130:
                        // 橋
                        SetTBL("BRI", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), beaconData.Optional, "");
                        this.pTmpLocation = 0;
                        break;
                    case 85140:
                        // 踏切
                        SetTBL("CRO", ((this.pTmpLocation != 0) ? this.pTmpLocation : (int)vehicleState.Location), beaconData.Optional, "");
                        this.pTmpLocation = 0;
                        break;
                    case 85150:
                        // 通告
                        // ※通告は、記載されているマップファイルと同階層にある特定フォルダを探して解析という処理にしているため、
                        //   ここで呼ばれても処理できない
                        break;
                    default:
                        break;
                }

                BeaconSchema beaconSchema = new BeaconSchema();
                beaconSchema.Type = beaconData.Type;
                beaconSchema.Distance = beaconData.Distance;
                beaconSchema.Signal = beaconData.Signal;
                beaconSchema.Optional = beaconData.Optional;
                if(lBeacon == null)
                {
                    lBeacon = new List<BeaconSchema>();
                }
                lBeacon.Add(beaconSchema);
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.SetBeaconData] Type=" + beaconData.Type.ToString() + " : " + ex.Message);
            }
        }
        #endregion

        public void SetSoundPlayer(SoundPlayer sp1, SoundPlayer sp2)
        {
            soundPlayerDeparture = sp1;
            soundPlayerNearStation = sp2;
        }
        #region 戻る・進む
        /// <summary>
        /// 戻る・進むボタン(Form用)
        /// </summary>
        /// <param name="isNext">T:次へ</param>
        public void PushNextButton(bool isNext)
        {
            //bool open;
            if (isNext == true)
            {
                // 次へ

                // 発車前再確認チェック
                // - 次の停車駅の距離程
                double nextStopLocation = this.mBveStaff.StationList[mStationIndex].Location;
                for (int j = 0; j < this.mBveStaff.StationList.Count - 1; j++)
                {
                    if (this.mBveStaff.StationList[j].Location >= 0 && 
                        this.mBveStaff.StationList[mStationIndex].Location < this.mBveStaff.StationList[j].Location && 
                        this.mBveStaff.StationList[j].StaffHensuu.ChakuFlag != "P")
                    {
                        nextStopLocation = this.mBveStaff.StationList[j].Location;
                        break;
                    }
                }
                // - 現在駅距離～次の停車駅の距離程なら
                foreach (CautionData cd in this.ValidCautions)
                {
                    if (cd.Type > 0 && cd.Received == 2 && cd.LimpSpeed > 0)
                    {
                        if (this.mBveStaff.StationList[mStationIndex].Location < cd.Location1 && 
                            cd.Location1 <= nextStopLocation)
                        {
                            cd.Received = 3;
                            Log.Write("[Caution.Received] 3 No=" + cd.Number.ToString());
                        }
                    }
                }

                if (pNearStation < 2 && this.mBveStaff.StationList[this.mStationIndex].StaffHensuu.ChakuFlag == "P")
                {
                    // 走行中(次駅通過)
                    mStationIndex++;
                    SetDoorOpen(false);
                    Log.Write("[PushNextButton] F1 SI=" + mStationIndex.ToString());
                }
                else if (pNearStation < 2)
                {
                    // 走行中(次駅停車) 停車中表示へ
                    SetDoorOpen(true);
                    Log.Write("[PushNextButton] F2");
                }
                else if (pNearStation >= 2 && mStationIndex < this.mBveStaff.StationList.Count - 1)
                {
                    // 停車中
                    mStationIndex++;
                    SetDoorOpen(false);
                    Log.Write("[PushNextButton] F3 SI=" + mStationIndex.ToString());
                }
                else
                {
                    Log.Write("[PushNextButton] F4");
                }
            }
            else
            {
                // 戻る
                if (this.mStationIndex > 0)
                {
                    if (pNearStation < 2)
                    {
                        // 走行中 1つ前に戻る
                        if (this.mStationIndex > 0)
                        {
                            this.mStationIndex--;
                            if (this.mBveStaff.StationList[this.mStationIndex].StaffHensuu.ChakuFlag == "P")
                            {
                                // 前駅通過
                                SetDoorOpen(false, true);
                                Log.Write("[PushNextButton] B1");
                            }
                            else
                            {
                                // 前駅停車
                                SetDoorOpen(true, true);
                                Log.Write("[PushNextButton] B2");
                            }
                        }
                        else
                        {
                            Log.Write("[PushNextButton] B3");
                        }
                    }
                    else
                    {
                        // 停車中
                        SetDoorOpen(false, true);
                        Log.Write("[PushNextButton] B4");
                    }
                }
                else
                {

                }
            }

        }

        /// <summary>
        /// スタフタップ
        /// </summary>
        /// <param name="pIndex"></param>
        public void SetStaionIndex(int pIndex)
        {
            if (0 <= pIndex && pIndex < this.mBveStaff.StationList.Count)
            {
                mStationIndex = pIndex;
                if (this.mBveStaff.StationList[this.mStationIndex].StaffHensuu.ChakuFlag == "P")
                {
                    // 通過
                    SetDoorOpen(false);
                }
                else
                {
                    // 停車中表示へ
                    SetDoorOpen(true);
                }
            }
        }

        /// <summary>
        /// 通告タップ
        /// </summary>
        /// <param name="pIndex"></param>
        public void ClickCaution(int pIndex)
        {
            if(pIndex == -1)
            {
                if (this.Cautions.Count > 0)
                {
                    for (int i = 0; i < this.Cautions.Count; i++)
                    {
                        if (this.Cautions[i].Type == -1)
                        {
                            // 詳細通告をセット
                            this.DispDetailCaution = this.Cautions[i];
                        }
                    }
                    // ※毎フレーム処理なので特別な操作はしない
                }
            }
            else if (this.ValidCautionCount > 0 && 0 <= pIndex && pIndex < this.ValidCautionCount)
            {
                // 詳細通告をセット
                this.DispDetailCaution = this.ValidCautions[pIndex];
                // ※毎フレーム処理なので特別な操作はしない
            }
        }

        /// <summary>
        /// 通告行タップ
        /// </summary>
        /// <param name="pIndex"></param>
        public void ClickDetailCaution(int pIndex)
        {
            if (this.DispDetailCautionCount > 0 && 0 <= pIndex && pIndex < this.DispDetailCautionCount)
            {
                // 詳細通告をセット
                this.DispDetailCaution.Checked[pIndex].Checked = true;
                // ※毎フレーム処理なので特別な操作はしない
            }
        }

        /// <summary>
        /// 通告受領タップ
        /// </summary>
        public void CloseDetailCaution()
        {
            if (this.DispDetailCaution.Type < 0 || this.DispDetailCaution.Type == 1 && this.DispDetailCaution.LimpSpeed == 0)
            {
                if (this.DispDetailCaution.Received == 0)
                {
                    this.DispDetailCaution.Received = 1;
                    Log.Write("[DispCaution.Received] A1 No=" + this.DispDetailCaution.Number.ToString());
                }
                else
                {
                    this.DispDetailCaution.Received = 4;
                    Log.Write("[DispCaution.Received] A4 No=" + this.DispDetailCaution.Number.ToString());
                    this.DispDetailCaution = null;
                }
                CheckLimpSpeedList();
            }
            else
            {
                bool allChecked = true;
                for (int i = 0; i < 5; i++)
                {
                    if (this.DispDetailCaution.Checked == null)
                    {
                        continue;
                    }
                    else if (i >= this.DispDetailCaution.Checked.Length)
                    {
                        break;
                    }
                    else if (this.DispDetailCaution.Checked[i].Checked == false)
                    {
                        allChecked = false;
                        break;
                    }
                }
                if (allChecked == true)
                {
                    // 受領済みにして一覧に戻す
                    if (this.DispDetailCaution.Received == 0)
                    {
                        this.DispDetailCaution.Received = 1;
                        Log.Write("[DispCaution.Received] 1 No=" + this.DispDetailCaution.Number.ToString());
                        // リセット
                        for (int i = 0; i < 5; i++)
                        {
                            if (this.DispDetailCaution.Checked == null)
                            {
                                continue;
                            }
                            else if (i >= this.DispDetailCaution.Checked.Length)
                            {
                                break;
                            }
                            else
                            {
                                this.DispDetailCaution.Checked[i].Checked = false;
                            }
                        }
                        this.DispDetailCaution = null;
                    }
                    else if (this.DispDetailCaution.Received == 1)
                    {
                        this.DispDetailCaution.Received = 2;
                        Log.Write("[DispCaution.Received] 2 No=" + this.DispDetailCaution.Number.ToString());
                        // - 次の停車駅の距離程
                        double nextStopLocation = this.mBveStaff.StationList[mStationIndex].Location;
                        for (int j = 0; j < this.mBveStaff.StationList.Count - 1; j++)
                        {
                            if (this.mBveStaff.StationList[j].Location >= 0 &&
                                this.mBveStaff.StationList[mStationIndex].Location < this.mBveStaff.StationList[j].Location &&
                                this.mBveStaff.StationList[j].StaffHensuu.ChakuFlag != "P")
                            {
                                nextStopLocation = this.mBveStaff.StationList[j].Location;
                                break;
                            }
                        }
                        // - 現在駅距離～次の停車駅の距離程なら
                        foreach (CautionData cd in this.ValidCautions)
                        {
                            if (this.DispDetailCaution.Type > 0 && this.DispDetailCaution.LimpSpeed > 0)
                            {
                                if (this.mBveStaff.StationList[mStationIndex].Location < this.DispDetailCaution.Location1 &&
                                    this.DispDetailCaution.Location1 <= nextStopLocation)
                                {
                                    this.DispDetailCaution.Received = 4; // ここに来るということは直前で受領した　4にする
                                    Log.Write("[DispCaution.Received] 2-4 No=" + this.DispDetailCaution.Number.ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        this.DispDetailCaution.Received = 4;
                        Log.Write("[DispCaution.Received] 4 No=" + this.DispDetailCaution.Number.ToString());
                    }
                    this.DispDetailCaution = null;
                    CheckLimpSpeedList();
                }
            }
            DrawNext();
        }
        #endregion

        /// <summary>
        /// すべて受領済みにする
        /// </summary>
        public void ReceiveAllCaution()
        {
            if(this.ValidCautions != null && this.ValidCautions.Count > 0)
            {
                foreach(CautionData DC in this.ValidCautions)
                {
                    if (DC.Type < 0 || DC.Type == 1 && DC.LimpSpeed == 0)
                    {
                        DC.Received = 4;
                        Log.Write("[ReceiveAllCaution] A4 No=" + DC.Number.ToString());
                        //this.DispDetailCaution = null;
                        //CheckLimpSpeedList();
                    }
                    else
                    {
                        bool allChecked = true;
                        for (int i = 0; i < 5; i++)
                        {
                            if (DC.Checked == null)
                            {
                                continue;
                            }
                            else if (i >= DC.Checked.Length)
                            {
                                break;
                            }
                            else if (DC.Checked[i].Checked == false)
                            {
                                DC.Checked[i].Checked = true;
                                break;
                            }
                        }
                        if (allChecked == true)
                        {
                            // 受領済みにして一覧に戻す
                            DC.Received = 4;
                            Log.Write("[ReceiveAllCaution] 4 No=" + DC.Number.ToString());
                            //this.DispDetailCaution = null;
                            //CheckLimpSpeedList();
                        }
                    }
                }
                CheckLimpSpeedList();
                //if(this.LimpSpeedList != null)
                //{
                //    if (this.LimpSpeedList.Count > 0)
                //    {
                //        Log.Write("[ReceiveAllCaution] LimpSpeedList");
                //        foreach(int spd in this.LimpSpeedList)
                //        {
                //            Log.Write("                      ・" + spd.ToString());
                //        }

                //    }
                //    else
                //    {
                //        Log.Write("[ReceiveAllCaution] LimpSpeedList.Count=0");
                //    }
                //}
                //else
                //{
                //    Log.Write("[ReceiveAllCaution] LimpSpeedList=null");
                //}
                DrawNext();
            }
        }

        public void LogInit()
        {
            Log.Init();
        }
        public void LogWrite(string txt)
        {
            Log.Write(txt);
        }
        public void LogDispose()
        {
            Log.Dispose();
        }

        #region SetCars
        public void SetCars(int car)
        {
            this.pCars = car;
            DrawHeader();
        }
        #endregion

        #region 通告
        /// <summary>
        /// 通告データをセット
        /// </summary>
        /// <param name="pCaution">通告データ</param>
        public void SetCaution(CautionData pCaution)
        {
            if(pCaution != null)
            {
                pCaution.Devide();
                if(pCaution.Type == 1)
                {
                    // 1 計画徐行の場合
                    // 速度制限=0 (デフォルトの徐行なし)があれば上書き
                    bool dfAri = false;
                    for (int i = 0; i < this.Cautions.Count; i++)
                    {
                        if (this.Cautions[i].Type == 1 && this.Cautions[i].LimpSpeed <= 0 && pCaution.LimpSpeed > 0)
                        {
                            this.Cautions[i] = pCaution;
                            dfAri = true;
                            Log.Write("[ValidCautions.Upd] 1 No=" + pCaution.Number.ToString() + " DS=" + pCaution.DispStart.ToString());
                            break;
                        }
                        else if (this.Cautions[i].Type == 1 && this.Cautions[i].LimpSpeed > 0 && pCaution.LimpSpeed <= 0)
                        {
                            // 上書き不可
                            dfAri = true;
                            break;
                        }
                    }
                    if (dfAri == false)
                    {
                        this.Cautions.Add(pCaution);
                    }
                    dfAri = false;
                    for (int i = 0; i < this.ValidCautions.Count; i++)
                    {
                        if (this.ValidCautions[i].Type == 1 && this.ValidCautions[i].LimpSpeed <= 0 && pCaution.LimpSpeed > 0)
                        {
                            this.ValidCautions[i] = pCaution;
                            pCaution.Disped = true;
                            dfAri = true;
                            Log.Write("[ValidCautions.Upd] 2 No=" + pCaution.Number.ToString() + " DS=" + pCaution.DispStart.ToString());
                            break;
                        }
                        else if (this.ValidCautions[i].Type == 1 && this.ValidCautions[i].LimpSpeed > 0 && pCaution.LimpSpeed <= 0)
                        {
                            // 上書き不可
                            dfAri = true;
                            break;
                        }
                    }
                    if (dfAri == false && pCaution.DispStart == 0)
                    {
                        this.ValidCautions.Add(pCaution);
                        pCaution.Disped = true;
                        Log.Write("[ValidCautions.Add] 1 No=" + pCaution.Number.ToString() + " DS=" + pCaution.DispStart.ToString());
                    }

                }
                else
                {
                    this.Cautions.Add(pCaution);
                    if (pCaution.DispStart == 0)
                    {
                        this.ValidCautions.Add(pCaution);
                        pCaution.Disped = true;
                        Log.Write("[ValidCautions.Add] 2 No=" + pCaution.Number.ToString() + " DS=" + pCaution.DispStart.ToString());
                    }
                }
                CheckLimpSpeedList();
            }
        }
        /// <summary>
        /// 有効な通告データから徐行リストを生成
        /// </summary>
        private void CheckLimpSpeedList()
        {
            try
            {
                this.LimpSpeedList = new List<int>();
                for (int j = 0; j < this.mBveStaff.StationList.Count - 1; j++)
                {
                    this.LimpSpeedList.Add(-1);
                }

                if (this.ValidCautions != null)
                {
                    foreach (CautionData cd in this.ValidCautions)
                    {
                        if (cd.Type > 0 && cd.Received > 0 && cd.LimpSpeed > 0)
                        {
                            for (int j = 0; j < this.mBveStaff.StationList.Count - 1; j++)
                            {
                                if (this.mBveStaff.StationList[j].Location >= 0 && this.mBveStaff.StationList[j].Location <= cd.Location1 && cd.Location1 < this.mBveStaff.StationList[j + 1].Location)
                                {
                                    for (int k = j; k < this.mBveStaff.StationList.Count - 1; k++)
                                    {
                                        if (cd.Location2 < this.mBveStaff.StationList[k].Location)
                                        {
                                            break;
                                        }
                                        if (this.LimpSpeedList[k] == -1 || this.LimpSpeedList[k] > cd.LimpSpeed)
                                        {
                                            this.LimpSpeedList[k] = cd.LimpSpeed;
                                        }
                                        if (this.mBveStaff.StationList[k].Location >= 0 && this.mBveStaff.StationList[k].Location <= cd.Location2 && cd.Location2 < this.mBveStaff.StationList[k + 1].Location)
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Write("[CheckLimpSpeedList] " + ex.Message);
            }
        }
        #endregion

        #region 駅接近解除
        /// <summary>
        /// 駅接近状態を解除
        /// </summary>
        public void ResetNearStation()
        {
            // 奇数→1引く
            this.pNearStation -= (this.pNearStation % 2);
        }
        #endregion

        private void SetTBL(string pKind, int pStart, int pLen, string pEtc)
        {
            try
            {
                DataRow ROW = RailwayTBL.NewRow();
                ROW["kind"] = pKind;
                ROW["start"] = pStart + 1100;
                if (pKind == "SIG" || pKind == "CRO")
                {
                    ROW["end"] = pStart + 1100;
                }
                else 
                {
                    ROW["end"] = pStart + 1100 + pLen;
                }
                ROW["etc"] = pEtc;
                DataRow[] rows = RailwayTBL.Select("kind='" + pKind + "' AND start=" + (pStart + 1100).ToString());
                if(rows.Count() == 0)
                {
                    RailwayTBL.Rows.Add(ROW);
                }
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.SetBeaconData] : " + ex.Message);
            }
        }

        #region 送信データ
        public string MakeComText(AtsEngine.MyVehicleState myVehicleState, int pStationIndex, List<CautionSchema> pCautionSchemas)
        {
            string retval = "";
            try
            {
                ComSchema comSchema = new ComSchema();
                comSchema.Time = (int)(myVehicleState.NewState.Time / 1000);
                comSchema.Location = (int)myVehicleState.NewState.Location;
                comSchema.PanelLocation = (int)Math.Abs(myVehicleState.Location);
                comSchema.Speed = Math.Abs(myVehicleState.NewState.Speed);
                //if (this.pStopFlag == true)
                //{
                //    comSchema.StopStation = 1;
                //}
                //else
                //{
                //    comSchema.StopStation = 0;
                //}
                comSchema.StaId = pStationIndex;

                if (this.lBeacon != null)
                {
                    comSchema.Beacon = lBeacon.ToArray();
                    lBeacon = null;
                }

                if (pCautionSchemas != null && pCautionSchemas.Count > 0)
                {
                    comSchema.Caution = pCautionSchemas.ToArray();
                    pCautionSchemas.Clear();
                }

                retval = MakeComText(comSchema);
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.MakeComText] : " + ex.Message);
                retval = "";
            }
            return retval;
        }
        public string MakeComText(ComSchema comSchema)
        {
            //JsonSerializer.Serialize(comSchema);
            string retval = "";
            string bsl = "";
            try
            {
                retval = "{";
                retval += "\"Time\":" + comSchema.Time.ToString() + ",";
                retval += "\"Location\":" + comSchema.Location.ToString() + ",";
                retval += "\"PanelLocation\":" + comSchema.PanelLocation.ToString() + ",";
                retval += "\"Speed\":" + comSchema.Speed.ToString("0.0") + ",";
                //retval += "\"StopStation\":" + comSchema.StopStation.ToString() + ",";
                retval += "\"StaId\":" + comSchema.StaId.ToString() + "";

                if (comSchema.Doors != null)
                {
                    bsl = "";
                    retval += ",\"Doors\":{";
                    bsl += "\"Open\":" + comSchema.Doors.Open.ToString();// + ",";
                    retval += "}";
                }

                if (comSchema.Beacon != null && comSchema.Beacon.Count() > 0)
                {
                    bsl = "";
                    retval += ",\"Beacon\":[";
                    foreach(BeaconSchema bs in comSchema.Beacon)
                    {
                        if (bsl != "") bsl += ",";
                        bsl += "{";
                        bsl += "\"Type\":" + bs.Type.ToString() + ",";
                        bsl += "\"Distance\":" + bs.Distance.ToString("0.0") + ",";
                        bsl += "\"Signal\":" + bs.Signal.ToString() + ",";
                        bsl += "\"Optional\":" + bs.Optional.ToString();
                        bsl += "}";
                    }
                    retval += bsl + "]";
                }

                if (comSchema.Caution != null && comSchema.Caution.Count() > 0)
                {
                    bsl = "";
                    retval += ",\"Caution\":[";
                    foreach (CautionSchema bs in comSchema.Caution)
                    {
                        if (bsl != "") bsl += ",";
                        bsl += "{";
                        bsl += "\"Number\":" + bs.Number.ToString() + ",";
                        bsl += "\"Type\":" + bs.Type.ToString() + ",";
                        bsl += "\"Title\":\"" + bs.Title + "\",";
                        bsl += "\"Caution\":\"" + bs.Caution + "\",";
                        bsl += "\"DispStart\":" + bs.DispStart.ToString("0.0") + ",";
                        bsl += "\"Location1\":" + bs.Location1.ToString("0.0") + ",";
                        bsl += "\"Location2\":" + bs.Location2.ToString("0.0") + ",";
                        bsl += "\"LimpSpeed\":" + bs.LimpSpeed.ToString() + "";
                        bsl += "}";
                    }
                    retval += bsl + "]";
                }

                retval += "}";
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.MakeComText] : " + ex.Message);
                retval = "";
            }
            return retval;
        }
        #endregion

        #region 受信データ
        // このプラグインでは受信はしない
        #endregion
    }
}
