﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AtsPlugin
{
    public class Config : EngineBase
    {
        #region 定義
        public bool DebugMode;

        /// <summary>
        /// 車両設定
        /// </summary>
        public CFG_VEHICLE_SPEC VehicleSpec;

        /// <summary>
        /// CASTセクション
        /// </summary>
        public CFG_CAST Cast;

        /// <summary>
        /// SOUNDセクション
        /// </summary>
        public CFG_SOUND Sound;

        /// <summary>
        /// 駅リスト
        /// </summary>
        public Dictionary<int, string> Stations;

        /// <summary>
        /// チェック対象の仮想キーコードリスト
        /// </summary>
        public List<int> KeyCodes;

        #region キーボードの入力方法
        public enum KeyInput_ENUM
        {
            AtsKey = 0,
            Keyboard = 1,
            Com = 2
        }
        #endregion

        public string StationTxt = "";
        #endregion

        #region クラス

        #region - Base
        public class CFG_BASE
        {

            public int GetIndexFromXml(XElement row, string ColumName, int _default)
            {
                if (row.Element(ColumName) != null && row.Element(ColumName).Value != null)
                {
                    if (int.TryParse(row.Element(ColumName).Value, out int i) == true)
                    {
                        return GetIndex(i);
                    }
                    else
                    {
                        return _default;
                    }
                }
                else
                {
                    return _default;
                }

            }

            public int GetIndexFromXmlDirect(XElement row, int _default)
            {
                if (row != null && row.Value != null)
                {
                    if (int.TryParse(row.Value, out int i) == true)
                    {
                        return GetIndex(i);
                    }
                    else
                    {
                        return _default;
                    }
                }
                else
                {
                    return _default;
                }

            }

            public int GetIndex(int pIndex)
            {
                if (pIndex < 0 || pIndex > 255)
                {
                    return -1;
                }
                else
                {
                    return pIndex;
                }
            }
            //protected void AddPanelIndexList(ref Dictionary<int, PanelIndex_ENUM> dt, int _index, PanelIndex_ENUM _subject)
            //{
            //    if (dt != null && _index != -1)
            //    {
            //        if (dt.ContainsKey(_index) == true)
            //        {
            //            dt[_index] = _subject;
            //        }
            //        else
            //        {
            //            dt.Add(_index, _subject);
            //        }
            //    }
            //}
            //protected void AddSoundIndexList(ref Dictionary<int, SoundIndex_ENUM> dt, int _index, SoundIndex_ENUM _subject)
            //{
            //    if (dt != null && _index != -1)
            //    {
            //        if (dt.ContainsKey(_index) == true)
            //        {
            //            dt[_index] = _subject;
            //        }
            //        else
            //        {
            //            dt.Add(_index, _subject);
            //        }
            //    }
            //}
        }

        #endregion

        #region VehicleSpec
        public class CFG_VEHICLE_SPEC
        {
            #region 変数定義
            /// <summary>
            /// 常用ブレーキノッチ数
            /// </summary>
            private int pSvcBrakeNotches;
            /// <summary>
            /// 常用ブレーキノッチ数
            /// </summary>
            public int SvcBrakeNotches
            {
                get { return this.pSvcBrakeNotches; }
            }

            /// <summary>
            /// 非常ブレーキノッチ段
            /// </summary>
            private int pEmgBrakeNotch;
            /// <summary>
            /// 非常ブレーキノッチ段
            /// </summary>
            public int EmgBrakeNotch
            {
                get { return this.pEmgBrakeNotch; }
            }

            /// <summary>
            /// 加速ブレーキノッチ数
            /// </summary>
            private int pPowerNotches;
            /// <summary>
            /// 加速ブレーキノッチ数
            /// </summary>
            public int PowerNotches
            {
                get { return this.pPowerNotches; }
            }

            /// <summary>
            /// ATSキャンセルノッチ
            /// </summary>
            private int pAtsCancelNotch;
            /// <summary>
            /// ATSキャンセルノッチ
            /// </summary>
            public int AtsCancelNotches
            {
                get { return this.pAtsCancelNotch; }
            }

            /// <summary>
            /// 車両数
            /// </summary>
            private int pCarCount;
            /// <summary>
            /// 車両数
            /// </summary>
            public int CarCount
            {
                get { return pCarCount; }
            }

            #endregion

            #region コンストラクタ
            public CFG_VEHICLE_SPEC()
            {
                this.pSvcBrakeNotches = 0;
                this.pEmgBrakeNotch = this.pSvcBrakeNotches + 1;
                this.pPowerNotches = 0;
                this.pAtsCancelNotch = 0;
                this.pCarCount = 0;
            }
            #endregion

            #region VehicleSpecのセット
            public void SetSpec(AtsDefine.AtsVehicleSpec vehicleSpec)
            {
                this.pSvcBrakeNotches = vehicleSpec.BrakeNotches;
                this.pEmgBrakeNotch = this.pSvcBrakeNotches + 1;
                this.pPowerNotches = vehicleSpec.PowerNotches;
                this.pAtsCancelNotch = vehicleSpec.AtsNotch;
                this.pCarCount = vehicleSpec.Cars;

            }
            #endregion
        }
        #endregion

        #region - Cast
        public class CFG_CAST : CFG_BASE
        {
            #region 定義 
            /// <summary>
            /// 描画サイズ
            /// </summary>
            public Size ImageSize { get; set; }

            /// <summary>
            /// 対象のファイル名のパス(ファイル名)
            /// </summary>
            public string ImageName { get; set; }

            /// <summary>
            /// パネル上の座標
            /// </summary>
            public Point Location { get; set; }

            /// <summary>
            /// レイヤー
            /// </summary>
            public int Layer { get; set; }

            private bool pDeformation;
            /// <summary>
            /// 変形するか？
            /// </summary>
            public bool Deformation {
                get { return this.pDeformation; }
                set { this.pDeformation = value; }
            }

            /// <summary>
            /// 変形後座標
            /// </summary>
            public Point[] DeformationPoint { get; set; }

            /// <summary>
            /// 出力サイズ(矩形)
            /// </summary>
            public Size DxSize { get; set; }

            public double[][] HomographyMatrix;

            #region Com変数定義
            /// <summary>
            /// 送受信をするか
            /// </summary>
            private bool pUseCom;
            /// <summary>
            /// 送受信をするか
            /// </summary>
            public bool UseCom
            {
                get { return this.pUseCom; }
            }

            /// <summary>
            /// 送信か
            /// </summary>
            private bool pIsOutput;
            /// <summary>
            /// 送信か
            /// </summary>
            public bool isOutput
            {
                get { return this.pIsOutput; }
            }

            /// <summary>
            /// ホスト名
            /// </summary>
            private string pHostName;
            /// <summary>
            /// ホスト名
            /// </summary>
            public string HostName
            {
                get { return this.pHostName; }
            }

            /// <summary>
            /// 自分のポート番号
            /// </summary>
            private int pMyPort;
            /// <summary>
            /// ポート番号
            /// </summary>
            public int MyPort
            {
                get { return this.pMyPort; }
            }

            /// <summary>
            /// 相手のポート番号
            /// </summary>
            private int pToPort;
            /// <summary>
            /// ポート番号
            /// </summary>
            public int ToPort
            {
                get { return this.pToPort; }
            }

            #endregion

            #endregion

            /// <summary>
            /// コンストラクタ
            /// </summary>
            public CFG_CAST()
            {
                this.ImageSize = new Size(128, 128);
                this.ImageName = "";
                this.DxSize = new Size(128, 128);
                this.Location = new Point(0, 0);
                this.Layer = 0;

                this.Deformation = false;
                this.DeformationPoint = new Point[4];
                this.DeformationPoint[0] = new Point(0, 0);
                this.DeformationPoint[1] = new Point(128, 0);
                this.DeformationPoint[2] = new Point(128, 128);
                this.DeformationPoint[3] = new Point(0, 128);

                this.HomographyMatrix = HL.MatrixOpe.MatrixIdentity(3);

                this.pUseCom = false;
                this.pIsOutput = true;
                this.pHostName = "127.0.0.1";
                this.pMyPort = 49200;
                this.pToPort = 49200;
            }

            #region 読み込み
            public void LoadXml(XElement row)
            {
                if (row.Element("Size") != null && row.Element("Size").Value != null)
                {
                    // w,h の形で入力
                    string[] s = row.Element("Size").Value.Split(',');
                    int w = 128;
                    int h = 128;
                    if (s.Count() == 2)
                    {
                        if ((int.TryParse(s[0], out w) == true) && (int.TryParse(s[1], out h) == true))
                        {
                            if (w > 0 && h > 0)
                            {
                                this.ImageSize = new Size(w, h);

                                int dw = (int)Math.Pow(2, (int)Math.Ceiling(Math.Log(w, 2)));
                                int dh = (int)Math.Pow(2, (int)Math.Ceiling(Math.Log(h, 2)));
                                this.DxSize = new Size(dw, dh);
                            }
                        }
                    }
                }

                if (row.Element("Location") != null && row.Element("Location").Value != null)
                {
                    // w,h の形で入力
                    string[] s = row.Element("Location").Value.Split(',');
                    int x = 128;
                    int y = 128;
                    if (s.Count() == 2)
                    {
                        if ((int.TryParse(s[0], out x) == true) && (int.TryParse(s[1], out y) == true))
                        {
                            this.Location = new Point(x, y);
                        }
                    }
                }

                if (row.Element("Layer") != null && row.Element("Layer").Value != null)
                {
                    int l = 0;
                    if (int.TryParse(row.Element("Layer").Value, out l) == true)
                    {
                        this.Layer = l;
                    }
                }

                if (row.Element("Deformation") != null && row.Element("Deformation").Value != null)
                {
                    if ((bool.TryParse(row.Element("Deformation").Value, out this.pDeformation) == false))
                    {
                        this.pDeformation = false;
                    }
                }

                if (row.Element("DeformationPoint") != null && row.Element("DeformationPoint").Value != null)
                {
                    // w,h の形で入力
                    string[] s = row.Element("DeformationPoint").Value.Split(',');
                    int[] sz = new int[8];
                    if (s.Count() == 8)
                    {
                        for (int i=0; i<8; i++)
                        {
                            if (int.TryParse(s[i], out sz[i]) == false)
                            {
                                this.pDeformation = false;
                                break;
                            }
                            if (i % 2 == 1)
                            {
                                this.DeformationPoint[(int)(i / 2)] = new Point(sz[i-1], sz[i]);
                            }
                        }
                    }
                }

                if (row.Element("ImageName") != null && row.Element("ImageName").Value != null)
                {
                    this.ImageName = row.Element("ImageName").Value;
                }

                if (this.Deformation == true)
                {
                    Point[] startP = new Point[4];
                    startP[0] = new Point(0, 0);
                    startP[1] = new Point(0, this.ImageSize.Height);
                    startP[2] = new Point(this.ImageSize.Width, this.ImageSize.Height);
                    startP[3] = new Point(this.ImageSize.Width, 0);

                    this.HomographyMatrix = HL.GraOpe.CalcHomographyInvertMatrix(startP, this.DeformationPoint);
                }

                if (row.Element("UseCom") != null && row.Element("UseCom").Value != null)
                {
                    if (bool.TryParse(row.Element("UseCom").Value, out this.pUseCom) == false)
                    {
                        this.pUseCom = false;
                    }
                }

                if (row.Element("Output") != null && row.Element("Output").Value != null)
                {
                    if (bool.TryParse(row.Element("Output").Value, out this.pIsOutput) == false)
                    {
                        this.pIsOutput = true;
                    }
                }

                if (row.Element("HostName") != null && row.Element("HostName").Value != null)
                {
                    this.pHostName = row.Element("HostName").Value;

                    if (this.pHostName == "")
                    {
                        this.pUseCom = false;
                    }
                }

                if (row.Element("MyPort") != null && row.Element("MyPort").Value != null)
                {
                    if (int.TryParse(row.Element("MyPort").Value, out this.pMyPort) == false)
                    {
                        this.pMyPort = 49200;
                    }
                    else if (this.pMyPort < 49152 || this.pMyPort > 65535)
                    {
                        this.pMyPort = 49200;
                    }
                }

                if (row.Element("ToPort") != null && row.Element("ToPort").Value != null)
                {
                    if (int.TryParse(row.Element("ToPort").Value, out this.pToPort) == false)
                    {
                        this.pToPort = 49200;
                    }
                    else if (this.pToPort < 49152 || this.pToPort > 65535)
                    {
                        this.pToPort = 49200;
                    }
                }

            }
            #endregion

            #region 読み込み スキーマから変換
            public void LoadJson(ConfigSchema.Cfg_Cast pCast, ConfigSchema.Cfg_Com pCom)
            {
                if (pCast != null)
                {
                    if (pCast.Size != null)
                    {
                        // w,h の形で入力
                        string[] s = pCast.Size.Split(',');
                        int w = 128;
                        int h = 128;
                        if (s.Count() == 2)
                        {
                            if ((int.TryParse(s[0], out w) == true) && (int.TryParse(s[1], out h) == true))
                            {
                                if (w > 0 && h > 0)
                                {
                                    this.ImageSize = new Size(w, h);

                                    int dw = (int)Math.Pow(2, (int)Math.Ceiling(Math.Log(w, 2)));
                                    int dh = (int)Math.Pow(2, (int)Math.Ceiling(Math.Log(h, 2)));
                                    this.DxSize = new Size(dw, dh);
                                }
                            }
                        }
                    }

                    if (pCast.Location != null)
                    {
                        // w,h の形で入力
                        string[] s = pCast.Size.Split(',');
                        int x = 128;
                        int y = 128;
                        if (s.Count() == 2)
                        {
                            if ((int.TryParse(s[0], out x) == true) && (int.TryParse(s[1], out y) == true))
                            {
                                this.Location = new Point(x, y);
                            }
                        }
                    }

                    this.Layer = pCast.Layer;

                    this.pDeformation = pCast.Deformation;

                    if (pCast.DeformationPoint != null)
                    {
                        string[] s2;
                        int w = 128;
                        int h = 128;
                        this.DeformationPoint[0] = new Point(0, 0);
                        this.DeformationPoint[1] = new Point(this.ImageSize.Width, 0);
                        this.DeformationPoint[2] = new Point(this.ImageSize.Width, this.ImageSize.Height);
                        this.DeformationPoint[3] = new Point(0, this.ImageSize.Height);

                        if (pCast.DeformationPoint.TopLeft != null)
                        {
                            s2 = pCast.DeformationPoint.TopLeft.Split(',');
                            w = 128;
                            h = 128;
                            if (s2.Count() == 2)
                            {
                                if ((int.TryParse(s2[0], out w) == true) && (int.TryParse(s2[1], out h) == true))
                                {
                                    this.DeformationPoint[0] = new Point(w, h);
                                }
                            }
                        }

                        if (pCast.DeformationPoint.TopRight != null)
                        {
                            s2 = pCast.DeformationPoint.TopRight.Split(',');
                            w = 128;
                            h = 128;
                            if (s2.Count() == 2)
                            {
                                if ((int.TryParse(s2[0], out w) == true) && (int.TryParse(s2[1], out h) == true))
                                {
                                    this.DeformationPoint[1] = new Point(w, h);
                                }
                            }
                        }

                        if (pCast.DeformationPoint.BottomRight != null)
                        {
                            s2 = pCast.DeformationPoint.BottomRight.Split(',');
                            w = 128;
                            h = 128;
                            if (s2.Count() == 2)
                            {
                                if ((int.TryParse(s2[0], out w) == true) && (int.TryParse(s2[1], out h) == true))
                                {
                                    this.DeformationPoint[2] = new Point(w, h);
                                }
                            }
                        }

                        if (pCast.DeformationPoint.BottomLeft != null)
                        {
                            s2 = pCast.DeformationPoint.BottomLeft.Split(',');
                            w = 128;
                            h = 128;
                            if (s2.Count() == 2)
                            {
                                if ((int.TryParse(s2[0], out w) == true) && (int.TryParse(s2[1], out h) == true))
                                {
                                    this.DeformationPoint[3] = new Point(w, h);
                                }
                            }
                        }
                    }

                    if (pCast.ImageName != null)
                    {
                        this.ImageName = pCast.ImageName;
                    }
                }


                if (this.Deformation == true)
                {
                    Point[] startP = new Point[4];
                    startP[0] = new Point(0, 0);
                    startP[1] = new Point(0, this.ImageSize.Height);
                    startP[2] = new Point(this.ImageSize.Width, this.ImageSize.Height);
                    startP[3] = new Point(this.ImageSize.Width, 0);

                    this.HomographyMatrix = HL.GraOpe.CalcHomographyInvertMatrix(startP, this.DeformationPoint);
                }

                if(pCom != null)
                {
                    this.pUseCom = pCom.UseCom;
                    this.pIsOutput = true; // 固定
                    if (pCom.HostName != null)
                    {
                        this.pHostName = pCom.HostName;
                    }
                    this.pMyPort = 49200; // 固定
                    if(49152 <= pCom.ToPort && pCom.ToPort <= 65535)
                    {
                        this.pToPort = pCom.ToPort;
                    }
                    else
                    {
                        this.pToPort = 49200;
                    }

                }

            }
            #endregion

            #region 作成
            public XElement MakeXml()
            {
                try
                {
                    XElement xml = new XElement("CAST");
                    XElement data;

                    data = new XElement("Size", this.ImageSize.Width.ToString() + "," + this.ImageSize.Height.ToString());
                    xml.Add(data);

                    data = new XElement("Location", this.Location.X.ToString() + "," + this.Location.Y.ToString());
                    xml.Add(data);

                    data = new XElement("Layer", this.Layer);
                    xml.Add(data);

                    if (this.Deformation == true)
                    {
                        data = new XElement("Deformation", this.Deformation.ToString());
                        xml.Add(data);

                        data = new XElement("DeformationPoint", this.DeformationPoint[0].X.ToString() + "," + this.DeformationPoint[0].Y.ToString() + "," +
                            this.DeformationPoint[1].X.ToString() + "," + this.DeformationPoint[1].Y.ToString() + "," +
                            this.DeformationPoint[2].X.ToString() + "," + this.DeformationPoint[2].Y.ToString() + "," +
                            this.DeformationPoint[3].X.ToString() + "," + this.DeformationPoint[3].Y.ToString());
                        xml.Add(data);

                    }

                    if (this.ImageName != "")
                    {
                        data = new XElement("ImageName", this.ImageName);
                        xml.Add(data);
                    }

                    data = new XElement("UseCom", this.UseCom);
                    xml.Add(data);

                    if (this.UseCom == true)
                    {
                        data = new XElement("Output", this.isOutput);
                        xml.Add(data);
                        data = new XElement("HostName", this.HostName);
                        xml.Add(data);
                        data = new XElement("MyPort", this.MyPort);
                        xml.Add(data);
                        data = new XElement("ToPort", this.ToPort);
                        xml.Add(data);
                    }

                    return xml;
                }
                catch (Exception ex)
                {
                    ExceptionMessage(ex, "make general", true);
                    return null;
                }
            }
            #endregion

        }
        #endregion

        #region - Sound
        public class CFG_SOUND : CFG_BASE
        {
            #region 定義 
            private int pFirstDeparture;
            /// <summary>
            /// 始発駅発車２分前駅接近
            /// </summary>
            public int FirstDeparture 
            {
                get { return this.pFirstDeparture; }
                set { this.pFirstDeparture = value; }
            }

            private int pNearStation;
            /// <summary>
            /// 停車駅接近
            /// </summary>
            public int NearStation
            {
                get { return this.pNearStation; }
                set { this.pNearStation = value; }
            }

            #endregion

            /// <summary>
            /// コンストラクタ
            /// </summary>
            public CFG_SOUND()
            {
                this.pFirstDeparture = -1;
                this.pNearStation = -1;
            }

            #region 読み込み
            public void LoadXml(XElement row)
            {
                if (row.Element("FirstDeparture") != null && row.Element("FirstDeparture").Value != null)
                {
                    if ((int.TryParse(row.Element("FirstDeparture").Value, out this.pFirstDeparture) == false))
                    {
                        this.pFirstDeparture = -1;
                    }
                    else if (this.pFirstDeparture < -1 || 255 < this.pFirstDeparture)
                    {
                        this.pFirstDeparture = -1;
                    }
                }

                if (row.Element("NearStation") != null && row.Element("NearStation").Value != null)
                {
                    if ((int.TryParse(row.Element("NearStation").Value, out this.pNearStation) == false))
                    {
                        this.pNearStation = -1;
                    }
                    else if(this.pNearStation < -1 || 255 < this.pNearStation)
                    {
                        this.pNearStation = -1;
                    }
                }

            }
            #endregion

            #region 読み込み スキーマから変換
            public void LoadJson(ConfigSchema.Cfg_Sound pSound)
            {
                if(pSound != null)
                {
                    this.pFirstDeparture = GetIndex(pSound.FirstDeparture);
                    this.pNearStation = GetIndex(pSound.NearStation);

                }
            }
            #endregion

            #region 作成
            public XElement MakeXml()
            {
                try
                {
                    XElement xml = new XElement("SOUND");
                    XElement data;

                    data = new XElement("FirstDeparture", this.pFirstDeparture.ToString());
                    xml.Add(data);

                    data = new XElement("NearStation", this.pNearStation.ToString());
                    xml.Add(data);

                    return xml;
                }
                catch (Exception ex)
                {
                    ExceptionMessage(ex, "make general", true);
                    return null;
                }
            }
            #endregion

        }
        #endregion


        #endregion

        #region コンストラクタ
        public Config()
        {
            this.DebugMode = false;
            this.VehicleSpec = new CFG_VEHICLE_SPEC();
            this.Cast = new CFG_CAST();
            this.KeyCodes = new List<int>();
            this.Stations = new Dictionary<int, string>();
        }
        #endregion

        #region 読み込み
        public bool MainLoad()
        {
            try
            {
                if (System.IO.Path.GetExtension(ModulePathWithFileName) != "")
                {
                    LoadStaions();
                    string XmlPath = ModulePathWithFileName.Replace(System.IO.Path.GetExtension(ModulePathWithFileName), ".xml"); // 拡張子を.xmlに変更
                    string JsonPath = ModulePathWithFileName.Replace(System.IO.Path.GetExtension(ModulePathWithFileName), ".json"); // 拡張子を.jsonに変更
                    return Load(XmlPath, JsonPath);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "設定読み込み(MainLoad)", true);
                return false;
            }
        }

        public bool Load(string XmlPath, string JsonPath)
        {
            try
            {
                if (System.IO.File.Exists(XmlPath) == true)
                {
                    System.IO.File.Copy(XmlPath, System.IO.Path.GetDirectoryName(XmlPath) + "\\" + System.IO.Path.GetFileNameWithoutExtension(XmlPath) + "_back.xml", true);

                    var table = XDocument.Load(XmlPath).Element("CastPanel");

                    // Castセクション
                    var row = table.Element("CAST");
                    this.Cast = new CFG_CAST();
                    this.Cast.LoadXml(row);

                    // Sound
                    row = table.Element("SOUND");
                    this.Sound = new CFG_SOUND();
                    this.Sound.LoadXml(row);

                    return true;
                }
                else if(System.IO.File.Exists(JsonPath) == true)
                {
                    ConfigSchema JsConfig = JsonSerializer.Deserialize<ConfigSchema>(File.ReadAllText(JsonPath));
                    if(JsConfig != null)
                    {
                        this.Cast = new CFG_CAST();
                        this.Cast.LoadJson(JsConfig.Cast, JsConfig.Com);

                        this.Sound = new CFG_SOUND();
                        this.Sound.LoadJson(JsConfig.Sound);

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "設定読み込み(Load)", true);
                return false;
            }
        }

        public void LoadStaions()
        {
            try
            {
                this.Stations = new Dictionary<int, string>();

                if (System.IO.File.Exists(ModuleDirectoryPath + "\\stations.csv") == true)
                {
                    // 読み込みたいCSVファイルのパスを指定して開く
                    using (StreamReader sr = new StreamReader(ModuleDirectoryPath + "\\stations.csv", System.Text.Encoding.UTF8))
                    {
                        // 末尾まで繰り返す
                        while (!sr.EndOfStream)
                        {
                            // CSVファイルの一行を読み込む
                            string line = sr.ReadLine();
                            // 読み込んだ一行をカンマ毎に分けて配列に格納する
                            string[] values = line.Split(',');

                            if (values.Count() >= 2 && int.TryParse(values[0], out int i) == true)
                            {
                                if (this.Stations.ContainsKey(i) == false)
                                {
                                    this.Stations.Add(i, values[1]);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "設定読み込み(LoadStations)", true);
            }
        }
        #endregion

        #region 保存
        public bool Save()
        {
            try
            {
                string XmlPath = ModulePathWithFileName.Replace(System.IO.Path.GetExtension(ModulePathWithFileName), ".xml");
                return Save(XmlPath);
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "Save", true);
                return false;
            }
        }
        public bool Save(string filepath)
        {
            try
            {
                XDocument xdoc = new XDocument(new XDeclaration("1.0", "Shift-JIS", "true"));
                XElement xml = new XElement("CastPanel");

                XElement cas = this.Cast.MakeXml();
                xml.Add(cas);

                XElement snd = this.Sound.MakeXml();
                xml.Add(snd);

                xdoc.Add(xml);

                xdoc.Save(filepath);

                return true;
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "Save", true);
                return false;
            }
        }
        #endregion

        #region キーコードリスト
        private void AddKeyCode(int pKeyCode)
        {
            try
            {
                if (this.KeyCodes == null)
                {
                    this.KeyCodes = new List<int>();
                }

                if (pKeyCode > 0 && this.KeyCodes.Contains(pKeyCode) == false)
                {
                    this.KeyCodes.Add(pKeyCode);
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "", true);
            }
        }
        #endregion

    }
}
