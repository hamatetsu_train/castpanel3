﻿using System;
//using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtsPlugin
{
    public class EngineBase
    {
        #region static

        public static string PluginName = "CastPanel3";

        #region - DLLのパス
        private static string pModulePathWithFileName = "";
        public static string ModulePathWithFileName
        {
            get
            {
                if (pModulePathWithFileName == "")
                {
                    pModulePathWithFileName = Assembly.GetExecutingAssembly().Location;
                }
                return pModulePathWithFileName;
            }
        }
        #endregion

        #region - DLLのファイル名
        private static string pModuleName = "";
        public static string ModuleName
        {
            get
            {
                if (pModuleName == "")
                {
                    pModuleName = Path.GetFileName(ModulePathWithFileName);
                }
                return pModuleName;
            }
        }
        #endregion

        #region - DLLのフォルダ
        private static string pModuleDirectoryPath = "";
        public static string ModuleDirectoryPath
        {
            get
            {
                if (pModuleDirectoryPath == "")
                {
                    pModuleDirectoryPath = Path.GetDirectoryName(ModulePathWithFileName); ;
                }
                return pModuleDirectoryPath;
            }
        }
        #endregion

        #region - ログ
        public static class Log
        {
            private static string FilePath = "";
            private static FileStream fs = null;
            private static bool fserror = false;

            public static void Init()
            {
                try
                {
                    // 使われてたらそのまま使う
                    if (fs == null)
                    {
                        // ログファイルパスの生成
                        FilePath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".log";
                        // ファイルを空にしたい
                        if (File.Exists(FilePath) == true)
                        {
                            File.Delete(FilePath);
                        }
                        // オープン
                        fs = File.Open(FilePath, FileMode.OpenOrCreate);
                        fs.Seek(0, SeekOrigin.End);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログファイルオープン処理でエラーが発生しました。\n" + ex.Message, "ATS-PTプラグイン");
                }
            }
            public static void Dispose()
            {
                try
                {
                    if (fs != null)
                    {
                        fs.Dispose();
                        //fs = null;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログファイルクローズ処理でエラーが発生しました。\n" + ex.Message, "ATS-PTプラグイン");
                }
            }

            public static void Write(string message)
            {
                try
                {
                    if (fs != null)
                    {
                        UTF8Encoding utf8 = new UTF8Encoding();
                        byte[] result = utf8.GetBytes(DateTime.Now.ToString("HH:mm:ss.fff") + ":" + message + "\n");
                        //fs.WriteAsync(result, 0, result.Length);
                        fs.Write(result, 0, result.Length);
                    }
                    //Console.WriteLine(message);
                }
                catch
                {
                    try
                    {
                        if (fs != null && fserror == false)
                        {
                            // 一度生成を試す
                            // ログファイルパスの生成
                            FilePath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".log";
                            // オープン
                            fs = File.Open(FilePath, FileMode.OpenOrCreate);
                            fs.Seek(0, SeekOrigin.End);
                            Write(message);
                        }
                    }
                    catch (Exception ex2)
                    {
                        MessageBox.Show("ログ処理でエラーが発生しました。\n" + ex2.Message, "ATS-PTプラグイン");
                        fserror = true;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region エラー表示
        /// <summary>
        /// エラー表示回数
        /// </summary>
        private int ExceptionCounter = 0;

        /// <summary>
        /// エラーダイアログを表示する
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="addtext"></param>
        /// <param name="showDialog"></param>
        public static void ExceptionMessage(Exception ex, string addtext, bool showDialog)
        {
            Log.Write(addtext + " (Error) " + ex.Message + "\n" + ex.StackTrace);
#if DEBUG
            MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
#else
            Console.Write(addtext + " (Error) " + ex.Message + "\n" + ex.StackTrace);
            if(showDialog == true)
            {
                MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
            }
#endif
        }

        /// <summary>
        /// １回目だけエラーダイアログを表示する
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="addtext"></param>
        /// <param name="showDialog"></param>
        public void ExceptionMessageOnce(Exception ex, string addtext, bool showDialog)
        {
#if DEBUG
            if (ExceptionCounter == 0)
            {
                ExceptionCounter++;
                MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
            }
            else
            {
                Console.Write(addtext + "\n" + ex.Message);
            }
#else
            if (showDialog == true && ExceptionCounter == 0)
            {
                ExceptionCounter++;
                MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
            }
            else
            {
                Console.Write(addtext + "\n" + ex.Message);
            }
#endif
            Log.Write(addtext + " (Error) " + ex.Message);
        }
        #endregion

        #region パネル・サウンドIO
        /// <summary>
        /// パネル・サウンドの指定したインデックスに値をセット
        /// </summary>
        /// <param name="panel">パネル・サウンドの配列先頭ポインタ</param>
        /// <param name="pIndex">インデックス</param>
        /// <param name="pValue">T/F</param>
        /// <returns></returns>
        internal static void SetIndexlValue(IntPtr panel, int pIndex, bool pValue)
        {
            SetIndexValue(panel, pIndex, Convert.ToInt32(pValue));
        }
        /// <summary>
        /// パネル・サウンドの指定したインデックスに値をセット
        /// </summary>
        /// <param name="panel">パネル・サウンドの配列先頭ポインタ</param>
        /// <param name="pIndex">インデックス</param>
        /// <param name="pValue">値</param>
        /// <returns></returns>
        internal static void SetIndexValue(IntPtr panel, int pIndex, int pValue)
        {
            // BVE本体に渡せる範囲の場合のみセット
            if (0 <= pIndex && pIndex <= 255)
            {
                const int SIZE_OF_TYPE = 4;
                Marshal.WriteInt32(panel, SIZE_OF_TYPE * pIndex, pValue);
            }
        }

        /// <summary>
        /// パネル・サウンドの指定したインデックスの値を取得
        /// </summary>
        /// <param name="panel">パネル・サウンドの配列先頭ポインタ</param>
        /// <param name="pIndex">インデックス</param>
        /// <returns></returns>
        internal static int ReadIndexValue(IntPtr panel, int pIndex)
        {
            int retval = 0;
            // BVE本体が持っている範囲の場合のみ
            if (0 <= pIndex && pIndex <= 255)
            {
                const int SIZE_OF_TYPE = 4;
                retval = Marshal.ReadInt32(panel, SIZE_OF_TYPE * pIndex);
            }
            return retval;
        }
        #endregion
    }
}
