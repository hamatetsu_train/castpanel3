﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

//using AtsPlugin.Base;

namespace AtsPlugin
{
    #region イベントクラス
    public class UdpComRcvMessageEventArgs : EventArgs
    {
        public string Message;
    }
    #endregion


    public class UdpCom
    {
        #region 変数定義
        /// <summary>
        /// 送信用クライアント
        /// </summary>
        private UdpClient pUdpSend;

        /// <summary>
        /// 送信先ホスト名/IPアドレス
        /// </summary>
        private string pSendHostName;

        /// <summary>
        /// 送信先ポート
        /// </summary>
        private int pSendPort;

        /// <summary>
        /// 受信中か？
        /// </summary>
        //private bool pIsRecieving;

        /// <summary>
        /// 受信用クライアント
        /// </summary>
        private UdpClient pUdpRecieve;

        /// <summary>
        /// 受信先ホスト名/IPアドレス
        /// </summary>
        private string pRecieveHostName;

        /// <summary>
        /// 受信先ポート
        /// </summary>
        private int pRecievePort;

        /// <summary>
        /// 受信文字列
        /// </summary>
        private string pRcvMessage;
        public string RcvMessage
        {
            get
            {
                string r = this.pRcvMessage;
                this.pRcvMessage = "";
                return r;
            }
        }

        /// <summary>
        /// 受信処理のスレッド
        /// </summary>
        //private Thread rcvThread;

        //TimeEventArgs型のオブジェクトを返すようにする
        public delegate void UdpComRcvMessageEventHandler(object sender, UdpComRcvMessageEventArgs e);

        //イベントデリゲートの宣言
        public event UdpComRcvMessageEventHandler ReceiveMessage;

        #endregion

        #region コンストラクタ
        public UdpCom()
        {

        }
        #endregion

        #region デストラクタ
        ~UdpCom()
        {
            End();
            if (this.pUdpSend != null)
            {
                this.pUdpSend = null;
                //this.pUdpSend.Dispose();
            }
            if (this.pUdpRecieve != null)
            {
                this.pUdpRecieve = null;
                //this.pUdpRecieve.Dispose();
            }
        }
        #endregion


        #region 初期化
        public bool SendInit(string HostName, int MyPort, int ToPort)
        {
            try
            {
                this.pUdpSend = new UdpClient();
                this.pSendPort = ToPort;
                this.pSendHostName = HostName;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("[SendInit]\n" + ex.Message);
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 受信クライントの初期化
        /// </summary>
        /// <param name="HostName"></param>
        /// <param name="PortNo"></param>
        /// <returns></returns>
        public bool ReceiveInit(string HostName, int PortNo)
        {
            try
            {
                //this.pIsRecieving = false;

                IPAddress ip;
                System.Net.IPEndPoint localEP;
                if (IPAddress.TryParse(HostName, out ip) == true)
                {
                    //localEP = new System.Net.IPEndPoint(ip, PortNo);
                    localEP = new System.Net.IPEndPoint(System.Net.IPAddress.Any, PortNo);
                }
                else
                {
                    localEP = new System.Net.IPEndPoint(System.Net.IPAddress.Any, PortNo);
                }
                this.pUdpRecieve = new UdpClient(localEP);

                this.pRecievePort = PortNo;
                this.pRecieveHostName = HostName;

                this.pRcvMessage = "";

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region 送信
        public void Send(string SendMessgage)
        {
            try
            {
                byte[] SendBytes = Encoding.ASCII.GetBytes(System.Uri.EscapeDataString(SendMessgage));
                //byte[] tempBytes = System.Text.Encoding.Convert(System.Text.Encoding.ASCII, System.Text.Encoding.UTF8, SendBytes);
                //this.pUdpSend.Send(SendBytes, SendBytes.Length, this.pSendHostName, this.pSendPort);
                Task.Run(() =>
                {
                    this.pUdpSend.Send(SendBytes, SendBytes.Length, this.pSendHostName, this.pSendPort);
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Send]\n" + ex.Message);
            }
        }

        #endregion

        #region 受信
        //public string Recieve()
        //{
        //    try
        //    {
        //        if (this.pIsRecieving == false)
        //        {
        //            this.pIsRecieving = true;
        //            this.RecieveCore();
        //        }

        //        return this.RcvMessage;
        //    }
        //    catch(Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        return "";
        //    }
        //}


        #region 実行処理
        //private void RecieveCore()
        //{
        //    IPEndPoint remoteEP = null; // 任意の送信元から受信
        //    try
        //    {
        //        while (true)
        //        {
        //            byte[] rcvBytes = this.pUdpRecieve.Receive(ref remoteEP);
        //            Interlocked.Exchange(ref this.pRcvMessage, Encoding.ASCII.GetString(rcvBytes));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        this.pIsRecieving = false;
        //    }
        //}

        public void Start_Recieve()
        {
            try
            {
                //this.rcvThread.Start();
                this.pUdpRecieve.BeginReceive(ReceiveCallback, this.pUdpRecieve);
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[UDPCOM:Start_Recieve]" + ex.Message);
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                System.Net.Sockets.UdpClient udp = (System.Net.Sockets.UdpClient)ar.AsyncState;

                //非同期受信を終了する
                System.Net.IPEndPoint remoteEP = null;
                byte[] rcvBytes;
                try
                {
                    rcvBytes = udp.EndReceive(ar, ref remoteEP);
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    EngineBase.Log.Write("[UDPCOM:ReceiveCallback]受信エラー(" + ex.Message + "/" + ex.ErrorCode.ToString() + ")");
                    return;
                }
                catch (ObjectDisposedException ex)
                {
                    //すでに閉じている時は終了
                    EngineBase.Log.Write("[UDPCOM:ReceiveCallback]Socketは閉じられています。" + ex.Message);
                    return;
                }

                //データを文字列に変換する
                string rcvMsg = System.Text.Encoding.UTF8.GetString(rcvBytes);

                this.pRcvMessage = rcvMsg;
#if DEBUG
                //AtsDebug.SetText(frmDebug.LabelKind_Enum.UdpRcv, rcvMsg);
#endif

                //返すデータの設定
                UdpComRcvMessageEventArgs e = new UdpComRcvMessageEventArgs();
                e.Message = rcvMsg;
                //イベントの発生
                OnReceiveMessage(e);

                //再びデータ受信を開始する
                udp.BeginReceive(ReceiveCallback, udp);
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[UDPCOM:ReceiveCallback]" + ex.Message);
            }
        }

        private void Stop_Recieve()
        {
            try
            {
                //this.rcvThread.Interrupt();
                this.pUdpRecieve.Close();
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[UDPCOM:Stop_Recieve]" + ex.Message);
            }
        }

        protected virtual void OnReceiveMessage(UdpComRcvMessageEventArgs e)
        {
            if (ReceiveMessage != null)
            {
                ReceiveMessage(this, e);
            }
        }
        #endregion

        #region 終了
        public void End()
        {
            try
            {
                //this.rcvThread.Abort();
                if (this.pUdpSend != null)
                {
                    this.pUdpSend.Close();
                }
                if (this.pUdpRecieve != null)
                {
                    this.pUdpRecieve.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        #endregion

    }
}
