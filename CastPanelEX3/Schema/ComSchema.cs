﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AtsPlugin
{
    public class ComSchema
    {
        /// <summary>
        /// BVE内時間[sec]
        /// </summary>
        public int Time { get; set; }

        /// <summary>
        /// BVE内ロケーション[m]
        /// </summary>
        public int Location { get; set; }

        /// <summary>
        /// 表示用ロケーション[m]
        /// </summary>
        public int PanelLocation { get; set; }

        /// <summary>
        /// BVE内速度[km/h]
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// 停車駅接近 1:true
        /// </summary>
        public int StopStation { get; set; }

        /// <summary>
        /// 駅番号
        /// </summary>
        public int StaId { get; set; }

        /// <summary>
        /// 停車場リストファイル文字列
        /// </summary>
        public string Stations { get; set; }

        /// <summary>
        /// 地上子データ
        /// </summary>
        public DoorSchema Doors { get; set; }

        /// <summary>
        /// 地上子データ
        /// </summary>
        public StationLocSchema[] StationLocs { get; set; }

        /// <summary>
        /// 地上子データ
        /// </summary>
        public BeaconSchema[] Beacon { get; set; }

        /// <summary>
        /// 通告データ
        /// </summary>
        public CautionSchema[] Caution { get; set; }
    }

    /// <summary>
    /// 停車場距離程データクラス
    /// </summary>
    public class DoorSchema
    {
        public bool Open { get; set; }
    }

    /// <summary>
    /// 地上子データクラス
    /// </summary>
    public class BeaconSchema
    {
        public int Type { get; set; }
        public double Distance { get; set; }
        public int Signal { get; set; }
        public int Optional { get; set; }
    }

    /// <summary>
    /// 停車場距離程データクラス
    /// </summary>
    public class StationLocSchema
    {
        public double Location { get; set; }
        public string Key { get; set; }
    }

    /// <summary>
    /// 通告データクラス
    /// </summary>
    public class CautionSchema
    {
        /// <summary>
        /// 通告番号
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 通告種類(-1:時刻表確認 0:通常 1:徐行 2:規制)
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 通告タイトル
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 通告内容
        /// </summary>
        public string Caution { get; set; }
        /// <summary>
        /// 開始距離程
        /// </summary>
        public double Location1 { get; set; }
        /// <summary>
        /// 終了距離程
        /// </summary>
        public double Location2 { get; set; }
        /// <summary>
        /// 規制速度
        /// </summary>
        public int LimpSpeed { get; set; }
        /// <summary>
        /// 表示開始距離程
        /// </summary>
        public double DispStart { get; set; }
    }
}
