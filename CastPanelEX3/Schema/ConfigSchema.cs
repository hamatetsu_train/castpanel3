﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtsPlugin
{
    /// <summary>
    /// JSON形式設定ファイル用スキーマ
    /// </summary>
    public class ConfigSchema
    {
        /// <summary>
        /// 全般設定
        /// </summary>
        public Cfg_Cast Cast { get; set; }
        /// <summary>
        /// サウンド設定
        /// </summary>
        public Cfg_Sound Sound { get; set; }
        /// <summary>
        /// 通信設定
        /// </summary>
        public Cfg_Com Com { get; set; }

        /// <summary>
        /// 全般設定
        /// </summary>
        public class Cfg_Cast
        {
            /// <summary>
            /// 描画サイズ
            /// </summary>
            public string Size { get; set; }

            /// <summary>
            /// DxDT適用画像ファイル名
            /// </summary>
            public string ImageName { get; set; }

            /// <summary>
            /// パネル上の座標
            /// </summary>
            public string Location { get; set; }

            /// <summary>
            /// パネル上のレイヤー
            /// </summary>
            public int Layer { get; set; }

            /// <summary>
            /// 変形させるか
            /// </summary>
            public bool Deformation { get; set; }

            /// <summary>
            /// 変形設定(変形しない場合はnull)
            /// </summary>
            public Cfg_DeformationPoint DeformationPoint { get; set; }
        }

        /// <summary>
        /// 変形設定
        /// </summary>
        public class Cfg_DeformationPoint
        {
            /// <summary>
            /// 左上座標
            /// </summary>
            public string TopLeft { get; set; }
            /// <summary>
            /// 右上座標
            /// </summary>
            public string TopRight { get; set; }
            /// <summary>
            /// 右下座標
            /// </summary>
            public string BottomRight { get; set; }
            /// <summary>
            /// 左下座標
            /// </summary>
            public string BottomLeft { get; set; }
        }

        /// <summary>
        /// 通信設定
        /// </summary>
        public class Cfg_Com
        {
            /// <summary>
            /// 外部送信を行うか
            /// </summary>
            public bool UseCom { get; set; }
            /// <summary>
            /// 宛先IPアドレス/ホスト名
            /// </summary>
            public string HostName { get; set; }
            /// <summary>
            /// 宛先ポート番号
            /// </summary>
            public int ToPort { get; set; }
        }

        /// <summary>
        /// サウンド設定
        /// </summary>
        public class Cfg_Sound
        {
            /// <summary>
            /// 発車２分前サウンド インデックス
            /// </summary>
            public int FirstDeparture { get; set; }

            /// <summary>
            /// 停車駅接近サウンド インデックス
            /// </summary>
            public int NearStation { get; set; }
        }
    }
}
