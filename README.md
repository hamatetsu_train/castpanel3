# Castもどきプラグイン
Bve Trainsim 6 / 5.8向けのATSプラグイン/アプリケーションです。  
  
  
## 内容
・Castもどきプラグイン(CastPanelEx3)  
・Castもどきアプリケーション(CastPanel3)  
  
  
## 車両データに組み込む
### ダウンロード  
[CastPanelEX3](https://gitlab.com/hamatetsu_train/castpanel3/-/tree/main/Plugin/)

CastPanelEx3はAtsEXの車両プラグインとして動作します。
[AtsEX](https://automatic9045.github.io/contents/bve/AtsEX/)のATSプラグイン版が別途必要です。  
AtsEXのサンプル車両データからAtsEx.x64.Caller.dllをコピーし、AtsEx.x64.Caller.dllからCastPanelEX3.dllが読み込まれるようにしてください。  

### 設定  
CastPanelEX3.dllと同じフォルダにCastPanelEX3.xmlを配置してください。  

#### パネル  
CASTの画面を含む画像をPilotLampとして貼ります。  
CastPanelEX3はその画像の左上から長方形で描画します。画面を傾けたい場合はNeddleとして貼ってTiltで調整してください。(プラグインに変形機能があるにはありますが、めちゃくちゃ重いので推奨しません)  
  
#### サウンド(保留)
現状ではサウンドの仕様が不明なため、保留としています。  
(旧CastPanelの仕様)  
停車位置を表示する際に１回サウンドを鳴らします。  
サウンドファイルのats0～ats255のどこかに設定してください。
  
#### 外部送信  
ネットワークを通じてCastPanel3にデータを送信します。  
ファイアウォールもしくはお使いのセキュリティソフトにて送信の許可が必要になる場合があります。  

#### 設定ファイル
CAST - Size : CAST画面のサイズをwidth,heightの形で記述します。(できればW:H=3:4)  
CAST - ImageName : CAST画面を含む画像のファイル名を記述します。  
CAST - UseCom : 外部送信を有効にする場合はtrue。  
CAST - Output : ※true固定です。  
CAST - HostName : 送信先の端末名もしくはIPアドレス。自端末の場合はlocalhostまたは127.0.0.1。  
CAST - SendPort : CASTもどきアプリが待ち受けているポート番号。(1024～65535)  
SOUND - NearStation : 停車位置接近音のサウンドのインデックス(0～255)を記述します。使用しない場合は-1。  
  

## 路線データに組み込む
### 地上子  

|Type|種類|Optional|
|----|----|----|
|85000|初期化|0|
|85001|距離設定|距離程[m]|
|85010|停車場接近|0|
|85011|次停車場(停車目標)|停車目標の番号(停車目標画像の番号)|
|85012|次停車場(駅)|駅番号(station.csvで設定)|
|85014|次停車場(線路)|線路番号(station.csvで設定) ※外部送信用|
|85100|停車場|駅番号(station.csvで設定)|
|85110|信号|1～9:閉塞番号 10:出 11:場 12:継 13:遠 14:入|
|85120|トンネル|長さ[m]|
|85130|橋|長さ[m]|
|85140|踏切|0|
  
・85000  
画面や内部データを初期状態にします。CastPanelEX3用地上子の中で一番小さい距離程に設置してください。設置しないと、シナリオがリロードされた場合にリロード前の表示が残る可能性があります。  
  
・85001  
85100以降のCastPanelEX用地上子を設置したい距離程を指定します。  
設置しなかった場合、Beacon.Putイベントが発動した距離程となります。  
85100以降のCastPanelEX用地上子があるとその処理後に0にリセットされます。  
  
・85010  
この地上子を通過すると、停車場接近モードになります。  
  
・85011  
停車駅停車中モードに表示する停車目標画像の番号を設定します。停車駅停車中モード終了後に停車駅接近モードで表示される画像になります。  
  
・85012  
CastPanelEX3では使用しません。  
(停車駅停車中モードに表示する駅名の番号を設定します。停車駅停車中モード終了後に停車駅接近モードで表示される駅名になります。)  
  
・85014  
停車駅停車中モードに表示する駅名の線路名の番号を設定します。  
  
・85100  
駅を示す白い長方形を描画します。長方形の中に駅名を表示します。開始点の1100m前に設置します。  
長さは150mで固定です。出発信号機がある場合は出発信号機の150m前、ない場合は停車場中心の75m前が目安です。  
  
・85110  
信号機を示す画像を描画します。画像の左側に信号機の種類または閉塞番号を表示します。信号機位置の1100m前に設置します。  
  
・85120  
トンネルを示す白い半透明のエリアを描画します。開始点の1100m前に設置します。  
  
・85130  
踏切を示す画像を描画します。踏切位置の1100m前に設置します。  
  
  
### 設置例

0;  
Beacon.Put(85001, 1, -190);  
Beacon.Put(85100, 1, 8); // 駅(富士 910～1060mに表示)  
1000;  
Beacon.Put(85011, 1, 19); // ダミー  
Beacon.Put(85012, 1, 8); // ダミー  
Beacon.Put(85011, 1, 4); // 4両停目 ★シナリオ開始時点でこれらが表示される  
Beacon.Put(85012, 1, 201); // 柚木  
1940;  
Beacon.Put(85011, 1, 4); // 4両停目  
Beacon.Put(85012, 1, 202); // 竪堀  
Beacon.Put(85010, 1, 0); // 停車場接近モード(柚木4両を表示。開扉後に竪堀4両を表示)  
2029;  
Beacon.Put(85001, 1, distance);  
Beacon.Put(85140, 1, 0); // 踏切  
2267 - 1100;  
Beacon.Put(85001, 1, distance);  
Beacon.Put(85110, 1, 1); // 信号(第１閉塞)  
2400 - 1100;  
Beacon.Put(85001, 1, distance);  
Beacon.Put(85100, 1, 201); // 柚木 (2400～2550mに表示)  
  
  
## CastPanel3
画面をダブルクリックすると、最大化するのと同時に設定画面が表示されます。  
リッスンしたいポート番号を設定して開始ボタンを押してください。  
再度画面をダブルクリックすると、画面サイズが元に戻ります。  
終了する場合は、もとのサイズに戻した上で右上のバツボタンを押してください。  

利用する場合には、CastPanel3用のデータを送信する車両データを動作しているBveTrainsimをプレイしている端末と同じ端末内もしくは同じネットワーク内にあること。  


## ソースについて
AtsEXのSDKが別途必要です。
CastPanelEX3の参照設定にて、AtsEX、AtsEx.PluginHost、BveTypesの各プロジェクトを参照していますので、ソース利用時は参照設定を修正してください。  
NuGet等に無いDLLファイルについてはDLLフォルダに入れてあります。  


## 利用について
▼ダウンロードについて  
　右上の↓マークを押し、ZIPを押してください。  
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
  
## こんなときは
▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　このプラグインはBVE6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼BVE5で使用したときに「DXDynamicTextureが見つからない」のエラーが出る場合  
　AtsExを使用するプラグインと併用する場合、x64フォルダにあるHarmony-net48.dllとZbx1425.DXDynamicTexture-net48.dllをx86フォルダにコピーしてください。  
　※AtsExが動作するように対象フレームワークを4.0に変えるため、CastPanelは上記2ファイルが必要になってしまいます。  

▼その他のエラーが出た場合  
　プラグインのバージョン、エラーの内容、エラーが出た時の状況、プラグイン設定ファイルの内容等を詳しく教えてください。  
　また、プラグインと同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
